
#include<stdio.h>
#include<iostream>
#include<string.h>
#include<stdlib.h>
using namespace std;

int main(int argc, char **argv)
{
      if(argc<5)
      {
	cout<<"Usage: <img> <width> <height> <output>\n";
	exit(0);
      }
      
      int width=atoi(argv[2]);
      int height=atoi(argv[3]);
     
      FILE *f = fopen(argv[1],"r");
      FILE *g = fopen(argv[4],"w");
      
      int **arr = new int*[height];
      for(int i=0;i<height;i++)
	arr[i] = new int[width];
      
      for(int i=0;i<height;i++)
	for(int j=0;j<width;j++)
	  fscanf(f,"%d",&arr[i][j]);
	
      for(int i=0;i<width;i++)
      {	    
	for(int j=0;j<height;j++)
	  fprintf(g,"%d ",arr[height-j-1][i]);
	fprintf(g,"\n");
      }
      fclose(f);
      fclose(g);
      return 0;
}      
      