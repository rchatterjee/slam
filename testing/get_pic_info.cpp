#include<stdio.h>
#include<stdlib.h>
#include<iostream>
using namespace std;
int  **pic,**pictemp;
int IsComplete;
int FILE_NO=1;
class pixel
{
	public:int x,y;
};

class region
{
	public:
	float xmean,ymean;
	int pixelcount;
	pixel *Pixel;
	int pcount;
	pixel *Perimeter;
	int rtlen;
	int *relationtree;
	int nrows,ncols;
	int *status;
	public:
	region(int,int);
	~region();
	void findRegion(int x,int y,int label);
	void calcMean();
	int isNeighbour(pixel,pixel);
	int findRelationTree();

};

region::region(int m, int n)
{
	pixelcount=0;
	pcount=0;
	nrows=m;
	ncols=n;
	Pixel=(pixel*)malloc(m*n*sizeof(pixel));
	Perimeter=(pixel*)malloc(m*n*sizeof(pixel));
	status=new int[m*n];
}

region::~region()
{
	free(Pixel);
	free(Perimeter);
	free(status);
}

void region::findRegion( int x, int y, int label)
{
	if(y<=0 || x<=0 ||y>=nrows-1||x>=ncols-1)	
	{
		IsComplete=0;
		return;
	}	

	if(pictemp[y][x]!=label)
	{
	//	printf("pcount=%d",pcount);
		if(pic[y][x]!=label)
		{
			for( int i=0;i<pcount;i++)
				if(Perimeter[i].x==x && Perimeter[i].y==y)
					return;
			Perimeter[pcount].x=x;
			Perimeter[pcount++].y=y;
		}	
		return;
	}	
	
	pictemp[y][x]=-1;

	Pixel[pixelcount].x=x;
	Pixel[pixelcount++].y=y;

	findRegion(x+1,y,label);
	findRegion(x+1,y+1,label);
	findRegion(x,y+1,label);
	findRegion(x-1,y+1,label);
	findRegion(x-1,y,label);
	findRegion(x-1,y-1,label);
	findRegion(x,y-1,label);
	findRegion(x+1,y-1,label);
}	

void region::calcMean()
{
	float x=0.0,y=0.0;
	for(int i=0;i<pixelcount;i++)
	{
		x+=Pixel[i].x;
		y+=Pixel[i].y;
	}
	xmean=x/pixelcount;	
	ymean=y/pixelcount;		
}	

int region::isNeighbour(pixel p1,pixel p2)
{
	if( p1.x-p2.x>=-1 && p1.x-p2.x<=1 && p1.y-p2.y>=-1 && p1.y-p2.y<=1)
	{
		if(p1.x==p2.x && p1.y==p2.y)
			return 0;
		else
			return 1;
	}
	return 0;
}				
	
int region::findRelationTree()
{
	int i,j,tmp,min,max;
	rtlen=2;
	relationtree=new int[(pcount+1)];
/*	
	int label=pic[Perimeter[0].y][Perimeter[0].x];
	int count=0;
	for(i=0;i<pcount;i++)
	{
	  if(label==pic[Perimeter[i].y][Perimeter[i].x])
	    count++;
	  if(label!=pic[Perimeter[i].y][Perimeter[i].x])
	  {
	    if(count>10 && label!=relationtree[rtlen-1])
	      relationtree[rtlen++]=label;
	    count=1;
	    label=pic[Perimeter[i].y][Perimeter[i].x];
	  }
	} 
	  
	for(i=0;i<pcount;i++)
	{
	  if(label==pic[Perimeter[i].y][Perimeter[i].x])
	    count++;
	  if(label!=pic[Perimeter[i].y][Perimeter[i].x])
	  {
	    if(count>10 && label!=relationtree[1])
	      relationtree[rtlen++]=label;
	    break;
	  }
	} 
*/
	int count[15];
	for(i=0;i<15;i++)
	  count[i]=0;
	for(i=0;i<pcount;i++)
	  count[pic[Perimeter[i].y][Perimeter[i].x]]++;
	int maxcount=0;
	for(i=0;i<15;i++)
	  if(maxcount<count[i])
	  {
	    maxcount=count[i];
	    relationtree[1]=i;
	  }  
//	if((relationtree[rtlen-1]==relationtree[1] && rtlen>2) || relationtree[rtlen-1]==0)
//		rtlen--;	

    	if(rtlen<2 || rtlen>8)
    		return 0;
   	 else
    		return 1;	
    //printf("\n");			    

}

void getPicInfo(int argc, char **argv)
{
	FILE *f;
	int m,n,i,j;
	FILE *fp=fopen("string_table.asc","r+a");
	char ch=getc(fp);
	while( ch != EOF ) {
       if(ch=='\n') FILE_NO++;
       ch = getc( fp );
     }
  	fprintf(fp, "%s\n",argv[1]);
	fclose(fp);
	//cout<<"####FILE NO: "<<FILE_NO<<endl;

	n=atoi(argv[2]);
	m=atoi(argv[3]);
	
	f = fopen( argv[1],"r"); //DSLabels442x649.asc
	
	pictemp = new int*[m];
	pic = new int*[m];
	
	for(i=0;i<m;i++)
	{
		pictemp[i] = new int[n];
		pic[i] = new int[n];
	}	
	
	for(i=0; i<m; i++)
	{			
		for(j=0; j<n; j++)
		{
			fscanf(f,"%d",&pic[i][j]);
			pictemp[i][j]=pic[i][j];
		}
	}
	fclose(f);
	
	
	
	int label,landmark=0,p=0,t;
       
	f=fopen("picInfo.txt","w");
	
	region R=region(m,n);
	
	for(i=1;i<m-1;i++)
	for(j=1;j<n-1;j++)
		if(pictemp[i][j]!=-1)
		{
			
			IsComplete=1;
			R.pixelcount=0;
			R.pcount=0;
			label=pictemp[i][j];
				
			R.findRegion(j,i,label);
		//	cout<<label<<endl;
			if(IsComplete==0 || R.pixelcount<3)
				break;
			
		//	cout<<label<<endl;
			t=R.findRelationTree();		
			if(!t)
				break;
			
		//	cout<<label<<endl;		
			R.relationtree[0]=label; 
				
			R.calcMean();	
		//	printf(">>>>>>>>>>%f %f\n",R.xmean-(n-1)/2.0,R.ymean-(m-1)/2.0);
			fprintf(f,"%f %f\n%d\n",R.xmean-(n-1)/2.0,R.ymean-(m-1)/2.0,R.rtlen);
			for(int k=0;k<R.rtlen;k++)
			{
				fprintf(f,"%d ",R.relationtree[k]);
			}	
			fprintf(f,"\n");
			landmark++;

		}
		
		
	for(i=0;i<m;i++)
	{
		delete []pic[i];
		delete []pictemp[i];
	}
	
	delete []pic;
	delete []pictemp;
	
	
			
	fclose(f);

	//printf("No of landmarks:%d\n",landmark);	

}	

