#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<assert.h>
#include "relation_tree.cpp"
#include "get_pic_info.cpp"
#include "matrix.cpp"

#include <iostream>
using namespace std;

////////////////////Update of a Particle///////////////////////////////////
/*
*expected_pose         st<hat>
*pose_variance         initial error in control information (sigma in paper)
*num_of_obs            no of closed segments observed
*expected_obs          zt<hat>
*G_map               Jacobian wrt theta  
*G_pose                Jacobian wrt theta 
*N[][]                 N[i][0] contains the number of candidate landmarks
                       followed by the N[i][0] indices 
*w                     weight associated with particle, used in resampling  
*mean,cov              mean and covariance of proposal distribution                                                    
*proposal /R             covariance of initial proposal distribution    
*z                     observation            
*p                     probability (% match)  
*highestmatch_prob     %match for maximum match of landmark
*highestmatch_loc      location of the maximum matched landmark
*p0                    threshold value of probability       
*relation tree comparison to be done for every particle separately.
 So, for every particle, we will have comparisons with RAG of landmarks in that particular particle.
 Changed part not applied yet.	


*Additional non parameterized input required for updateParticle()
-----num_of_obs, **N (for candidate observations)----------		


*/
/////////////////////Structures used/////////////////////////////////////

#define PI  3.14159265
#define EXP 2.71828183
#define THRESHOLD_td 0.0005

// class to store landmarks

class Landmark
{
public:
  Matrix mean;			// mean of landmark
  Matrix variance;		// variance associated with landmark pose
  int rtlen;			// relation tree length
  int *relationtree;		// the relation tree		
  int nfile, ifinfo[100][3];	//'nfile' number of file where the landmark is present
  
  void loadinfo(int fileno, Matrix p){
    ifinfo[nfile][0]=fileno;
    ifinfo[nfile][1]=p.value[0][0];
    ifinfo[nfile][2]=p.value[1][0];
    nfile++;
    return ;
  }
  
  Landmark(){
  
 	mean = Matrix(2,1);
 	variance = Matrix(2,2);
 	nfile=0;
 }
 														//'ifinfo' is the land mark info corresponding to the image file
 /* Landmark operator=(Landmark L)
  {
  	mean = L.mean;
  				
  	variance = L.variance;
  	rtlen = L.rtlen;
  	relationtree = new int[rtlen];
  	for(int i=0;i<rtlen;i++)
  		relationtree[i] = L.relationtree[i];
  	nfile = L.nfile;
  	for(int i=0;i<100;i++)
  		for(int j=0;j<3;j++)
  			ifinfo[i][j] = L.ifinfo[i][j];
  }*/
};


// class to store particles
class Particle
{
	public:
	int N;					// number of landmarks
	Matrix pose;				// present pose of robot
	Landmark *landmark;			// landmarks observed 
	double w;				// weight associated with the particle
	/*Particle operator=(Particle A)
	{
		pose=A.pose;
		landmark =  new Landmark[N];
		for(int i=0;i<A.N;i++)
			landmark[i]=A.landmark[i];
		w=A.w;
	 	return *this;
	}*/
};	




		
////////////////Particle Update////////////////////////////////////


// This function takes observation set, checks all the landmarks seen upto the previous level
// Prepares a list of candidates for each of the observation

void relationTreeCheck( Landmark **obs, int num_obs, Particle *p ,int ***candidates)
{
	float td;
	int partition_cost=1;
	
	for( int i=0;i<num_obs;i++)
	{
		(*candidates)[i][0]=0;
		
		
		for(int j=0;j<p->N;j++)
		{

			td=getTreeDistance( (*obs)[i].relationtree, (*obs)[i].rtlen, 
								p->landmark[j].relationtree, p->landmark[j].rtlen, 
								 partition_cost );
		//	printf("TD THRESH %d %f\n",i,td);
			if( td > THRESHOLD_td)
			{
		//		
				(*candidates)[i][0]++;
				(*candidates)[i][(*candidates)[i][0]]=j;
			}

		}	
		(*candidates)[i][(*candidates)[i][0]+1]=p->N;
/*	printf("Candidates");
	for(int j=0;j<=(*candidates)[i][0];j++)
		printf("%d ", (*candidates)[i][j]);
	printf("\n");	
*/	
	}	
	
// 	return candidates;
}


// Returns maximum number of candidates possible (useful for memory allocation) 

int maximum(int **N,int n)
{

  int max=0;
  for(int i=0;i<n;i++)
    if(max<N[i][0]) max=N[i][0];

  return max;
}


// Returns probability of correctness after data association

float calcProb(	Matrix error, float obserror_x, Matrix *L)
{
  //this is only for specific case of linear observation error in x only
  	float temp=(error.value[0][0]*error.value[0][0])/(obserror_x*obserror_x);

	return pow( EXP, -0.5*temp)/abs(sqrt(2*PI)*L->det());
  
}


// Main function for updating particle
// R -> pose variance
// 
void updateParticle ( Particle *P , Matrix u, Matrix R, Landmark **obs, int num_obs, int **N )
{
	Matrix expected_pose(3, 1), expected_obs(2, 1), temp3(1, 1), tempPose(3, 1), tempObs(2, 1);
	Matrix pose_variance(3, 3);
	int maxcandi = maximum(N,num_obs);
	float p0=0.0001;
	int n;
	
	//this formulae is presently for linear motion
	//change for non linear
	expected_pose= P->pose + u;

	printf("expctd pose: %f %f %f\n", expected_pose.value[0][0], expected_pose.value[1][0], expected_pose.value[2][0]);

	Matrix *z=new Matrix[num_obs];
	for(int i=0;i<num_obs;i++){
		z[i] = Matrix(2, 1);
		z[i] = (*obs)[i].mean;
	}
	
	Matrix G_map(2, 2), G_pose(2, 3), I(2, 2);
	Matrix Z(2, 2), invZ(2, 2), invL(3, 3), K(3, 2), L(2, 2), tempmat(3, 2);			//dekhe
	int loc;
	
	printf("SSUP111!%d\n", __LINE__);	
	cout<<"max candi="<<maxcandi<<endl;
	Matrix *mean = new Matrix[maxcandi+1];
	Matrix *cov  = new Matrix[maxcandi+1];
	for(int i=0;i<maxcandi+1;i++){
		mean[i]=Matrix(3,1);
		cov[i]=Matrix(3,3);
	}
	double *p= new double[maxcandi+1];
	
	float *highestmatch_prob=new float[num_obs];
	int *highestmatch_loc=new int[num_obs];
	
	for(int i=0;i<num_obs;i++)
	{
		highestmatch_prob[i]=0.0;
		highestmatch_loc[i]=0;
	}	
	//G_map -> Jacobian wrt map-features

	pose_variance = R;	
	printf("SSUP!%d\n", __LINE__);		
	double ct=cos(expected_pose.value[2][0]), st=sin(expected_pose.value[2][0]);
	
	G_map.value[0][0] =  ct;	G_map.value[0][1] =  st;
	G_map.value[1][0] = -st;	G_map.value[1][1] =  ct;
	
	G_pose.value[0][0] = -ct;	G_pose.value[1][0] = -st;
	G_pose.value[0][1] =  st;	G_pose.value[1][1] = -ct;
	
	for(int i=0;i<num_obs;i++)
	{
		for( n=0; n < N[i][0] ;n++)
		{
			expected_obs.g( &expected_pose, &P->landmark[N[i][n+1]].mean);
			
			G_pose.value[0][2] = - st*(P->landmark[N[i][n+1]].mean.value[0][0] - expected_pose.value[0][0]) 
								 - ct*(P->landmark[N[i][n+1]].mean.value[1][0] - expected_pose.value[1][0]) ;	
								 
			G_pose.value[1][2] =   ct*(P->landmark[N[i][n+1]].mean.value[0][0] - expected_pose.value[0][0]) 
								 - st*(P->landmark[N[i][n+1]].mean.value[1][0] - expected_pose.value[1][0]) ;	
							 
			Z = pose_variance + (G_map * P->landmark[N[i][n+1]].variance * G_map.transpose());
			invZ = Z.inverse();
			printf("%d\n", __LINE__);
			cov[n] = (G_pose.transpose()*invZ*G_pose + R.inverse()).inverse();	
			mean[n] = expected_pose + cov[n] * G_pose.transpose() * invZ * (z[i] - expected_obs);
			printf("%d\n", __LINE__);
			tempPose.g(&mean[n], &P->landmark[N[i][n+1]].mean);
			tempPose = z[i]-tempPose;
			
			temp3 = tempPose.transpose() * invZ * tempPose;

			p[n]= pow( EXP, -0.5*temp3.value[0][0])/(sqrt(abs( 2* PI*Z.det())));	

		}

		p[n]=p0;
		loc=0;
		
		for( n=0;n<=N[i][0];n++) 
		{
			if(highestmatch_prob[i] < p[n])
			{
				highestmatch_prob[i]=p[n];
				highestmatch_loc[i]=N[i][n+1];
				loc=n; 
			}
		}
		for( int j=0;j<i;j++)
			if( highestmatch_loc[j] == highestmatch_loc[i])
			{
				if(highestmatch_prob[i]>highestmatch_prob[j])
					highestmatch_loc[j] = P->N;	
				else
					highestmatch_loc[i] = P->N;	
				break;
			}	
		
		if(highestmatch_loc[i] < P->N)
		{
			pose_variance = cov[loc];
			expected_pose = mean[loc];
		}
	}

	
	printf("sampling: %f %f %f\n", expected_pose.value[0][0], expected_pose.value[1][0], expected_pose.value[2][0]);
	P->pose.sample(&expected_pose, &pose_variance);
	printf("sample: %f %f %f\n", P->pose.value[0][0], P->pose.value[1][0], P->pose.value[2][0]);
	P->w = 100.0;
	loc = P->N;
		
	ct=cos(expected_pose.value[2][0]); st=sin(expected_pose.value[2][0]);
		
	G_map.value[0][0] =  ct;	G_map.value[0][1] =  st;
	G_map.value[1][0] = -st;	G_map.value[1][1] =  ct;
	
	G_pose.value[0][0] = -ct;	G_pose.value[1][0] = -st;
	G_pose.value[0][1] =  st;	G_pose.value[1][1] = -ct;
		
	
	int oldLandmarks = 0, newLandmarks = 0;
	Matrix R1(2,2);
	R1.value[0][0] = R.value[0][0];		R1.value[0][1] = R.value[0][1];
	R1.value[1][0] = R.value[1][0];		R1.value[1][1] = R.value[1][1];

	I.value[0][0]=I.value[1][1]=1;
	for( int i=0; i<num_obs; i++)
	{
		if( highestmatch_loc[i]==loc)
		{
			P->landmark[P->N].mean.inv_g( &P->pose, z[i]);
			//TODO allocate memory and then put obserr_x in 0 index
			*(P->landmark[P->N].variance) = (G_map.transpose() * R1.inverse() * G_map).inverse();
			
			P->landmark[P->N].rtlen = (*obs)[i].rtlen;
			P->landmark[P->N].relationtree = (*obs)[i].relationtree;
			//P->landmark[P->N].pixelcnt=(*obs)[i].pixelcnt;
			//P->landmark[P->N].Pixel=(*obs)[i].Pixel;
			P->w *= p0;
			P->N++;
			newLandmarks++;
			///******************************************************************
			P->landmark[P->N-1].loadinfo(FILE_NO,*(z[i]));
			//*******************************************************************
		}
		else
		{
			expected_obs.g(&expected_pose, P->landmark[ highestmatch_loc[i]].mean);	
			
			G_pose.value[0][2] = - st*(P->landmark[highestmatch_loc[i]].mean.value[0][0] - expected_pose.value[0][0]) 
								 - ct*(P->landmark[highestmatch_loc[i]].mean.value[1][0] - expected_pose.value[1][0]) ;							 
			G_pose.value[1][2] =   ct*(P->landmark[highestmatch_loc[i]].mean.value[0][0] - expected_pose.value[0][0]) 
								 - st*(P->landmark[highestmatch_loc[i]].mean.value[1][0] - expected_pose.value[1][0]) ;	
			printf("%d\n", __LINE__);		
			Z = R1 + (G_map * *(P->landmark[highestmatch_loc[i]].variance) * G_map.transpose());
			K = *(P->landmark[highestmatch_loc[i]].variance) * G_map.transpose() * Z.inverse();				//??????????
			tempObs = *(z[i])-expected_obs;
			
			*(P->landmark[highestmatch_loc[i]].mean) = *(P->landmark[highestmatch_loc[i]].mean) + K * tempObs;
			L = G_pose*R*G_pose.transpose() + R1 + G_map * *(P->landmark[highestmatch_loc[i]].variance) * G_map.transpose();
			*(P->landmark[highestmatch_loc[i]].variance) = (I-K*G_map) * *(P->landmark[highestmatch_loc[i]].variance);
			invL = L.inverse();
			printf("%d\n", __LINE__);	
			temp3 = tempObs.transpose()*invL*tempObs;
		
			P->w*= 1000.0 *pow( EXP, -0.5*temp3.value[0][0])/abs(sqrt(2*PI*L.det()));	
			oldLandmarks++;
			///******************************************************************
			P->landmark[highestmatch_loc[i]].loadinfo(FILE_NO, expected_obs);
			//*********************************************************************
		}
		
	}		
	printf("weight=%lf\n",P->w);
	printf("Old landmarks observed:%d\n",oldLandmarks);		
	printf("New landmarks observed:%d\n",newLandmarks);	
	printf("Total landmarks detected:%d\n",P->N);	
	

	printf("Deleting....\n");
	delete[] mean;
	delete[] cov;
	printf("Successfully deleted\n");

}
///////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////Resampling///////////////////////////////////////////////////////////

Particle* resampling(Particle* P, int M )
{
	float *index = (float*)malloc((M+1)*sizeof(float));
	index[0]=0.0;
	int randno;
	for(int i=1; i<=M; i++)
	{
		index[i] = P[i-1].w*1000 + index[i-1] + 1;
	//	printf("%d %f\n",i,index[i]);	
	}	
	Particle *updatedSet = new Particle[M];

	for(int i=0; i<M; i++)
	{
		randno= rand() % (int)index[M];
		
		for(int j=1; j<=M; j++)
			if(randno<index[j])
			{
				updatedSet[i] = P[j-1];
				printf("ami jani na: %x\n", updatedSet[i].N);
				break;
			}
	}
	
	return updatedSet;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////				

