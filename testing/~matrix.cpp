#define PI  3.14159265
#define EXP 2.71828183
#include<stdio.h>
#include<cmath>
#include<stdlib.h>

class Matrix
{
public:
   int nrows, ncols;
   float **value;
   
public:Matrix(int n_rows=0, int n_cols=0):nrows(n_rows), ncols(n_cols)
	{
   	  if(!(n_rows*n_cols)){ 
      value = new float*[n_rows];
      for(int i=0;i<nrows;i++){
	 	value[i] = new float[n_cols];
	 	for(int j=0;j<ncols;j++)
	    	value[i][j]=0;
      	}
      }
      
	}
   friend Matrix operator+ ( Matrix A, Matrix B)
   {
      Matrix C( A.nrows, A.ncols);
      for(int i=0;i<A.nrows;i++)
	 {
	    for(int j=0;j<A.ncols;j++)
	       C.value[i][j]=A.value[i][j]+B.value[i][j];
	 }		
      return C;
   }	
   
   friend Matrix operator- (Matrix A, Matrix B)
   {
      Matrix C( A.nrows, A.ncols);
      for(int i=0;i<A.nrows;i++)
	 {
	    for(int j=0;j<A.ncols;j++)
	       C.value[i][j]=A.value[i][j]-B.value[i][j];
	 }		
      return C;
   }	
   
   friend Matrix operator* (Matrix A, Matrix B)
   {
      if(A.ncols != B.nrows) printf("OMG!!!@   Dimention mismatch in '*'!!!!!\n");
      Matrix C(A.nrows, B.ncols);
      for(int i=0;i<C.nrows;i++)
	 {
	    for(int j=0;j<C.ncols;j++)
	       {
		  C.value[i][j]=0;
		  for(int k=0;k<A.ncols;k++)
		     C.value[i][j]+=A.value[i][k]*B.value[k][j];
	       }
	 }
      return C;
   }		
   
   /*   Matrix operator=(Matrix A)
   {
      if(A.ncols != ncols || A.nrows != nrows) printf("OMG!!!@ <%d,%d> & <%d, %d>  Dimention mismatch in '='!!!!!\n",nrows, ncols, A.nrows, A.ncols);
      
      
      for(int i=0;i<A.nrows;i++)
	 for(int j=0;j<A.ncols;j++)
	    value[i][j]=A.value[i][j];
      return *this;
   }	
   */
public:Matrix transpose()
   {
      Matrix A( ncols, nrows);
      for(int i=0;i<ncols;i++)
	 {
	    for(int j=0;j<nrows;j++)
	       A.value[i][j]=value[j][i];
	 }		
      return A;
   }
   
public:Matrix inverse()
   {
      Matrix inverse(nrows, ncols), I(nrows, ncols);
      int matsize=nrows;
      for(int i=0;i< nrows;i++)
	 for(int j=0;j<ncols;j++){
	    inverse.value[i][j]=value[i][j];
	    I.value[i][j]=(i==j)?1:0;
	 }
      //procedure to make the matrix A to unit matrix
      float temp;
      for(int k=0;k<matsize;k++){
	 temp=inverse.value[k][k];
	 for(int j=0;j<matsize;j++){
	    inverse.value[k][j]/=temp;
	    I.value[k][j]/=temp;
	 }
	 for(int i=0;i<matsize;i++){
	    temp=inverse.value[i][k];
	    for(int j=0;j<matsize;j++){
	       if(i==k)
		  break;
	       inverse.value[i][j]-=inverse.value[k][j]*temp;
	       I.value[i][j]-=I.value[k][j]*temp;
	    }
	 }
      }
      return I;
   }
   
public:Matrix inverse3Mat ()
   {
      Matrix A(3,3);
      float determinant= det();
      
      A.value[0][0]=(value[1][1]*value[2][2]-value[1][2]*value[2][1])/determinant; 
      A.value[0][1]=(value[0][2]*value[2][1]-value[0][1]*value[2][2])/determinant; 
      A.value[0][2]=(value[0][1]*value[1][2]-value[0][2]*value[1][1])/determinant; 
      A.value[1][0]=(value[1][2]*value[2][0]-value[1][0]*value[2][2])/determinant; 
      A.value[1][1]=(value[0][0]*value[2][2]-value[0][2]*value[2][0])/determinant; 
      A.value[1][2]=(value[0][2]*value[1][0]-value[1][2]*value[0][0])/determinant; 
      A.value[2][0]=(value[1][0]*value[2][1]-value[1][1]*value[2][0])/determinant; 
      A.value[2][1]=(value[0][1]*value[2][0]-value[0][0]*value[2][1])/determinant; 
      A.value[2][2]=(value[1][1]*value[0][0]-value[1][0]*value[0][1])/determinant; 
      
      return A;
      
   }
   
public:Matrix inverse2Mat ()
   {
      Matrix A(2,2);
      float determinant= det();
      
      A.value[0][0]=value[1][1]/determinant; 
      A.value[0][1]=(-1)*value[0][1]/determinant; 
      A.value[1][0]=(-1)*value[1][0]/determinant; 
      A.value[1][1]=value[0][0]/determinant;
      
      return A;
      
   }
   
   
public:float det()
   {
      int a = nrows;
      float d=0;
      if(a==3)
	 {
	    d+=value[0][0]*(value[1][1]*value[2][2]-value[1][2]*value[2][1]);
	    d-=value[0][1]*(value[1][0]*value[2][2]-value[1][2]*value[2][0]);
	    d+=value[0][2]*(value[1][0]*value[2][1]-value[1][1]*value[2][0]);
	 }
      if(a==2)
	 {
	    d+=value[0][0]*value[1][1]-value[0][1]*value[1][0];
	 }
      if(a==1)
	 d=value[0][0];
      
      return d;
   }	
   
public:void g( Matrix *expected_pose, Matrix *landmarkmean )
   {
      float t;
      value[0][0]= landmarkmean->value[0][0]-expected_pose->value[0][0]; 
      value[1][0]= landmarkmean->value[1][0]-expected_pose->value[1][0]; 
      t= value[0][0];
      float ct= cos(expected_pose->value[2][0]), st=sin(expected_pose->value[2][0]);
      value[0][0]= value[0][0]*ct - value[1][0]*st;
      value[1][0]=	t*st  	 + value[1][0]*ct;
      //value[2][0]= 0;
      
   }	
   
public:void inv_g( Matrix *expected_pose, Matrix *z )
   {
      float ct= cos(expected_pose->value[2][0]), st=sin(expected_pose->value[2][0]);
      value[0][0] = expected_pose->value[0][0] + z->value[0][0]*ct + z->value[1][0]*st;
      value[1][0] = expected_pose->value[1][0] - z->value[0][0]*st + z->value[1][0]*ct;
      //value[2][0] = 0;	
      
   }			
   
public:void sample ( Matrix *mean, Matrix *variance)
   {
      float u=(rand()%1000+1)/1000.0;
      float v=(rand()%1000+1)/1000.0;
      float x= sqrt( -2*log(u)) * cos (2*PI*v);
      value[0][0]=mean->value[0][0] + x*sqrt(variance->value[0][0]);
      
      u = (rand()%1000+1)/1000.0;
      v=(rand()%1000+1)/1000.0;
      x= sqrt( -2*log(u)) * cos (2*PI*v);
      value[1][0]=mean->value[1][0] + x*sqrt(variance->value[1][1]);
      
      u=(rand()%1000+1)/1000.0;
      v=(rand()%1000+1)/1000.0;
      x= sqrt( -2*log(u)) * cos (2*PI*v);		
      value[2][0]=mean->value[2][0] + x*sqrt(variance->value[2][2]);
   }	
   
public:void print()
   {
      for(int i=0;i<nrows;i++)
	 {
	    for(int j=0;j<ncols;j++)
	       printf("%f ",value[i][j]);
	    printf("\n");
	 }
   }		
   
};
/*
  class Pose
  {
  public:float value[3];
  public:Pose()
  {
  for(int i=0;i<3;i++)
  value[i]=0;
  }
  friend Pose operator+ (Pose A, Pose B)
  {
  Pose C;
  for(int i=0;i<3;i++)
  {
  C.value[i]=A.value[i]+B.value[i];
  }		
  return C;
  }	
  friend Pose operator- (Pose A, Pose B)
  {
  Pose C;
  for(int i=0;i<3;i++)
  {
  C.value[i]=A.value[i]-B.value[i];
  }		
  return C;
  }	
  Pose operator=(Pose A)
  {
  for(int i=0;i<3;i++)
  value[i]=A.value[i];
  return *this;
  }	
  
  public:void g( Pose *expected_pose, Pose *landmarkmean )
  {
  float t;
  value[0]= landmarkmean->value[0]-expected_pose->value[0]; 
  value[1]= landmarkmean->value[1]-expected_pose->value[1]; 
  t= value[0];
  float ct= cos(expected_pose.value[2]), st=sin(expected_pose.value[2]);
  value[0]= value[0]*ct - value[1]*st;
  value[1]=	t*st  	 + value[1]*ct;
  value[2]= 0;
  
  }	
  
  public:void inv_g( Pose *expected_pose, Pose *z )
  {
  float ct= cos(expected_pose.value[2]), st=sin(expected_pose.value[2]);
  value[0] = expected_pose->value[0] + z->value[0]*ct + z->value[1]*st;
  value[1] = expected_pose->value[1] - z->value[0]*st + z->value[1]*ct;
  value[2] = 0;	
  
  }			
  
  public:void sample ( Pose *mean, Matrix *variance)
  {
  float u=(rand()%1000+1)/1000.0;
  float v=(rand()%1000+1)/1000.0;
  float x= sqrt( -2*log(u)) * cos (2*PI*v);
  value[0]=mean->value[0]+ x*sqrt(variance->value[0][0]);
  
  u = (rand()%1000+1)/1000.0;
  v=(rand()%1000+1)/1000.0;
  x= sqrt( -2*log(u)) * cos (2*PI*v);
  value[1]=mean->value[1]+ x*sqrt(variance->value[1][1]);
  
  u=(rand()%1000+1)/1000.0;
  v=(rand()%1000+1)/1000.0;
  x= sqrt( -2*log(u)) * cos (2*PI*v);		
  value[2]=mean->value[2]+ x*sqrt(variance->value[2][2])*PI/18000;
  }	
  
  public: void print()
  {
  printf("%f %f %f\n",value[0],value[1], value[2]);
  }
  
  };
*/




