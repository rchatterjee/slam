#!/bin/bash
#gnome-terminal

#
# Set these variables value and run the script        
# <num of raw images> <raw imnages> <sourceimgwidth> <sourceimgheight> <width> <height> <numcut image>
# img_centers="r1 theta1 r2 theta2..."
#<num raw images> <raw images> <width< <height> <widthof output images> <height of output images> <noise matrix ( only xx and yy and theta_theta is considered)> <no. of points of path> <initial position( x , y , theta )> <list of displacements>
#
EXECS=executables
RESULTS=results
#echo "segmentation1.sh is running"
if (($#<4))
then
	opt_file=~/Desktop/seg_full_Jun4/images/optical.jpg
	rad_file=~/Desktop/seg_full_Jun4/images/radar.jpg
	num_rawfile=5
	imagepath=~/Desktop/seg_full_Jun4/images/
	out_putimg_folder=cut_image
	sourceimgw=1016
	sourceimgh=1485	
	width=500
	height=500
	numsensors=2
	num_optimg=4
	num_radimg=1
	num_cut_img=1
	img_centers="200 30 100 0 " # 140 100 0"
	#raw_img_folder=raw_folder
else
	num_raw_img=$1
	source_raw_img=$2
	#out_putimg_folder=cut_image
	sourceimgw=$3
	sourceimgh=$4
	width=$5
	height=$6
	numsensors=2
	num_optimg=4
	num_radimg=1
	
	num_cut_img=$7
	raw_img_folder=raw_folder
	#read -p "image centers(${num_cut_img} no of xx yy theta)" img_centers
	img_centers=$8
#	echo $img_centers
fi
################################END#################

#rm -r ${raw_img_folder} ${out_putimg_folder} classified #*.raw
rm -rf ${RESULTS}
mkdir ${RESULTS}
#clear
echo "Cutting Images..."

####-----------------Commented----------------######
simu="${EXECS}/cutimage.out ${num_raw_img} ${source_raw_img} ${sourceimgw} ${sourceimgh} ${width} ${height} 0 0 0 ${num_cut_img} ${img_centers}"
#simu="${EXECS}/cutimage1.out ${num_raw_img} ${source_raw_img} ${sourceimgw} ${sourceimgh} ${width} ${height} ${num_cut_img} ${img_centers}"
echo $simu;
#echo ${simu};
./${simu}
#./simulation ${simu}

mv *_*.raw raw_images/ 
echo "Segmenting..."
for ((i=1;i<=${num_cut_img}+1; i++))
do
#	echo $i
	raw_img=raw_images/${i}_*.raw
#	echo $raw_img 
	#clear
	echo "Image ${i}"
	./${EXECS}/uniform.out raw_images/${i}_2.raw ${width} ${height} initseg1.asc 0 3 -f 2.1 
	./${EXECS}/uniform.out raw_images/${i}_2.raw ${width} ${height} initseg2.asc 0 3 -f 2.1
	./${EXECS}/graph.out ${numsensors} ${num_optimg} ${num_radimg} ${raw_img} ${width} ${height} 2 initseg1.asc initseg2.asc 2 14 0 dsl 1 1 1 1
	./${EXECS}/convertor.out out.asc $width $height 14 0
	mv out.asc ${RESULTS}/out$i.asc
	mv out.ppm $RESULTS/ClassifiedImage_$i.ppm
done
echo "done"
rm -r *.asc *.bin RAG.txt

