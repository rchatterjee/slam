#include<stdio.h>
#include<iostream>
#include<fstream>
#include<string>
#include<math.h>
#include<stdlib.h>

#define NUM_LANDMARK 10000
#define PI 3.14159265
using namespace std;
void readParticle(char *particleFile);
void printMap();
void showImg(int width, int height);

class imgFile {
public:
  int nL;
  double **Lp_l, **Lp_g;
  double *trans;
  
  imgFile() {
      nL = 0;
      Lp_l = new double*[NUM_LANDMARK];
      Lp_g = new double*[NUM_LANDMARK];
      for(int i=0;i<NUM_LANDMARK;i++){
	Lp_l[i]= new double[3];
	Lp_g[i]= new double[3];
      }
      trans=new double[4];
  }
  ~imgFile(){
    delete []Lp_l;
    delete []Lp_g;
  }
   void loadLm(double *local, double *global) {
      Lp_l[nL][0] = local[0];
      Lp_l[nL][1] = local[1];
      Lp_l[nL][2] = local[2];
      Lp_g[nL][0] = global[0];
      Lp_g[nL][1] = global[1];
      Lp_g[nL][2] = global[2];
      nL++;
   }
   void calcTrans();
};

float sub(float th1,float th2){
   float diff = th1-th2;
   if(diff>PI) diff = 2*PI-diff;
   else if(diff<=-PI)diff+=2*PI;
   return diff;
}
float addnav(float th1,float th2, int n){
   float diff = th1-th2;
   if(diff>PI || diff<=-PI) return (th1*n+th2)/(n+1)+PI;
   else return (th1*n+th2)/(n+1);
}

void imgFile::calcTrans() {
   //Assuming x and y transformations are independent;
   //for(int i=0;i<nL;i++) Lp_l[i].show();
  //cout<<"*************************In calcTrans:**************************\n";
   if (nL <= 0) {cout<<"Oh my GOD!!! there is no landmark."<<endl;return;}
   int i;
   double X[4], X1[2];
   double mean[2][4]={{0}};
   int nL_by_2=nL/2;
   for(int i=0;i<nL_by_2;i++)
     for(int j=0;j<2;j++){
       mean[0][j]+=Lp_l[i][j];
       mean[0][j+2]+=Lp_g[i][j];
       mean[1][j]+=Lp_l[nL_by_2+i][j];
       mean[1][j+2]+=Lp_g[nL_by_2+i][j];
     }
   
   for(int j=0;j<4;j++){
      mean[0][j]=mean[0][j]/nL_by_2;
      mean[1][j]=mean[1][j]/(nL-nL_by_2);
      //printf("%f\t%f\n", mean[0][j], mean[1][j]);
   }
 
   X[0]=mean[0][0]-mean[1][0];
   X[1]=mean[0][1]-mean[1][1];

   X1[0]=mean[0][2]-mean[1][2];
   X1[1]=mean[0][3]-mean[1][3];

   //cout<<"X:\n";
   //cout<<X[0]<<"\t"<<X[1]<<endl;
   //cout<<"X1:\n";
   //cout<<X1[0]<<"\t"<<X1[1]<<endl;
   double det=X[0]*X[0]+X[1]*X[1];
   double t[2]={X[0]*X1[0]+X[1]*X1[1],-X[1]*X1[0]+X[0]*X1[1]};
   t[0]/=det;
   t[1]/=det;
   //cout<<"t:\n"<<t[0]<<"\n"<<t[1]<<"\n";
   float t_normalise=sqrt(t[0]*t[0]+t[1]*t[1]);
   //c=t[0][0]. s=t[1][0];
   t[0]/=t_normalise;
   t[1]/=t_normalise;
   t_normalise=(asin(t[1])+acos(t[0]))/2;
   //trans[0]=t[0]/t_normalise;//costheta
   //trans[1]=t[1]/t_normalise;//sintheta
   trans[0]=cos(t_normalise);//costheta
   trans[1]=sin(t_normalise);//sintheta
   
   trans[2]=(mean[0][2]+mean[1][2])/2 - t[0]*(mean[0][0]+mean[1][0])/2 + t[1]*(mean[0][1]+mean[1][1])/2;//sx
   trans[3]=(mean[0][3]+mean[1][3])/2 - t[1]*(mean[0][0]+mean[1][0])/2 - t[0]*(mean[0][1]+mean[1][1])/2;//sy

   //cout<<"Trans Mat: \t";
   //for(int p=0;p<4;p++)   cout<<trans[p]<<"\t";   
   //cout<<"\t----END-----\n\n";
   return ;
}
void findMaxTransform();
void showImg(int width, int height);

imgFile *img;
int nImg = 0;
int width, height,m;
char fileName[50][50];
int fimg_h, fimg_w;


void readParticle(char *particleFile) {
   FILE *fp;
   int nL, n; //,nfile_Ln;
   int k, x, y;
   double  *global, *local;
   float tmp;
   int itmp;

   ifstream ifs("string_table.asc");
   while (!ifs.eof()) {
      ifs.getline(fileName[nImg], 100);
      nImg++;
   }nImg--;
   ifs.close();
   int theta;
   fp = fopen(particleFile, "r");
   fscanf(fp, "%f%f%f", &tmp, &tmp, &tmp);
   fscanf(fp, "%d", &nL);
   //printf("<nL, nImg>: %d %d\n", nL, nImg);     
   //  global.MatPrint();
   global=new double[3];
   local =new double[3];

   //fscanf(fp,"%d",&nImg);
   img = new imgFile[nImg]();
   
   //for(int i=0;i<nImg;i++) img[i]= new imgFile();

   for (int i = 0; i < nL; i++) {
      fscanf(fp, "%lf%lf%lf", &global[0], &global[1], &global[2]);
      for (int i = 0; i < 9; i++)fscanf(fp, "%f", &tmp);
      fscanf(fp, "%d", &itmp);
      for (int j = itmp; j > 0; j--)fscanf(fp, "%d", &itmp);
      fscanf(fp, "%d", &n);
      for (int j = 0; j < n; j++) {
	 fscanf(fp, "%d%lf%lf%lf", &k, &local[0], &local[1], &local[2]);
	 img[k - 1].loadLm(local, global);
      }
   }
   fclose(fp);
   //cout<<"------------__***********--------_\n\n";
   return;
}
/*
void printMap() {
   for (int i = 0; i < nImg; i++) {
      printf("%d\n", i);
      for (int j = 0; j < img[i].nL; j++) {
	 printf("\t");
	 img[i].Lp_l[j].MatPrint();
      }
      printf("\n");
   }
   return;
}

*/
void findMaxTransform() {
   double minx=0, miny=0, maxx=img[0].trans[2], maxy=img[0].trans[3];
   for (int i = 1; i < nImg; i++) {
      //img[i].trans->show();
      /*if (img[(int) maximum.A[0][0]].trans->A[0][0] < img[i].trans->A[0][0]) maximum.A[0][0] = i;
      if (img[(int) maximum.A[1][0]].trans->A[1][0] < img[i].trans->A[1][0]) maximum.A[1][0] = i;
      if (img[(int) minimum.A[0][0]].trans->A[0][0] > img[i].trans->A[0][0]) minimum.A[0][0] = i;
      if (img[(int) minimum.A[1][0]].trans->A[1][0] > img[i].trans->A[1][0]) minimum.A[1][0] = i;*/
       if(minx>img[i].trans[2]) minx=img[i].trans[2];
       else if(maxx<img[i].trans[2]) maxx=img[i].trans[2];
       if(miny>img[i].trans[3]) miny=img[i].trans[3];
       else if(maxy<img[i].trans[3]) maxy=img[i].trans[3];
   }
   //cout<< "The maximum size of the img is:";
// maximum.show();
    //printf(">>min: %lf, %lf\tmax: %lf %lf\n", minx, miny, maxx, maxy);
   //transform the transformations
    m/=2;
    if(minx || miny || true){
    	 minx-=m;miny-=m;
    }
    maxx+=m*2;
    maxy+=m*2;
    //printf("min: %lf, %lf\tmax: %lf %lf\n", minx, miny, maxx, maxy);
    for (int i = 0; i < nImg; i++) {
       img[i].trans[2]-=minx;
       img[i].trans[3]-=miny;
    }
    fimg_w=maxx-minx;
    fimg_h=maxy-miny;
    for(int i=0;i<nImg;i++){
       img[i].trans[2]-=minx;
       img[i].trans[3]-=maxx;
    }
   return;
}


void buildImg() {
  //findMaxTransform();
   /* pos dimMax = maximum;
      if(img[(int)minimum.A[1][0]].trans->A[1][0]<0 || img[(int) minimum.A[0][0]].trans->A[0][0]<0 ) correct_shift();
      fimg_w = img[(int) maximum.A[0][0]].trans->A[0][0] + width;
      fimg_h = img[(int) maximum.A[1][0]].trans->A[1][0] + height;*/
   fimg_h=1000; fimg_w=1000;
   
   //cout << "height :" << fimg_h << " width:" << fimg_w << endl;
   
   int **fimg = new int*[fimg_h];
   for (int i = 0; i < fimg_h; i++) {
      fimg[i] = new int[fimg_w];
      for (int j = 0; j < fimg_w; j++)
	 fimg[i][j] = 0;
   }
   int data;
   FILE *fp;
   /****************************************************
   fp=fopen("control", "r");
   int t1,t2,t3;
   float t4, t5, t6=0;
   fscanf(fp, "%d%d%d", &t1, &t2, &t3);
   for(int i=0;i<t1;i++){
      fscanf(fp, "%f%f",&t4, &t5);
      t4=t5;
      t5+=t6;
      t6=t4;
      img[i].trans->c1=pos(cos(t5),-sin(t5),0);
      img[i].trans->c2=pos(sin(t5),cos(t5),0);
   }
   fclose(fp);
   *************************************************/
   fp=fopen("thetaInfo.txt", "r");
   if(!fp) fopen("markat.txt", "r");
   int t1,t2,t3;
   float t4, t5, t6=0;
   
   for(int i=0;i<nImg;i++){
	   fscanf(fp, "%lf%lf%f", &img[i].trans[2], &img[i].trans[3], &t4);
	   //printf("theta forImg(%d): %f\n", i, t4);
	   img[i].trans[0]=cos(t4);
       img[i].trans[1]=sin(t4);
   }
   fclose(fp);
   double x,y, costheta, sintheta;
   //  char filename[100];
   for (int i = 0; i < nImg; i++) {

      //printf("\nFile Name: %s\n", fileName[i]);
      fp = fopen(fileName[i], "r");
      if (!fp) {
	 cout << fileName[i] << ": File not found error.\n" << endl;
	 break;
      }
      float t[2];
      //      double theta=3.141*30.0/180.0;
      //img[i].trans[0]= cos(theta);img[i].trans[1]=sin(theta);img[i].trans[2]=20;img[i].trans[3]=60;
      
      //printf("Translation Matrix:\n");
      //printf("%lf\t%lf\t%lf\n", img[i].trans[0],-img[i].trans[1],img[i].trans[2]);
      //printf("%lf\t%lf\t%lf\n", img[i].trans[1],img[i].trans[0],img[i].trans[3]);
      //printf("%lf\t%lf\t%lf\n", 0.,0.,1.);
     // for(int p=0;p<4;p++)   cout<<img[i].trans[p]<<"\t";
     // cout<<endl;
      int w=width/2, h=height/2;
     // cout<<w<<" : "<<h<<endl;
      for (int y = 0; y < height; y++)
	 for (int x = 0; x < width; x++) {
	    fscanf(fp, "%d", &data);
	    {
	      t[0]=(x-w)*img[i].trans[0]-(y-h)*img[i].trans[1]+img[i].trans[2]+200;
	      t[1]=(x-w)*img[i].trans[1]+(y-h)*img[i].trans[0]+img[i].trans[3]+200;
	      //printf("<%3d,%3d> ---> <%3d,%3d>\n",x,y,(int)t_g.A[0][0], (int)t_g.A[1][0]);
	      //if(!fimg[(int)(t[1])][(int)(t[0])] )
		fimg[(int)(t[1])][(int)(t[0])]=data;
	    }
	 }
      fclose(fp);
   }
   
   fp = fopen("finalimg.asc", "w");
   for (int i = 0; i < fimg_h; i++) {
      for (int j = 0; j < fimg_w; j++)
	 fprintf(fp, "%d ", fimg[i][j]);
      fprintf(fp, "\n");
   }
   
   fclose(fp);
   showImg(fimg_w, fimg_h);
   return;
}
/*void filter(int width, int height, char **fimg)
{
   for (int i = 0; i < fimg_h; i++) {
      for (int j = 0; j < fimg_w; j++)
	 fprintf(fp, "%d ", fimg[i][j]);
      fprintf(fp, "\n");
   
}*/
void showImg(int width, int height) {
   char call[200];
   sprintf(call, "./executables/convertor.out finalimg.asc %d %d 14 0", height, width);
   //printf("%s\n",call);
   system(call);
    /*system("mv DSfile.jpg file1.jpg");
      sprintf(call,"./asc2jpg finalimg.asc %d %d 14 1", height, width);
      system(call);*/
   return;
}

int main(int argc, char *argv[]) {
   //readParticle();
    //calTransformation();
  
   /*    if (argc < 4) {
       printf("Usage: %s <particle_file's name> <width> <height>\n", argv[0]);
       pos a(1,-2 ,3), b(2, 1, 0), c(4,5,1);
       Mat A(a,b,c);
       cout<<"A"<<endl;
       A.print();
       printf("det(A): %f\n", A.det());
       Mat B=A.inv();
       B.print();
       printf("A*B: \n");
       Mat C=A*B;
       C.print();
       cout<<"t:"<<endl;
       a.show();
       a=a/3;
       a.show();
       exit(0);
       }*/
   // printf("*******Running  Map_new.cpp ******\n%s %s %s %s\n", argv[0], argv[1], argv[2], argv[3]);
    width = atoi(argv[2]);
    height = atoi(argv[3]);
    m=width>height?width:height;
    readParticle(argv[1]);
    FILE *fp;
    char name[20];
    //printMap();
    for (int i = 0; i < nImg; i++)
       {  
	  //cout<<"for Image<"<<i<<">:"<<endl;
	  sprintf(name, "results/Image%d.asc",i);
	  fp=fopen(name,"w");
	  fprintf(fp, "%d\n",img[i].nL); 
	  for(int j=0;j<img[i].nL;j++)
	    fprintf(fp, "%lf %lf\t%f %f\n", img[i].Lp_l[j][0], img[i].Lp_l[j][1], img[i].Lp_g[j][0], img[i].Lp_g[j][1]);
	  img[i].calcTrans();
	  fclose(fp);
       }
    buildImg();
    return 0;
}
