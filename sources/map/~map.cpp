#include<stdio.h>
#include<iostream>
#include<fstream>
#include<string>
#include<math.h>
#include<stdlib.h>

#define NUM_LANDMARK 10000
#define PI 3.14159265
#define NUMCLASS 14
#define FILTER_WINDOW_HALFWIDTH 1

using namespace std;
void readParticle(char *particleFile);
void printMap();
void showImg(int width, int height, char	*);
int max(int **a,int r, int c, int w);

class imgFile {
public:
  double trans[4];
};

void findMaxTransform();


imgFile *img;
int nImg = 0;
int width, height,m;
char fileName[50][50];
int fimg_h, fimg_w;

void findMaxTransform() {
   double minx=0, miny=0, maxx=img[0].trans[2], maxy=img[0].trans[3];
   for (int i = 0; i < nImg; i++) {
       if(minx>img[i].trans[2]) minx=img[i].trans[2];
       else if(maxx<img[i].trans[2]) maxx=img[i].trans[2];
       if(miny>img[i].trans[3]) miny=img[i].trans[3];
       else if(maxy<img[i].trans[3]) maxy=img[i].trans[3];
   }
   //cout<< "The maximum size of the img is:";
	// maximum.show();
    //printf(">>min: %lf, %lf\tmax: %lf %lf\n", minx, miny, maxx, maxy);
   //transform the transformations
    m=m/1.413;
    if(minx!=0 || miny!=0 || true ){
    	 minx-=m;miny-=m;
    }
    maxx+=2*m+20;
    maxy+=2*m+20;
    //printf("min: %lf, %lf\tmax: %lf %lf\n", minx, miny, maxx, maxy);
    for (int i = 0; i < nImg; i++) {
       img[i].trans[2]-=minx;
       img[i].trans[3]-=miny;
    }
    fimg_w=maxx-minx;
    fimg_h=maxy-miny;
    //printf("Final Image....\nHeight: %d\t Width: %d\n", fimg_h, fimg_w);
   return;
}


void buildImg(int numP, int maxP) {
  ifstream ifs("string_table.asc");
   while (!ifs.eof()) {
      ifs.getline(fileName[nImg], 100);
      nImg++;
   }nImg--;
   ifs.close();
   img = new imgFile[nImg]();
   
   int data;
   FILE *fp;
   fp=fopen("thetaInfo.txt", "r");
   float t4, t1,t2,t3;
   for(int i=0;i<nImg;i++){
   	for(int j=0;j<numP; j++)
		if(j==maxP) {
			fscanf(fp, "%lf%lf%f", &img[i].trans[2], &img[i].trans[3], &t4);
			//printf("%f %f %f\n", img[i].trans[2], img[i].trans[3], t4);
			img[i].trans[0]=cos(t4);
	        img[i].trans[1]=sin(t4);
 		}
		else fscanf(fp, "%f%f%f",&t1,&t2,&t3);
	}
   fclose(fp);
  
   findMaxTransform();
   int **fimg = new int*[fimg_h];
   for (int i = 0; i < fimg_h; i++) {
      fimg[i] = new int[fimg_w];
      for (int j = 0; j < fimg_w; j++)
	 fimg[i][j] = 0;
   }
   //  char filename[100];
   for (int i = 0; i < nImg; i++) {

      fp = fopen(fileName[i], "r");
      if (!fp) {
	 cout << fileName[i] << ": File not found error.\n" << endl;
	 break;
      }
      float t[2];
     int w=width/2, h=height/2;
     //printf("Image<%d>...Height: %d\t Width: %d\n", i, w*2, h*2);
      for (int y = 0; y < height; y++)
	 for (int x = 0; x < width; x++) {
	    fscanf(fp, "%d", &data);
	    {
	      t[0]=(x-w)*img[i].trans[0]-(y-h)*img[i].trans[1]+img[i].trans[2]+10;
	      t[1]=(x-w)*img[i].trans[1]+(y-h)*img[i].trans[0]+img[i].trans[3]+10;
	      //printf("<%3d,%3d> ---> <%3d,%3d>\n",x,y,(int)t_g.A[0][0], (int)t_g.A[1][0]);
	      //if(!fimg[(int)(t[1])][(int)(t[0])] )
		fimg[(int)(t[1])][(int)(t[0])]=data;
	    }
	 }
      fclose(fp);
   }
/*
   fp = fopen("finalimg.asc", "w");
   for (int i = 0; i < fimg_h; i++) {
      for (int j = 0; j < fimg_w; j++)
	 fprintf(fp, "%d ", fimg[i][j]);
      fprintf(fp, "\n");
   }
   fclose(fp);
   showImg(fimg_w, fimg_h,  (char *)"finalimg.asc");   system("mv out.ppm Unfiltered.ppm");*/
   //filtering
   fp = fopen("filtered.asc", "w");
   int filt_w=FILTER_WINDOW_HALFWIDTH;
   for (int i = filt_w; i < fimg_h - filt_w; i++) {
      for (int j = filt_w; j < fimg_w - filt_w; j++)
      	if(!fimg[i][j])
      		fprintf(fp, "%d ", max(fimg, i,j,filt_w));
		else fprintf(fp, "%d ", fimg[i][j]);
		fprintf(fp,"\n");
	}
   showImg(fimg_w - filt_w*2, fimg_h -filt_w*2, (char *)"filtered.asc");

   return;
}

int max(int **a,int r, int c, int w){
	int t[NUMCLASS]={0};
	int max=0;
	for (int i = r-w; i < r+w; i++) 
      for (int j = c-w; j < c+w; j++)
			t[a[i][j]]++;
			
	for(int i=1;i<NUMCLASS;i++)
		if(t[i]>t[max]) max=i;
	return max;
}

void showImg(int width, int height, char *fileName) {
   char call[200];
   sprintf(call, "./executables/convertor.out  %s %d %d %d 0", fileName, width, height, NUMCLASS);
   system(call);
   return;
}

int main(int argc, char *argv[]) {
    if (argc < 3) {
    printf("Usage: %s <width> <height>\n", argv[0]);
    return 0;
    }
    width = atoi(argv[1]);
    height = atoi(argv[2]);
    int numP=atoi(argv[3]);
    int maxP=atoi(argv[4]);
    
    m=width>height?width:height;
    //readParticle(argv[1]);
    buildImg(numP, maxP);
    return 0;
}
