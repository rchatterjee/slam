#include <vector>
#include <stdlib.h>
#include <highgui.h>
#include <cv.h>
#include <iostream>
#include <stdio.h>
#include <math.h>

#define MAX_NUM_MOVE 100
#define SCALE 2
using namespace std;
using namespace cv;

const Scalar RED = Scalar(0, 0, 255);
const Scalar PINK = Scalar(230, 130, 255);
const Scalar BLUE = Scalar(255, 0, 0);
const Scalar LIGHTBLUE = Scalar(255, 255, 160);
const Scalar GREEN = Scalar(0, 255, 0);

enum {
	NOT_SET = 0, IN_PROCESS = 1, SET = 2
};
//void showOriginal(int numfiles, char *files, int w, int h);

class pose {
public:
	CvPoint p;
	float angle;
	pose(CvPoint p1 = cvPoint(0, 0), float ang = 0.0) {
		p = cvPoint(p1.x, p1.y);
		angle = ang;
	}
	pose(int x, int y, float ang) {
		p = cvPoint(x, y);
		angle = ang;
	}
};

// Rotate the rectangle for incorpoarating the angule change
CvPoint *RotateRect(CvPoint r, float angle, CvSize s) {
	CvPoint *p = new CvPoint[4];
	int w = s.width / 2, h = s.height / 2;
	float ct = cos(angle), st = sin(angle);
	p[0] = cvPoint(r.x + ((-w) * ct - (-h) * st),
					r.y + ((-w) * st + (-h) * ct));
	p[1] = cvPoint(r.x + ((-w) * ct - h * st), r.y + ((-w) * st + h * ct));
	p[2] = cvPoint(r.x + (w * ct - h * st), r.y + (w * st + h * ct));
	p[3] = cvPoint(r.x + (w * ct - (-h) * st), r.y + (w * st + (-h) * ct));
	return p;
}

float findAngle(CvPoint p1, CvPoint p2) {
	return atan2(p2.y - p1.y, p2.x - p1.x);
}
float findDist(CvPoint p1, CvPoint p2) {
	float d;
	d = (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
	return sqrt(d);
}

class GUI {
public:
	IplImage *img;
	pose *mv_points; //mv_points[0]=start point
	CvSize cutImg_s;
	CvSize cameraImg_s;
	int nMove;
	char sizes[MAX_NUM_MOVE * 8];
private:
	CvPoint **pt;
	int radius, lineWidth;
	CvRect start;
	char *windowName;
	int decideState, movementState;
public:
	GUI(char *imgName = NULL, char *winName = NULL) {
		img = cvLoadImage(imgName);
		windowName = new char[strlen(winName)];
		strcpy(windowName, winName);
		if (!img) {
			printf("File Loading Error in GUI_new.\n");
			exit(0);
		}
		mv_points = new pose[MAX_NUM_MOVE];
		pt = new CvPoint*[MAX_NUM_MOVE];

		lineWidth = 2;
		radius = 3;
		for (int i = 0; i < MAX_NUM_MOVE; i++)
			pt[i] = new CvPoint[4];
		nMove = 0;
		decideState = movementState = NOT_SET;
		cvShowImage(windowName, img);
	}

	void reset() {
		nMove = 0;
		decideState = movementState = NOT_SET;
		cutImg_s = cvSize(0, 0);
	}

	void showImage(int x = 0, int y = 0) {	// Display the image
		CvArr *res = cvCloneImage(img);
		if (decideState + movementState) {
			if (decideState == IN_PROCESS) {
				cvRectangle(res, cvPoint(start.x, start.y), cvPoint(x, y),
						GREEN, lineWidth);
			} else if (decideState == SET) {
				int nCurvePts[nMove];
				for (int i = 0; i < nMove; i++) {
					nCurvePts[i] = 4;
					cvCircle(res, mv_points[i].p, radius, GREEN, lineWidth);
					if (i)
						cvLine(res, mv_points[i - 1].p, mv_points[i].p, RED,
								lineWidth);
				}
				cvPolyLine(res, pt, nCurvePts, nMove, 1, BLUE, lineWidth);
			}
			int nCurvePts[1] = { 4 };
			if (movementState == IN_PROCESS) {		// If the initial size of the cut image, The reference ho has not been set
				CvPoint *x[1] = { RotateRect(mv_points[nMove].p,
						mv_points[nMove].angle, cutImg_s) };
				cvPolyLine(res, x, nCurvePts, 1, 1, LIGHTBLUE, lineWidth);
			} else if (movementState == SET) {		// Display the rotated rectangle
				CvPoint *x[1] = { RotateRect(mv_points[nMove - 1].p,
				mv_points[nMove - 1].angle, cutImg_s) };
				cvPolyLine(res, x, nCurvePts, 1, 1, PINK, lineWidth);
				cvCircle(res, mv_points[nMove - 1].p, radius, RED, lineWidth);
			} else {								// 
				CvPoint *x[1] = { RotateRect(mv_points[nMove - 1].p,
						mv_points[nMove - 1].angle, cutImg_s) };
				cvPolyLine(res, x, nCurvePts, 1, 1, PINK, lineWidth);
				cvCircle(res, mv_points[nMove - 1].p, radius, BLUE, lineWidth);
			}
		}
		cvShowImage(windowName, res);
		cvRelease(&res);
	}
	int t;
	
	/*
	* Event handler for mouse click
	* Set the initial rectangle anf the cut images
	*/
	void mouseClick(int event, int x, int y, int flags, void* param) {
		switch (event) {
		case CV_EVENT_LBUTTONDOWN: {
			if (decideState == NOT_SET) {
				decideState = IN_PROCESS;
				start = cvRect(x, y, 1, 1);
			} else if (decideState == SET && movementState == IN_PROCESS) {
				movementState = SET;
//				mv_points[nMove] = pose(x, y, 0.0);
				mv_points[nMove] = pose(x, y, findAngle(mv_points[nMove - 1].p, cvPoint(x, y)));
				pt[nMove] = RotateRect(mv_points[nMove].p,
						mv_points[nMove].angle, cutImg_s);
				nMove++;
				sprintf(sizes, "%s %d %d", sizes, (int)(cutImg_s.width),
						(int)(cutImg_s.height));
			}
		}
			break;
		case CV_EVENT_LBUTTONUP: {
			if (decideState == IN_PROCESS) {
				mv_points[nMove] = pose((start.x + x) / 2, (start.y + y) / 2,
						0.0);
				cutImg_s = cvSize(fabs(x - start.x), fabs(y - start.y));
				cameraImg_s = cvSize(fabs(x - start.x), fabs(y - start.y));
				pt[0] = RotateRect(mv_points[nMove].p, mv_points[nMove].angle,
						cutImg_s);
				decideState = SET;
				nMove++;
				sprintf(sizes, "%d %d", cutImg_s.width, cutImg_s.height);
			} else if (movementState == SET) {
				movementState = NOT_SET;
				if (nMove > MAX_NUM_MOVE - 1) {
					cout << nMove << "Exceeded" << endl;
					exit(0);
				}
			}
		}
			break;
		case CV_EVENT_MOUSEMOVE:
			if (decideState == IN_PROCESS) {
				start = Rect(Point(start.x, start.y), Point(x, y));
			} else if (decideState == SET && (movementState != SET)) {
				movementState = IN_PROCESS;
//				mv_points[nMove] = pose(x, y, 0.0);
				mv_points[nMove] = pose(x, y, findAngle(mv_points[nMove - 1].p, cvPoint(x, y)));
				pt[nMove] = RotateRect(mv_points[nMove].p,
						mv_points[nMove].angle, cutImg_s);
			}
			break;
		default:
			break;
		}
		showImage(x, y); // Redraw the image after new rectangle included.
	}
	void increaseCutSize() {
		if (cameraImg_s.width > cameraImg_s.height) {
			cutImg_s.height++;
			cutImg_s.width = cutImg_s.height * cameraImg_s.width
					/ cameraImg_s.height;
		} else {
			cutImg_s.width++;
			cutImg_s.height = cutImg_s.width * cameraImg_s.height
					/ cameraImg_s.width;
		}
		showImage(0, 0);
	}
	void decreaseCutSize() {
		if (cameraImg_s.width > cameraImg_s.height && cameraImg_s.height > 2) {
			cutImg_s.height--;
			cutImg_s.width = cutImg_s.height * cameraImg_s.width
					/ cameraImg_s.height;
		} else if (cameraImg_s.width < cameraImg_s.height && cameraImg_s.width
				> 0) {
			cutImg_s.width--;
			cutImg_s.height = cutImg_s.width * cameraImg_s.height
					/ cameraImg_s.width;
		}
		showImage(0, 0);
	}
};

void usage() {
	printf("<numrawimg> <rawimages> <width> <height>\n");
	printf("Please Note: Max num of cut image = %d\n", MAX_NUM_MOVE);
	exit(0);
	return;
}

GUI *g;

void on_mouse(int event, int x, int y, int flags, void* param) {
	g->mouseClick(event, x, y, flags, param);
}

double gaussian_rand(){
	double U, V, w, y1, y2;
	do {
		U = 2.0 * (rand()%(10000+1) / 10000.0) - 1.0;
		V = 2.0 * (rand()%(10000+1) / 10000.0) - 1.0;
		w = U*U + V*V;
	} while (w >= 1.0);
	w = sqrt( (-2.0*log(w)) / w );
	return U * w;
}

int main(int argc, char** argv) {
	if (argc < 3)
		usage();
	char rawfiles[1000];
	int numrawimg, height, width;
	numrawimg = atoi(argv[1]);
	sprintf(rawfiles, "");
	for (int i = 0; i < numrawimg; i++)
		sprintf(rawfiles, "%s%s ", rawfiles, argv[i + 2]);
	height = atoi(argv[3 + numrawimg]);
	width = atoi(argv[2 + numrawimg]);

	char command[1000];
	sprintf(command, "./executables/raw2jpg.out 3 %s %s %s %d %d out.jpg",
			argv[2], argv[3], argv[4], width, height);
	cout << command << endl;

	system(command);

	cvNamedWindow("Full Image", 1);
	cvMoveWindow("Full Image", 10, 10);
	IplImage *org = cvLoadImage("out.jpg");
	IplImage *resized = cvCreateImage(
	cvSize(org->width / SCALE, org->height / SCALE), 8, 3);	// Resize image for proper display of the image within the image dimensions
	cvResize(org, resized);
	cvSaveImage("out.jpg", resized);						// Save the resized image.
	cvReleaseImage(&org);
	cvReleaseImage(&resized);

	g = new GUI((char *) "out.jpg", (char *) "Full Image");
	g->showImage();
	cvSetMouseCallback("Full Image", on_mouse, 0);
	g->showImage();

	FILE* fp;
	float r = 0;
	int c = 0;
	int numParts = 10;
	
	do {
		switch ((char) c) {
		case '\x1b':	// on press esc exit
			cout << "Exiting ..." << endl;
			exit(0);
		case 'r':		// Reset on press r
			cout << endl;
			g->reset();
			g->showImage();
			break;
		case 'n':
		case 'N':	// Run slam after cutting images
		
			for (int i=0; i < g->nMove; i++){
				printf("%d: (%d, %d, %f)\n", (i+1), g->mv_points[i].p.x, g->mv_points[i].p.y, g->mv_points[i].angle);
			}
			char s[1000];
			int k;
			fp = fopen("sizes.txt", "w");
			fprintf(fp, "%s", g->sizes);
			fclose(fp);

			sprintf(s, "./segmentation.sh");
			sprintf(s, "%s %d \'%s\' %d %d", s, numrawimg, rawfiles, width,
					height);
			sprintf(s, "%s %d %d %d \'", s, g->cameraImg_s.width * SCALE,
					g->cameraImg_s.height * SCALE, g->nMove - 1);
			sprintf(s, "%s%d %d %d", s, g->mv_points[0].p.x * SCALE,
					g->mv_points[0].p.y * SCALE, 0);
			fp = fopen("realpath", "w");
			fprintf(fp, "%d\n%d %d\n0 0 0\n", g->nMove, 
				g->cutImg_s.width * SCALE, g->cutImg_s.height *SCALE);
			for (int i = 1; i < g->nMove; i++){
				fprintf(fp, "%d ",
						(g->mv_points[i].p.x - g->mv_points[i - 1].p.x) * SCALE);
				fprintf(fp, "%d ",
						(g->mv_points[i].p.y - g->mv_points[i - 1].p.y) * SCALE);
				fprintf(fp, "%f\n",
						g->mv_points[i].angle - g->mv_points[i - 1].angle);
			}
			fclose(fp);
		case 'o':
		case 'O':
			double error_val;
			printf("Input number of particles:");
			scanf("%d", &numParts);
			printf("Input translational error:");
			scanf("%lf", &error_val);
			double x1, x2, y1, y2, angle;
			fp = fopen("control", "w");
			fprintf(fp, "%d\n%d %d\n0 0 0\n", g->nMove,
					g->cutImg_s.width * SCALE, g->cutImg_s.height * SCALE);
			for (int i = 1; i < g->nMove; i++) {
				r = findDist(g->mv_points[i].p, g->mv_points[i - 1].p);
//				float r_angle = findAngle(g->mv_points[i-1].p, g->mv_points[i].p);
				sprintf(s, "%s %f %f", s, r * SCALE, // r_angle);
						g->mv_points[i].angle - g->mv_points[i - 1].angle);
	/*			r *= (1 + error_val/100.0*gaussian_rand())*SCALE;
				double th_er = 0.0; //1*error_val*gaussian_rand();
				angle = (g->mv_points[i].angle - g->mv_points[i - 1].angle) + th_er;
				fprintf(fp, "%lf %lf %lf\n", r*cos(g->mv_points[i-1].angle + angle), r*sin(g->mv_points[i-1].angle + angle), angle);
*/	
				fprintf(fp, "%f ",
						(g->mv_points[i].p.x - g->mv_points[i - 1].p.x)*SCALE*(1.0 + abs(error_val*gaussian_rand()/100.0)));
				fprintf(fp, "%f ",
						(g->mv_points[i].p.y - g->mv_points[i - 1].p.y)*SCALE*(1.0 + abs(error_val*gaussian_rand()/100.0)));
				fprintf(fp, "%f\n",
						(g->mv_points[i].angle-g->mv_points[i - 1].angle)*(1.0 + 0.0*error_val*gaussian_rand()/10000.0));
				
			}
			fclose(fp);
			sprintf(s, "%s\'", s);
			printf("%s\n", s);
			system(s);
			sprintf(s, "./executables/zoomasc.out %d %f", g->nMove, 0.0); // note: scaling error incorporated here
			system(s);
			cout << g->sizes << endl;
		case 10:
		case 'p':
		case 'P':
			if ((char)c=='p' || (char)c=='P'){
				printf("Input number of particles:");
				scanf("%d", &numParts);
				printf("Input error : ");
				scanf("%lf", &error_val);
			}
			sprintf(s, "./executables/run.out results/ %f %d", error_val, numParts);
			system(s);	// run the slam on the cu images already present
			break;
		case 's':
			g->increaseCutSize();	// Increase the image cutsize as if the robot is moving down
			break;
		case 'a':
			g->decreaseCutSize();	// Decreset the image cutsize as if the robot is moving up
			break;
		}
	} while ((c = cvWaitKey(0)) != 27);

	exit_main: cvDestroyWindow("Full Image");
	return (EXIT_SUCCESS);
}
