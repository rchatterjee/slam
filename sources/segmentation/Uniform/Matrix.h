#ifndef	_MATRIX_H
#define	_MATRIX_H	1000

// Only meant for primitive type
template <typename T>
class CMatrix
{
public:
    CMatrix(int nWidth = 0, int nHeight = 0)
        :m_nWidth(0),  m_nHeight(0),  m_ppT(NULL)
    {
    	Construct(nWidth, nHeight);
    }
	virtual ~CMatrix()
    {
        Clear();
    }
private:
	int	m_nWidth;
	int	m_nHeight;
	T	**m_ppT;

public:
    void	Clear()
    {
	    if(m_ppT)
	    {
            for(int nIndex = 0; nIndex < m_nHeight; nIndex++)
			    free(m_ppT[nIndex]);
		    free(m_ppT);
		    m_ppT = NULL;
	    }
	    m_nWidth = 0;
	    m_nHeight = 0;
    }
	int	GetWidth()const { return m_nWidth; };
	int GetHeight()const { return m_nHeight; };
	
    bool Construct(int nWidth, int nHeight)
    {
	    Clear();
        m_ppT = (T **)malloc(nHeight * sizeof(T *));
	    if(NULL == m_ppT)
		    return false;
	    m_nHeight = nHeight;
	    for(int nIndex = 0; nIndex < m_nHeight; nIndex++)
        {
		    m_ppT[nIndex] = (T *)malloc(nWidth * sizeof(T));
            if(NULL == m_ppT[nIndex])
            {
                for(int n = 0; n < nIndex; ++n)
                    free(m_ppT[n]);
                free(m_ppT);
                m_ppT = NULL;
                return false;
            }
        }
	    m_nWidth = nWidth;
	    return true;
    }

    inline T *operator [](int nIndex) { return m_ppT[nIndex]; }
    inline const T *operator [](int nIndex)const { return m_ppT[nIndex]; }
};

#endif	//of _MATRIX_H;
