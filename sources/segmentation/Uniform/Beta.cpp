// Beta.cpp: implementation of the CBeta class.
//
//////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cstdio>
#include <cmath>
#include <string.h>
#include "Image.h"
#include "Beta.h"
using namespace std;

static	double	Th[][9] =
{
	{29.60, 27.29, 27.29, 24.88, 24.88, 22.35, 22.35, 22.35, 19.63},
	{23.10, 21.00, 21.00, 18.90, 18.90, 16.50, 16.50, 16.50, 14.00},
	{24.99, 22.84, 22.84, 20.62, 20.62, 18.28, 18.28, 18.28, 15.79},
	/*******/
	{17.76, 15.92, 15.92, 14.03, 14.03, 12.07, 12.07, 12.07, 9.99},
	/*********/
	{13.20, 11.10, 11.10, 10.20, 10.20, 8.10, 8.1, 8.1, 5.9},
};

static	double	Nr_th[][9] =
{
	/*  0   1     2      3   |   4      5       6     7     8 */
	{0.0, 12.67, 15.03, 17.23, 19.63, 22.35, 24.88, 27.29, 29.60},
	{0.0,  8.43, 10.95, 13.23, 15.79, 18.28, 20.62, 22.84, 24.99},
	{0.0,  7.02,  9.23, 11.65, 14.00, 16.50, 18.90, 21.00, 23.10},
	{0.0,  3.02,  5.34,  7.65,  9.99, 12.07, 14.03, 15.92, 17.76},
	{0.0,  0.02,  1.34,  3.65,  5.90,  8.10, 10.20, 11.10, 13.20},
};

#define	ZERO	1.0e-20
#define	INFIN	1.0e20

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CImage	*CBeta::m_pImage = NULL;
CImage	*CBeta::m_pOut = NULL;
CParams	*CBeta::m_pParams = NULL;
char *CBeta::m_outImageName = NULL;
double	CBeta::m_aB[3][3];
double	CBeta::m_aS[3][3];
int	CBeta::m_nLevel= 0;

CBeta::CBeta()
{
}

CBeta::~CBeta()
{

}

void CBeta::SetNairLevel(int nLevel)
{
	nLevel %= (sizeof(Nr_th)/(9*sizeof(double)));
	m_nLevel = nLevel;
}

void CBeta::SetOutImageName(char *name) {
	m_outImageName=new char[strlen(name)+1];
	strncpy(m_outImageName,name,strlen(name)+1);
	//cout << "Output Image: " << m_outImageName << endl;
}

CImage * CBeta::AttachImage(CImage * pImage)
{
	CImage *pRet = m_pImage;
	m_pImage = pImage;
	return pRet;
}

CImage * CBeta::AttachOutImage(CImage * pImage)
{
	CImage *pRet = m_pOut;
	m_pOut = pImage;
	return pRet;
}

CParams * CBeta::AttachParams(CParams *pImage)
{
	CParams *pRet = m_pParams;
	m_pParams = pImage;
	return pRet;
}

double CBeta::GetBeta(int x, int y, int i, int j, double *pdError)
{
/////////////////////////////////////////////////////////////////
//         1    1
// temp = Sum  Sum  (Tmplt[k][i] * G(x+l, y+k) * Tmplt[l][j])
//        k=-1 l=-1
//
// B[i][j](x, y) = temp / Vars[i][j];
//
// E[i][j](x, y) = (temp * temp) / Vars[i][j];
//
////////////////////////////////////////////////////////////////
	static const double Vars[3][3] = 
	{
		{  9.0,  6.0, 18.0},
		{  6.0,  4.0, 12.0},
		{ 18.0, 12.0, 36.0},
	};

	static const double Tmplt[3][3] = 
	{
		{  1.0, -1.0,  1.0},
		{  1.0,  0.0, -2.0},
		{  1.0,  1.0,  1.0},
	};

	double	temp = 0.0;
	for(int k = 0; k < 3; k++)
		for(int l = 0; l < 3; l++)
			temp += (Tmplt[k][i] * Tmplt[l][j] * 
					(double)(GetImage()->GetPixel(x + l - 1, y + k - 1)));
	if(fabs(temp) < ZERO)
		temp = 0.0;
	if(pdError)
		(*pdError) = (temp * temp) / Vars[i][j];
	temp /= (Vars[i][j]);
	return temp;
}

void CBeta::GetAllBeta(int x, int y)
{
	double	err;
	int	i, j;
	if( (x < 1) || (y < 1) || 
		((x + 2) > GetImage()->GetImageWidth()) || 
		((y + 2) > GetImage()->GetImageHeight()))
	{
		for( i = 0; i < 3; i++)
		{
			for( j= 0; j < 3 ; j++)
			{
				m_aB[i][j] = INFIN;
				m_aS[i][j] = INFIN;
				return;
			}
		}
	}
	for(i = 0; i < 3; i++)
	{
		for(j = 0; j < 3; j++)
		{
			m_aB[i][j] = GetBeta(x, y, i, j, &err);
			m_aS[i][j] = err;//m_aB[i][j] * m_aB[i][j];
		}
	}
}

/*
cls In-class set                out-of-class set
U1	{b00, b01, b10, b12}		{b02, b11, b20, b21, b22}
U2	{b00, b01, b10, b21}		{b02, b11, b12, b20, b22}
U3	{b00, b01, b10, b21, b12}	{b02, b11, b20, b22}
U4	{b00, b11, b02}				{b01, b10, b12, b20, b21, b22}
U5	{b00, b11, b20}				{b01, b02, b10, b12, b21, b22}
U6	{b00, b11, b02, b20, b22}	{b01, b10, b12, b21}
U7	{b00, b02, b20, b22}		{b01, b10, b11, b12, b21}
U8	{b00}						{b01, b02, b10, b11, b12, b20, b21, b22}
U9	{b00, b10, b01}				{b02, b11, b12, b20. b21, b22}
*/

static	const	int	seq[9] 	= {7, 3, 4, 8, 5, 6, 0, 1, 2};

//Call GetAllBeta() before calling this function.
//Otherwise, the result is unpredictable
bool CBeta::IsInClass(int nClassID)
{
	double	am = 0.0, gm = 1.0;
	int	i;
	static	const	int	Num[9] = { 8, 7, 7, 6, 6, 5, 5, 5, 4};
	typedef struct tagNode
	{
		int	i, j;
	}Node;
	static	const	Node E[9][9] = 
	{
		{ {1, 0}, {0, 1}, {1, 1}, {2, 0}, {0, 2}, {2, 1}, {1, 2}, {2, 2}, {0, 0}},
		{ {1, 0}, {0, 1}, {1, 1}, {2, 0}, {2, 1}, {1, 2}, {2, 2}, {0, 0}, {0, 2}},
		{ {1, 0}, {0, 1}, {1, 1}, {0, 2}, {2, 1}, {1, 2}, {2, 2}, {0, 0}, {2, 0}},
		{ {1, 1}, {2, 0}, {0, 2}, {2, 1}, {1, 2}, {2, 2}, {0, 0}, {1, 0}, {0, 1}},
		{ {1, 0}, {0, 1}, {2, 0}, {0, 2}, {2, 1}, {1, 2}, {0, 0}, {1, 1}, {2, 2}},
		{ {1, 0}, {0, 1}, {1, 1}, {2, 1}, {1, 2}, {0, 0}, {2, 0}, {0, 2}, {2, 2}},
		{ {1, 1}, {2, 0}, {0, 2}, {2, 1}, {2, 2}, {0, 0}, {1, 0}, {0, 1}, {1, 2}},
		{ {1, 1}, {2, 0}, {0, 2}, {1, 2}, {2, 2}, {0, 0}, {1, 0}, {0, 1}, {2, 1}},
		{ {1, 1}, {2, 0}, {0, 2}, {2, 2}, {0, 0}, {1, 0}, {0, 1}, {2, 1}, {1, 2}},
	};
	int	n = Num[nClassID];

	am = 0.0, gm = 0.0;
	for(i = 0; i < n; i++)
	{
		double dT;
		dT = m_aS[E[nClassID][i].i][E[nClassID][i].j];
		am += (dT);
		if(dT > ZERO)
			gm += log(dT);
	}
	am /= (double)n;
	double Nr = 0.0;
	if( am > ZERO)
	{
		am = log(am);
		am *= (double)n;
		Nr = (am - gm);
	}
	if(fabs(Nr) < Nr_th[GetNairLevel()][n])
		return true;
	return false;
}

int CBeta::Classify(int x, int y)
{
	int	i;

	GetAllBeta(x, y);
	for(i = 0; i < 9; i++)
	{
		if(IsInClass(i))
		{
			if(m_pParams)
			{
				if(i == 0 || i == 3)
				{
					double Nr;
					Nr = m_aS[0][2];
					Nr += (m_aS[1][1] + m_aS[1][2]);
					Nr += (m_aS[2][0] + m_aS[2][1] + m_aS[2][2]);
					(*m_pParams)[y][x][3] = Nr;
					(*m_pParams)[y][x][0] = m_aB[0][0];
					(*m_pParams)[y][x][1] = m_aB[1][0];
					(*m_pParams)[y][x][2] = m_aB[0][1];
				}
			}
			return seq[i];
		}
	}
	return -1;
}

const char * CBeta::GetClassString(int nClassID)
{
	const static	char name[10][30] = {
		{"Horizontal Step Edge"},
		{"Vertical Step Edge"},
		{"Diagonal Step Edge"},
		{"Horizontal Line Edge"},
		{"Vertical Line Edge"},
		{"Diagonal Line Edge"},
		{"Dot Edge"},
		{"Flat facet"},
		{"Sloped facet"},
		{"Unclassified"},
	};
	if(nClassID < 0 || nClassID > 8)
		return name[9];
	return name[nClassID];
}

static	const	int	nMap[10] = 
//{ 255, 255, 255, 255, 255, 255, 255, 100, 200, 255};
{ 25, 50, 75, 100, 125, 150, 175, 200, 225, 255};

bool CBeta::IsPixelUnclassified(int x, int y)
{
	if(GetOutImage()->GetPixel(x, y) == 9)
		return true;
	return false;
}

bool CBeta::IsRegionPixel(int x, int y)
{
	int nType = GetOutImage()->GetPixel(x, y);
	if(nType == 7 || nType == 8)
		return true;
	return false;
}

bool CBeta::SaveImageAfterFirstStage(char * szFileName)
{
	int	nPixel;
	int	nWidth = GetOutImage()->GetImageWidth();
	int nHeight = GetOutImage()->GetImageHeight();
	CImage *pImage = new CImage(nWidth, nHeight);
	for(int nY = 0; nY < nHeight; nY++)
	{
		for(int nX = 0; nX < nWidth; nX++)
		{
			if(IsPixelUnclassified(nX, nY))
				nPixel = 128;
			else
			{
				nPixel = GetOutImage()->GetPixel(nX, nY);
				nPixel = (nPixel == 7 || nPixel == 8) ? 255 : 0;
			}
			pImage->SetPixel(nX, nY, nPixel);
		}
	}
	pImage->SaveImageAs(szFileName);
	delete pImage;
	return true;
}

void CBeta::FirstStage()
{
	if(!GetOutImage())
		return;
	int	nW, nH;
	nW = GetOutImage()->GetImageWidth();
	if( nW != (int)GetImage()->GetImageWidth())
		return;
	nH = GetOutImage()->GetImageHeight();
	if( nH != (int)GetImage()->GetImageHeight())
		return;
	int	nX, nY, nType;
	for(nY = 0; nY < nH; nY++)
	{
		for(nX = 0; nX < nW; nX++)
		{
			GetOutImage()->SetPixel(nX, nY, 9);
			if(m_pParams)
			{
				(*m_pParams)[nY][nX][0] = 0;
				(*m_pParams)[nY][nX][1] = 0;
				(*m_pParams)[nY][nX][2] = 0;
				(*m_pParams)[nY][nX][3] = 0;
			}
		}
	}
	//printf("First Stage : ");
	for(nY = 1; nY < nH - 1; nY++)
	{
		if(nY % 16 == 0)
		{
			//printf(".");
			fflush(stdout);
		}
		for(nX = 1; nX < nW - 1; nX++)
		{
			nType = Classify(nX, nY);
			if(nType < 0 || nType > 9)
				nType = 9;
			GetOutImage()->SetPixel(nX, nY, nType);
		}
	}
	//printf("Done.\n");
	GetOutImage()->SaveImageAs("class.asc", CImage::ftText);
	SaveImageAfterFirstStage((char*)"First.bin");
}

double CBeta::BestNeighbor(int x, int y, int & nX, int & nY)
{
	int	xx,yy;
	double dErr;

	nX = x, nY = y;
	dErr = (*m_pParams)[y][x][3];
	for(int i = 0; i < 3; i++)
	{
		yy = y - 1 + i;
		for(int j = 0; j < 3; j++)
		{
			xx = x - 1 + j;
			if(!IsRegionPixel(xx, yy))
				continue;
			if((*m_pParams)[yy][xx][3] < dErr)
			{
				dErr = (*m_pParams)[yy][xx][3];
				nX = xx, nY = yy;
			}
		}
	}
	return dErr;
}

//const double Fcr[] = {99.9%, 99.5%, 99.00%, 95.0%, 90.0%};

//static	const double Fcr[] = {18.643, 11.754, 9.33, 4.75, 3.18};
static const double Fcr[]={10.804, 7.23, 5.95, 3.49, 2.61};
//Levels are corresponding to 99.9, 99.5, 99, 95, 90 }
//First row corresponding to 1 and 12 df and second row corresponding to 3 and 12 df.

double	CBeta::m_fFcr = Fcr[0];

void CBeta::SetFTestLevel(int nLevel)
{
	nLevel %= (sizeof(Fcr)/sizeof(double));
	SetFTestValue(Fcr[nLevel]);
}

bool CBeta::F_Test(int xx1, int yy1, int xx2, int yy2)
{
	double	dx, dy, dbo, dbi, dboo;
	double	mseff, mserr, F, dE1, dE2;
	int	x1, y1, x2, y2;

	dE1 = BestNeighbor(xx1, yy1, x1, y1);
	dE2 = BestNeighbor(xx2, yy2, x2, y2);

	if(x1 < 0 || x2 < 0 || x1 >= m_pParams->GetWidth() || x2 >= m_pParams->GetWidth())
		return false;
	if(y1 < 0 || y2 < 0 || y2 >= m_pParams->GetHeight() || y1 >= m_pParams->GetHeight())
		return false;
	if((x1 == x2) && (y1 == y2))
		return false;
	if( ((xx1 - x1) == (xx2 - x2)) && ((yy1 - y1) == (yy2 - y2)) )
		return false;
	dx = x1 - x2;
	dx = fabs(dx)/2.0;
	dy = y1 - y2;
	dy = fabs(dy)/2.0;
	
	dbo = (*m_pParams)[y1][x1][1] - (*m_pParams)[y2][x2][1];
	dbi = (*m_pParams)[y1][x1][2] - (*m_pParams)[y2][x2][2];
	dbo *= dbo;
	dbi *= dbi;

	mseff = 3.0 * (dbo + dbi);

	dbo = (*m_pParams)[y1][x1][1] + (*m_pParams)[y2][x2][1];
	dbi = (*m_pParams)[y1][x1][2] + (*m_pParams)[y2][x2][2];
	dboo= (*m_pParams)[y1][x1][0] - (*m_pParams)[y2][x2][0];

	dbo *= dx;
	dbi *= dy;

	dbo += (dbi + dboo);
	dbo *= dbo;

	dbi = ((dx * dx) / 6.0 + (dy * dy) / 6.0 + 1.0 / 9.0) * 2.0;
	dbo /= dbi;
	
	mseff+= dbo;

	//mserr = (*m_pParams)[y1][x1][3] + (*m_pParams)[y2][x2][3];
	mserr = dE1 + dE2;

	if(mserr > ZERO)
		F = (mseff * 4.0) / mserr;
	else
		F = INFIN;

	if(F > m_fFcr)
		return true;
	return false;
}

bool CBeta::IsInSameRegion(int x1, int y1, int x2, int y2)
{
//	int	xx1,yy1,xx2,yy2;

	if(!IsRegionPixel(x1, y1))
		return false;
	if(!IsRegionPixel(x2, y2))
		return false;
	
	if(F_Test(x1, y1, x2, y2))
		return false;
	return true;
}

bool CBeta::TestPixel(int x, int y, int clr)
{
	int	i,j;
	if(!IsRegionPixel(x, y))
		return false;

	for(i = y - 1; i <= y + 1; i++)
	{
		if( i < 1 || i > (int)GetImage()->GetImageHeight() - 2)
			continue;
		for(j = x - 1; j <= (x + 1); j++)
		{
			if( j < 1 || j > (int)GetImage()->GetImageWidth() - 2)
				continue;
			if( i == y && j == x)
				continue;
			if(GetImage()->GetPixel(j, i) < 1)
			{
				if(IsInSameRegion(x, y, j, i))
					return true;
			}
		}
	}
	return false;
}

bool CBeta::SecondStage()
{
	CImage imgO(GetImage()->GetImageWidth(), GetImage()->GetImageHeight());
	CImage imgI(GetImage()->GetImageWidth(), GetImage()->GetImageHeight());
	int i, j;
	CSecondStage stg2nd;
	for(i = 0; i < (int)GetImage()->GetImageHeight(); i++)
		for(int j = 0; j < (int)GetImage()->GetImageWidth(); j++)
			imgI.SetPixel(j, i, GetOutImage()->GetPixel(j, i));
	stg2nd.m_pOut = &imgO;
	stg2nd.AttachImage(&imgI);
	int	nPixel = 0;
	//printf("Second Stage : ");
	for(i = 1; i < (int)imgI.GetImageHeight() - 1; i++)
	{
		if(i % 16 == 0)
		{
			//printf(".");
			fflush(stdout);
		}
		for(j = 1; j < (int)imgI.GetImageWidth() - 1; j++)
		{
			if(!IsRegionPixel(j, i) || imgO.GetPixel(j, i) > 0)
				continue;
			nPixel = imgO.GetNextRegionColor(j, i, nPixel, false);
			stg2nd.ScanFill(j, i, nPixel, imgI.GetPixel(j, i));
		}
	}
	for(i = 0; i < (int)imgO.GetImageWidth(); i++)
	{
		imgO.SetPixel(i, 0, 0);
		imgO.SetPixel(i, imgO.GetImageHeight() - 1, 0);
	}
	for(i = 0; i < (int)imgO.GetImageHeight(); i++)
	{
		imgO.SetPixel(0, i, 0);
		imgO.SetPixel(imgO.GetImageWidth() - 1, i, 0);
	}
	//printf("Done.\n");
	/*imgO.SaveImageAs("second.bin");
	imgO.SaveImageAs("second.asc", CImage::ftText);*/
	imgO.SaveImageAs(CBeta::m_outImageName, CImage::ftText);
	return true;
}

void CSecondStage::SetPixel(int x, int y, int clr)
{
	GetImage()->SetPixel(x, y, -1);
	m_pOut->SetPixel(x, y, clr);
}

bool CSecondStage::TestPixel(int x, int y, int clr, int nColor)
{
	if(x < 1 || x > (int)(GetImage()->GetImageWidth() - 2) ||
		(y < 1) || (y > (int)(GetImage()->GetImageHeight() - 2)))
		return false;
	if(m_pOut->GetPixel(x, y) > 0)
		return false;
	int	i,j;
	if(!CBeta::IsRegionPixel(x, y))
		return false;

	for(i = y - 1; i <= y + 1; i++)
	{
		for(j = x - 1; j <= (x + 1); j++)
		{
			if( i == y && j == x)
				continue;
			if(m_pOut->GetPixel(j, i) == clr)
			{
				if(CBeta::IsInSameRegion(x, y, j, i))
					return true;
			}
		}
	}
	return false;
}

