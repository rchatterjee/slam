// ScanFill.cpp: implementation of the CScanFill class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "Image.h"
#include "ScanFill.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

using namespace std;

CScanFill::CScanFill()
:X(0), Y(0)
{
	m_pImg = NULL;
}

CScanFill::~CScanFill()
{
}

void CScanFill::AttachImage(CImage *pImg)
{
	m_pImg = pImg;
	if(NULL != pImg)
	{
		X = pImg->GetImageWidth();
		Y = pImg->GetImageHeight();
	}
}

void CScanFill::SetPixel(int x, int y, int nPixel)
{
	m_pImg->SetPixel(x, y, nPixel);
}

int  CScanFill::GetPixel(int x, int y)const
{
	return m_pImg->GetPixel(x, y);
}

bool CScanFill::TestPixel(int x, int y, int nFillColor, int nOldColor)
{
	return (GetPixel(x, y) == nOldColor);
}

bool CScanFill::SetNextSeed(int xl, int xr, int y, int nFillColor, int nOldColor)
{
	if((y < 0) || (y > (Y - 1)))
		return false;
	while((xl < xr) && !TestPixel(xl, y, nFillColor, nOldColor))
		++xl;
	while(xl < xr)
	{
		while((xl < xr) && TestPixel(xl, y, nFillColor, nOldColor))
			++xl;
		if(TestPixel(xl - 1, y, nFillColor, nOldColor))
			m_stack.push(pair<int, int>(xl - 1, y));
		++xl;
		while((xl < xr) && !TestPixel(xl, y, nFillColor, nOldColor))
			++xl;
	}
	return true;
}

int CScanFill::ScanFill(int nPosX, int nPosY, int nFillColor, int nOldColor)
{
	int	x, xl, xr, sx, sy;
	int	cnt = 0;

	m_stack.push(std::pair<int, int>(nPosX, nPosY));
	while(!m_stack.empty())
	{
		sx = m_stack.top().first;
		sy = m_stack.top().second;

		SetPixel(sx, sy, nFillColor);
		cnt++;

		x = sx - 1;
		while((0 <= x) && TestPixel(x, sy, nFillColor, nOldColor))
		{
			SetPixel(x, sy, nFillColor);
			++cnt, --x;
		}
		xl = (x > 0) ? x : 0;
		x = sx + 1;
		while((x < X) && TestPixel(x, sy, nFillColor, nOldColor))
		{
			SetPixel(x, sy, nFillColor);
			++cnt, ++x;
		}
		
		xr = (x < X - 1) ? (x) : (X - 1);
		m_stack.pop();
		SetNextSeed(xl, xr, sy + 1, nFillColor, nOldColor);
		SetNextSeed(xl, xr, sy - 1, nFillColor, nOldColor);
	}
	return cnt;
}
