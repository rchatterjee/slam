//*************************************************************************
// uniform <original_file> <width> <height> initseg2.asc 0 3 -f 2.1
//************************************************************************

#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include "Image.h"
#include "Beta.h"
#include "ScanFill.h"
#include "Image.cpp"
#include "Beta.cpp"
#include "ScanFill.cpp"
#include <time.h>
using namespace std;

int DisplayUsage(int argc, char *argv[])
{
	//cout << argv[0] <<" : <file> <width> <height> ";
	//cout << "[type (1->text, 0->Binary) [Nair's Level (0->99%, 1->95%,...0-4) ";
	//cout << "[F Test Level (0->99.9%, 1->99.5%, 2->99.0%, 3->95%, 4->90.0%)] ";
	//cout << "[-F <F Test Value> ]]]" << endl;
	return -1;
}

static const Perms zero_perm = { 0.0, 0.0, 0.0, 0.0 };

main(int argc, char *argv[])
{
	clock_t start,end;
	start=clock();
	
	char	szOrgImg[_MAX_PATH] = "test.img";
	CImage::FileType	ftOrg = CImage::ftBinary;
	ulong	ulWidth = 8, ulHeight = 8;
	int		nType = 0, nFLevel = 2;
	int nLevel =1;
	double	fFVal;

	if(argc > 3)
	{
		sscanf(argv[1], "%s", szOrgImg);
		sscanf(argv[2], "%lu", &ulWidth);
		sscanf(argv[3], "%lu", &ulHeight);
	}
	else if(argc > 1)
		return DisplayUsage(argc, argv);
	else
	{
		//cout << "Original Image File : ";
		cin >> szOrgImg;
		//cout << szOrgImg << endl;
		//cout << "Image Size (horiz, vert) : ";
		cin >> ulWidth >> ulHeight;
		//cout << ulWidth << " " << ulHeight<< endl;
	}
	if(argc < 8)
		DisplayUsage(argc, argv);
	if(argc > 4)
		CBeta::SetOutImageName(argv[4]);
	if(argc > 5)
		sscanf(argv[5], "%d", &nType);
	if(argc > 6)
	{
		sscanf(argv[6], "%d", &nLevel);
		CBeta::SetNairLevel(nLevel);
	}
	if(argc > 7)
	{
		if( (strncmp(argv[7], "-F", 2) == 0) ||(strncmp(argv[7], "-f", 2) == 0) )
		{
			
			if(argc > 8)
			{
				sscanf(argv[8], "%lf", &fFVal);
				CBeta::SetFTestValue(fFVal);
			}
		}
		else
		{
			sscanf(argv[7], "%d", &nFLevel);
			fFVal=nFLevel;
			CBeta::SetFTestLevel(nFLevel);
		}
	}
	FILE *fp = fopen(szOrgImg, "rb");
	if(!fp)
	{
		perror(szOrgImg);
		return -1;
	}
	fclose(fp);

	//cout << "Original Image File : " << szOrgImg << endl;
	//cout << "Image Size (horiz, vert) : " << ulWidth << "," << ulHeight<< endl;
	//cout << "Nair Test Level : " << CBeta::GetNairLevel() << endl;
	//cout << "F Test Value : " << CBeta::GetFTestValue() << endl;

	ftOrg = (nType > 0) ? CImage::ftText : CImage::ftBinary;
	CImage	imgOrg(szOrgImg, ulWidth, ulHeight, ftOrg);
	CImage	imgOut(szOrgImg, ulWidth, ulHeight, ftOrg);
	CParams	prmOut((int)ulWidth, (int)ulHeight);

	CBeta::AttachImage(&imgOrg);
	CBeta::AttachOutImage(&imgOut);
	CBeta::AttachParams(&prmOut);
	CBeta::FirstStage();
	//printf("\ndone1");
#define	SHOW_PARAMS
#ifdef SHOW_PARAMS
	fp = fopen("temp.asc", "w");
	if(fp)
	{
		for(int i = 1; i < prmOut.GetHeight() - 1; i++)
		{
			if(i > 20)
				break;
			for(int j = 1; j < prmOut.GetWidth() - 1; j++)
			{
				if(j > 20)
					break;
				fprintf(fp, "Parms[%.2d][%.2d] (%.2d) = %+3.4f %+3.4f %+3.4f %+3.4f\n",
					i, j, imgOut.GetPixel(j, i), prmOut[i][j][0], prmOut[i][j][1],
					prmOut[i][j][2], prmOut[i][j][3]);
			}
		}
		fclose(fp);
	}
#endif	//of SHOW_PARAMS
	CBeta::SecondStage();
	end=clock();
	double time = (double)((end-start)/CLOCKS_PER_SEC);
	//printf("\n time = %lf",time);
	return 0;
}
