// Image.h: interface for the CImage class.
//
//////////////////////////////////////////////////////////////////////

#ifndef	IMAGE_H
#define IMAGE_H

#include <stdio.h>
#include <stdlib.h>

#ifndef	_MAX_PATH
#define	_MAX_PATH	256
#endif	//of _MAX_PATH

typedef	unsigned long	ulong;

class CImage 
{
public:
	enum	FileType{ftBinary = 0, ftText = 1};
	CImage &Copy(const CImage &src);
	CImage(const char *pszFileName, int nWidth, int nHeight, 
				FileType fileType = ftBinary);
	CImage(int nWidth, int nHeight, int nPixel = 0);
    CImage(const CImage &src);
	virtual ~CImage();
public:
	CImage & operator = (const CImage &src) { return Copy(src); };
	virtual bool AdjustBoundingPixels(int nPixel = 0);
	virtual	void Display();
	int * operator[](int nIndex);
	void	SetPixel(int x, int y, int nVal);
	int		GetPixel(int x, int y)const;
	int	    SubstitutePixel(int nOldVal, int nNewVal);
	int	    GetImageHeight()const;
	int	    GetImageWidth()const;
	bool	IsValidPixel(int x, int y)const;
	bool	FreeImageSpace();
	bool	SaveImageAs(const char *pszFileName = NULL, FileType enType = ftBinary );
protected:
	bool	SaveAsText(FILE *fp);
	bool	SaveAsBinary(FILE *fp);
	bool	ReadTextFile(FILE *fp);
	bool	ReadBinaryFile(FILE *fp);
	bool	AllocateImageSpace(int nWidth, int nHeight, int nInitVal = 0);
	bool	ReadImageFromFile();
protected:
	FileType m_enFileType;
	int		**m_ppnPixels;
	char	m_szFileName[_MAX_PATH];
	int		m_nWidth;
	int		m_nHeight;
public:
	int	GetNextRegionColor(int x, int y, int nColor = 0, bool bWrap = true)const;
};

#endif //of IMAGE_H
