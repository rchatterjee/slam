// ScanFill.h: interface for the CScanFill class.
//
//////////////////////////////////////////////////////////////////////
#if !defined(AFX_SCANFILL_H__E31567C1_82D9_11D1_90B4_444553540000__INCLUDED_)
#define AFX_SCANFILL_H__E31567C1_82D9_11D1_90B4_444553540000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <memory>
#include <stack>

class CImage;
class CScanFill  
{
private:
	std::stack<std::pair<int, int> >	m_stack;

protected:
	CImage *m_pImg;
	int	X, Y;

	CImage * GetImage()const { return m_pImg;};
	virtual int  GetPixel(int x, int y)const;
	bool SetNextSeed(int xl, int xr, int y, int nFillColor, int nOldColor);

public:
	CScanFill();
	virtual ~CScanFill();

protected:
	// This function returns true if (x, y) should be filled. nOldColor is
	// what was passed to ScanFill(). This function must return false if
	// (x, y) has already been filled, otherwise the algorithm will never 
	// terminate
	virtual bool TestPixel(int x, int y, int nFillColor, int nOldColor);
	// Scan fill calls SetPixel to fill (x, y) with nFillColor
	virtual void SetPixel(int x, int y, int nPixel);

public:
	void AttachImage(CImage *pImg);
	int  ScanFill(int x, int y, int nFillColor, int nOldColor);
};
#endif // !defined(AFX_SCANFILL_H__E31567C1_82D9_11D1_90B4_444553540000__INCLUDED_)
