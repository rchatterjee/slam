// Beta.h: interface for the CBeta class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BETA_H__8BD64A23_772F_11D1_90B4_444553540000__INCLUDED_)
#define AFX_BETA_H__8BD64A23_772F_11D1_90B4_444553540000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Matrix.h"

extern	const double	B[3][3][3][3];

typedef double Perms[4];

typedef	CMatrix<Perms> CParams;

class	CImage;
class CBeta  
{
protected:
	static	CImage	*m_pImage;
	static	CImage	*m_pOut;
	static	CParams *m_pParams;
	static	int	m_nLevel;
	static char *m_outImageName;
	static	double	m_fFcr;
	static	double	m_aB[3][3];
	static	double	m_aS[3][3];
public:
	CBeta();
	virtual ~CBeta();
public:
	static	void	SetFTestLevel(int nLevel);
	static	void	SetFTestValue(double fVal) { m_fFcr = fVal;};
	static	double	GetFTestValue() { return m_fFcr;};
	static  void    SetOutImageName(char *name); 
	static	bool	SecondStage();
	static	const	char * GetClassString(int nClassID);
	static	int		Classify(int x, int y);
	static	int		GetNairLevel(){ return m_nLevel;};
	static	void	SetNairLevel(int nLevel);
	static	bool	IsInClass(int nClassID);
	static	CImage	*AttachImage(CImage *pImage);
	static	CImage	*AttachOutImage(CImage *pImage);
	static	CParams *AttachParams(CParams *pImage);
	static	void	GetAllBeta(int x, int y);
	static	CImage	*GetImage() { return m_pImage; };
	static	CImage	*GetOutImage() { return m_pOut; };
	static	double	GetBeta(int x, int y, int i, int j, double *pdError = NULL);
	static	void	FirstStage();
	static	bool	TestPixel(int x, int y, int clr);
	static	bool	IsRegionPixel(int x, int y);
	static	bool	IsInSameRegion(int x1, int y1, int x2, int y2);
private:
	static	bool SaveImageAfterFirstStage(char *szFileName);
	static	bool F_Test(int x1, int y1, int x2, int y2);
	static	bool IsPixelUnclassified(int x, int y);
	static	double BestNeighbor(int x, int y, int &nX, int &nY);
};

#include "ScanFill.h"

class CSecondStage : public CScanFill
{
private:
	CImage *m_pOut;
protected:
	virtual void SetPixel(int x, int y, int clr);
	virtual bool TestPixel(int x, int y, int clr, int nOldColor);
	friend class CBeta;
};

#endif // !defined(AFX_BETA_H__8BD64A23_772F_11D1_90B4_444553540000__INCLUDED_)
