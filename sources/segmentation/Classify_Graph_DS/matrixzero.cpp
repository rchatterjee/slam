//	matrixzero.cpp Version fusion3.6 July 2003.
//	general matrix zeroing methods.

#ifndef matrixzeroCpp

#	define matrixzeroCpp

int zerorow(double **a,double **b,int p, int k, int NumBands){
  int i;
  if(a[p][p] == 0.0)
  {
//	  printf("Divide by zero \n");
	  return (0);
  }
  double t=a[k][p]/a[p][p];
  for(i=0;i<NumBands;i++)
   {
    a[k][i]-=t*a[p][i];
    b[k][i]-=t*b[p][i];
   }
  return(1);
}

int zerocol(double **a,double **b,int p, int NumBands){
  int i;
  for(i=0;i<NumBands;i++)
   {
    if(i!=p)
	{
       if(!zerorow(a,b,p,i,NumBands))
	   return (0);
	}
   }
  return (1);
 }

#endif
