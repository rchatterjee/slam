//	DSNode.cpp Version fusion3.8
//
//	DSNode inherits from CNode defined in CNode.cpp adding the facility
//	 of performing the Dempster Shafer check before merging nodes.
//
//	(version3.8) now adapted for dynamic updation of region DS labels count
//	as nodes get merged.
//
#define DSNodeCpp

#include <stdio.h>

#ifndef CNodeCpp
#	include "CNode.cpp"
#endif

class DSNode: public virtual CNode
{
        typedef int INT;
        typedef int LABEL;
        typedef int STATUS;

        public: INT _nClasses;		// KEEPING PROTECTED GENERATES ERRORS!!
	public: INT* _dsClassScores;	// KEEPING PROTECTED GENERATES ERRORS!!
        public: LABEL _dsLabel;		// KEEPING PROTECTED GENERATES ERRORS!!

	protected: virtual BOOL F_Test(CNode * pTwo, int accr, double *Bij, double *Wij, int NumBands)
        {
        	return /*_dsLabel == ((DSNode*)pTwo->_actualThis)->_dsLabel
                 && */CNode::F_Test(pTwo, accr, Bij, Wij, NumBands);
        }
        public: DSNode(int nClasses):
         CNode(this), _dsClassScores(NULL), _nClasses(0), _dsLabel(-1)
        {
		initScores(nClasses);
        }
	public: virtual ~DSNode()
	{
		if(_dsClassScores!=NULL)
			delete[] _dsClassScores;
	}
        public: STATUS initScores(INT nClasses)
        {
        	if (_dsClassScores!=NULL || nClasses<0)
        	{
                	perror("DSNode::initScores() exception!");
                        return -1;
         	}
                _nClasses = nClasses;
                _dsClassScores = new INT[_nClasses];

                for (INT i=0; i<_nClasses; i++)
                	_dsClassScores[i] = 0;

                return 0;
        }
        public: STATUS fixScores()
        {
        	if (_dsClassScores==NULL)
                {
                	perror("DSNode::finalScores() exception!");
                        return -1;
                }
                _dsLabel = 0;

                for (INT i=0; i<_nClasses; i++)
                	if (_dsClassScores[i] > _dsClassScores[_dsLabel])
                        	_dsLabel = i;

                //delete[] _dsClassScores;
                return 0;
        }
	public: STATUS addClassLabel(LABEL label)
        {
        	if (label<0 || label>_nClasses-1 || _dsClassScores==NULL)
                {
                	perror("DSNode::addClassLabel() exception!");
                        return -1;
                }
                _dsClassScores[label]++;
        }
	public: void MergeNode(CNode *Node_j,CNode **GraphArr, int NumBands)
	{
		for (int i=0; i<_nClasses; i++)
			_dsClassScores[i] += ((DSNode*)Node_j->_actualThis)->_dsClassScores[i];

		fixScores();

		CNode::MergeNode(Node_j, GraphArr, NumBands);
	}
};
