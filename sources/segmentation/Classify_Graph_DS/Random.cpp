# include <stdio.h>
# include <string.h>
# include <math.h>
# include <time.h>
# include <iostream>
//# include "mass.cpp"
# include "Mass.cpp"
#include "Random.h"

const	long	CRandom::IM1 = 2147483563;
const	long	CRandom::IM2 = 2147483399;
const	double	CRandom::AM  = (1.0/IM1);
const	long	CRandom::IMM1= (IM1 - 1);
const	long	CRandom::IA1 = 40014;
const	long	CRandom::IA2 = 40692;
const	long	CRandom::IQ1 = 53668;
const	long	CRandom::IQ2 = 52774;
const	long	CRandom::IR1 = 12211;
const	long	CRandom::IR2 = 3791;
const	long	CRandom::NTAB= 32;
const	long	CRandom::NDIV= (1+IMM1/NTAB);
const	double	CRandom::EPS = 1.2e-7;
const	double	CRandom::RNMX= (1.0 - EPS);

long    CRandom::idum= -19961001;

const	double	CRandom::F_val[][2][44] =
{
{
	/*	For 1 band	*/
//99.99% confidence level
	{
	-1.0, 40528000.0, 9998.5, 784.01, 241.62, 124.94, 82.49, 62.16, 50.694, 43.4777,
	38.5777, 35.061, 32.427, 30.388, 28.7666, 27.4448, 26.357, 25.440, 24.658, 23.985, 
	23.399, 22.885, 22.431, 22.026, 21.663, 21.336, 21.040, 20.77, 20.525, 20.3, 20.092, 
	19.9, 19.723, 19.558, 19.405, 19.262, 19.128, 19.002, 18.884, 18.773, 18.668, 17.377,
	16.204, 15.137
	}, 
//99.95% confidence level
	{
        -1.0, 1.6211e6, 1998.5, 266.55, 106.22, 63.611, 46.082, 36.988,31.555, 27.991, 25.492, 
        23.652, 22.245, 21.137, 20.242, 19.506, 18.891, 18.368, 17.920, 17.530, 17.190,
	16.880, 16.621, 16.382, 16.166, 15.971, 15.223, 14.352, 14.012, 13.354, 12.804
	}
},
	/*	For 2 bands	*/
{
// alpha = 0.0001
	{
	-1.0, 5.0e7, 9999.0, 694.74, 198.0, 97.027, 61.633, 45.132, 36.0, 30.342, 26.548, 
	23.852, 21.85, 20.31, 19.093, 18.109, 17.298, 16.619, 16.043, 15.548, 15.119, 14.743, 
	14.411, 14.117, 13.853, 13.616, 13.402, 13.207, 13.03, 12.867, 12.718, 12.58, 
	12.45, 12.33, 12.22, 12.12, 12.026, 11.94, 11.85, 11.77, 11.70, 10.78, 9.95, 9.21
	},
// alpha = 0.0005
	{
	-1.0, 2.0e6, 1999.0, 236.61, 87.443, 49.782, 34.798, 27.206, 22.75, 19.865, 
	17.865, 16.405, 15.297, 14.43, 13.734, 13.163, 12.688, 12.286, 11.942, 11.645, 
	11.385, 11.156, 10.953, 10.771, 10.608, 10.461, 10.328, 10.206, 10.094, 9.9921, 
	9.8977, 9.81, 9.73, 9.65, 9.58, 9.52, 9.46, 9.40, 9.34, 9.29, 9.25, 8.65, 8.1, 
	7.6
	}
},	

	/*	For 3 bands 	*/
{
// alpha = 0.0001
	{
	-1.0, 5.4e7, 9999.2, 659.3, 181.02, 86.29, 53.68, 38.67, 30.46, 25.40, 
	22.04, 19.66, 17.90, 16.55, 15.49, 14.63, 13.93, 13.34, 12.85, 12.42, 
	12.05, 11.73, 11.44, 11.19, 10.96, 10.76, 10.58, 10.41, 10.26, 10.12, 
	9.99, 9.88, 9.77, 9.67, 9.57, 9.49, 9.41, 9.33, 9.26, 9.19, 9.13, 8.35, 
	7.66, 7.04
	},
// alpha = 0.0005
	{
	-1.0, 2.16e6, 1999.2, 224.7, 80.1, 44.42, 30.45, 23.45, 19.39, 16.77, 
	14.97, 13.65, 12.66, 11.89, 11.27, 10.76, 10.34, 9.98, 9.67, 9.42, 9.2,
	8.99, 8.81, 8.66, 8.51, 8.39, 8.27, 8.16, 8.06, 7.98, 7.89, 7.82, 7.75,
	7.68, 7.62, 7.56, 7.51, 7.46, 7.41, 7.37, 7.33, 6.81, 6.34, 5.91
	}
},
		
	/*	For 4 bands	*/
{
// alpha = 0.0001	
	{
	-1.0, 5.625e7, 9999.2, 640.2, 171.87, 80.527, 49.42, 35.22, 27.5, 22.77, 
	19.63, 17.42, 15.79, 14.55, 13.57, 12.78, 12.14, 11.6, 11.14, 10.75, 10.42,
	10.12, 9.86, 9.63, 9.42, 9.24, 9.07, 8.92, 8.78, 8.66, 8.54, 8.44, 8.34, 8.25,
	8.16, 8.08, 8.01, 7.94, 7.88, 7.82, 7.76, 7.06, 6.44, 5.88
	},
// alpha = 0.0005
	{
	-1.0, 2.25e6, 1999.2, 218.25, 76.124, 41.53, 28.12, 21.44, 17.58, 15.11,
	13.41, 12.18, 11.25, 10.52, 9.95, 9.48, 9.08, 8.75, 8.47, 8.23, 8.01, 7.83,
	7.66, 7.52, 7.39, 7.27, 7.16, 7.06, 6.97, 6.89, 6.82, 6.75, 6.68, 6.62, 6.56,
	6.51, 6.47, 6.42, 6.37, 6.33, 6.30, 5.82, 5.39, 5.0
	}
},
  // for 5 bands
  // alpha .0001
{
  {
   -1.0,5.764e7,9999.3,628.17,166.13,76.911,46.747,33.056,25.635,21.112,18.120,16.018,14.471,
     13.291,12.365,11.621,11.011,10.503,10.073,9.7057,9.3880,9.1107,8.8668,8.6506,8.4578,8.2847,
     8.1286,7.987,7.8581,7.7403,7.6322,7.5326,7.4406,7.3554,7.2762,7.2025,7.1336,7.0692,7.0088,
     6.9521,6.8987,6.2465,5.6661,5.1490 
     }
},
  // for 6 bands
  //alpha .0001
{
  {
    
      -1.0,5.8594e7,9999.3,619.91,162.19,74.426,44.909,31.567,24.357,19.974,17.081,15.051,
      13.560,13.425,11.534,10.819,10.234,9.7466,9.3349,8.9830,8.6789,8.4137,8.1804,7.9738,
      7.7896,7.6243,7.4752,7.3402,7.2172,7.1048,7.0017,6.9068,6.8191,6.7379,6.6625,6.5923,
      6.5267,6.4654,6.4079,6.3539,6.3031,5.6830,5.1323,4.6427
  }
}

};


static	CRandom	theRandom;

CRandom::CRandom()
{
	InitRandom();
}

CRandom::~CRandom()
{
}

void    CRandom::InitRandom()
{        
	time_t long_time;

    time( &long_time );                /* Get time as long integer. */
    idum = long_time;
	if(idum>0)idum = -idum;
	return;
}

double   CRandom::frand()
{
    int             j;
    long            k;
    static long     idum2 = 123456789;
    static long     iy = 0;
    static long     iv[NTAB];
    double           temp;
  
    if(idum <= 0)
    {
		if(-idum<1) 
			idum = 1;
		// below was only -idum but was gnrting warning stmt 
		else
			idum = -idum;
		idum2 = idum;
		for(j = NTAB + 7;j>=0;j--)
		{
			k = idum/IQ1;
			idum = IA1*(idum - k*IQ1) -k*IR1;
			if(idum <0) idum += IM1;
			if(j<NTAB)iv[j] = idum;
		}
		iy = iv[0];
    }
    k = idum/IQ1;
    idum = IA1*(idum - k*IQ1) - k*IR1;
    if(idum<0)idum += IM1;
    k = idum2/IQ2;
    idum2 = IA2*(idum2 - k*IQ2) - k*IR2;
    if(idum2 < 0)idum2 += IM2;
    j = iy/NDIV;
    iy = iv[j] - idum2;
    iv[j] = idum;
    if(iy < 1) iy += IMM1;
    if((temp = AM*iy)>RNMX)
            temp = RNMX;
    return temp;
}

int     CRandom::Random(int mx)
{
    double   x;

    x = frand();
    return((int)(x*mx));
}

double   CRandom::Gauss(double mu,double sd)
{
    static int      iset = 0;
    static double    gset;
    double           fac,rsq,v1,v2;
    
    if(iset == 0)
    {
        do{
            v1 = 2.0*frand() - 1.0;
            v2 = 2.0*frand() - 1.0;
            rsq = v1*v1 + v2*v2;
        }while(rsq >= 1.0);
        fac = sqrt( -2.0*log(rsq)/rsq);
        gset = v1*fac;
        iset = 1;
        return (mu + sd*v2*fac);
    }
    else
    {
        iset = 0;
            return (mu + sd*gset);
    }
}

// change for testing 
/*
double	CRandom::GetF_Value(int enAccr, int index)
{
	int	accr = enAccr % (sizeof(F_val) / (31 * sizeof(double)));
	double	Y_th;

	if(index <= 25)
		Y_th = 1.0/F_val[accr][index];
	 else
		Y_th = 1.0/390.7;

//	else if(index > 25 && index < 30)
//		Y_th = (1.0/F_val[accr][25]) + (double)(index - 25)*((1.0/F_val[accr][26]) - (1.0/F_val[accr][25]))/5.0;
//	else if(index >= 30 && index < 40)
//		Y_th = (1.0/F_val[accr][26]) + (double)(index - 30)*((1.0/F_val[accr][26]) - (1.0/F_val[accr][27]))/10.0;
//	else if(index >= 40 && index < 60)
//		Y_th = (1.0/F_val[accr][27]) + (double)(index - 40)*((1.0/F_val[accr][27]) - (1.0/F_val[accr][28]))/20.0;
//	else if(index >= 60 && index <= 120)
//		Y_th = (1.0/F_val[accr][28]) + (double)(index - 60)*((1.0/F_val[accr][28]) - (1.0/F_val[accr][29]))/60.0;
//	else
//		Y_th = 1.0/F_val[accr][30]; 
//	printf("Y_th=%f\n",Y_th);
	return Y_th;
}
*/
//  Original

double	CRandom::GetF_Value(int enAccr, int index, int NumBands)
{
//	int	accr = enAccr % (sizeof(F_val) / (44 * sizeof(double)));

	int 	accr = 0;//enAccr;
	double	Y_th;
	//printf("\n*********%dnilanjan\n",NumBands);
	if(index <= 40)
		Y_th = F_val[NumBands-1][accr][index];
	else if(index > 40 && index < 60)
		Y_th = F_val[NumBands-1][accr][41];
	else if(index >= 60 && index < 120)
		Y_th = F_val[NumBands-1][accr][42];
	else 
		Y_th = F_val[NumBands-1][accr][43];
	return Y_th;
}


/*double	CRandom::GetF_Value(int enAccr, int index)
{
	int	accr = enAccr % (sizeof(F_val) / (31 * sizeof(double)));
	double	Y_th;
	
	if(index <= 25)
		Y_th = F_val[accr][index];
	else if(index > 25 && index < 30)
		Y_th = F_val[accr][25] + (double)(index - 25)*(F_val[accr][26] - F_val[accr][25])/5.0;
	else if(index >= 30 && index < 40)
		Y_th = F_val[accr][26] + (double)(index - 30)*(F_val[accr][26] - F_val[accr][27])/10.0;
	else if(index >= 40 && index < 60)
		Y_th = F_val[accr][27] + (double)(index - 40)*(F_val[accr][27] - F_val[accr][28])/20.0;
	else if(index >= 60 && index <= 120)
		Y_th = F_val[accr][28] + (double)(index - 60)*(F_val[accr][28] - F_val[accr][29])/60.0;
	else
		Y_th = F_val[accr][30];
	return Y_th;
}
*/
ulong CombinedMeanVar(double m1, double ssq1, ulong n1, 
									double m2, double ssq2, ulong n2,
									double * mu, double * ssq)
{
	double	dMu;
	double	dSq;
	double	dF;
	double	dEta = 0.0;

	if( (n1 + n2) < 1)
		return 0;
	if( (n1 < 1) || ( n2 < 1))
	{
		if(mu)
			*mu = (m1 + m2)/(n1 + n2);
		if(ssq)
			*ssq = (ssq1 + ssq2)/(n1 + n2);
		return (n1+n2);
	}
	if( (n1 + n2) < 32 )
		dEta = 1.0;
	dF = 1.0/((double)n1 + (double)n2 - dEta);
	dMu = ((double)n1*dF)*m1 + ((double)n2*dF)*m2;
	dEta = 0.0;
	if(n1 < 32)
		dEta = 1.0;
	dSq = (((double)n1-dEta)*dF)*ssq1;
	dEta = 0.0;
	if(n2 < 32)
		dEta = 1.0;
	dSq+= (((double)n2-dEta)*dF)*ssq2;
	dF *= ( 1.0 / ( (double)n1 + (double)n2 ) );
	dF *= (double)((double)n1*(double)n2);
	dSq += ((m1-m2)*(m1-m2)*dF);
	if(mu)
		*mu = dMu;
	if(ssq)
		*ssq = dSq;
	return (n1+n2);
}

ulong AddOneMoreElement(double *mu_x, double *ssq_x, ulong num, double x)
{
	if(num < 1)
	{
		*mu_x = x;
		*ssq_x = 0;
		return num+1;
	}
	double	dF, dIta;
	double	dX, dMuX, dSqX;
	
	dMuX = *mu_x;
	dSqX = *ssq_x;

	dX = x;
	dF = (double)num/((double)num + 1.0);
	dIta = (num < 32)? 1.0 : 0.0;

	dSqX = (((double)num - dIta)*dSqX + dF*(dX - dMuX)*(dX - dMuX)) / ((double)num + 1 - dIta);
	dMuX = dF*(dMuX - dX) + dX;

	*mu_x = dMuX;
	*ssq_x = dSqX;
	return (num + 1);
	//return CombinedMeanVar(*mu_x, *ssq_x, num, x, 0.0, 1,mu_x, ssq_x);
}

void	RandomTest()
{
	int	n = 10,i;
	double	mu = 0, sd = 1.0;

	//cout << "Random Test ..." << endl;
	//cout << "Generating " << n << " random number ... " << endl;
	//for(i =0; i < n ; i ++)
		//cout << CRandom::frand() << " " ;

	//cout << endl;
	//cout << "Generating from Gaussian Distribution ..." << endl;
	//cout << "Mean = " << mu << " and s.d. = " << sd << endl;
	for( i = 0 ; i < n ; i ++)
		//cout << CRandom::Gauss(mu, sd) << " ";
	//cout << endl;
	//cout << "Testing AddOneMoreElement() .." << endl;
	int	nn = 0;
	mu = 0, sd = 0;
	double	x;
	//cout << "Mu = " << mu << " ssq = " << sd << endl;
	for(i =0; i < n ; i ++)
	{
		x = CRandom::Random(100);
		//cout << "Add : " << x << " then : ";
		AddOneMoreElement(&mu, &sd, i, x);
		//cout << "Mu = " << mu << " ssq = " << sd << endl;
	}
	//cout << "End of test..." << endl;
}


