#include <iostream>
#include <cstring>
#include<cstdio>
#include <cstdlib>
#include <malloc.h>

//#include<cv.h>
//#include<highgui.h>
#include "FTest_classify.cpp"

using namespace std;

#define NUM_CL 40		 // maximum number of classes
#define  NUM_BANDS 5
#define FALSE 0
#define TRUE 1
#ifndef DBL_MAX
#define DBL_MAX 1.7e+308
#endif

int nrows,ncols;

int zero_row(double **a,double **b,int p,int k,int num_bands)
{
	int i;
	if(a[p][p] == 0.0)
	{
		//	  printf("Divide by zero \n");
		return FALSE;
	}
	double t=a[k][p]/a[p][p];
	for(i=0;i<num_bands;i++)
	{
		a[k][i]-=t*a[p][i];
		b[k][i]-=t*b[p][i];
	}
	return TRUE;
}
int zero_col(double **a,double **b,int p,int num_bands)
{
	int i;
	for(i=0;i<num_bands;i++)
	{
		if(i!=p)
		{
			if(!zero_row(a,b,p,i,num_bands))
				return FALSE;
		}
	}
	return TRUE;
}


double inverse( double **a,double **b,int num_bands )
{
	int i,j;
	double det1=1.0;

	for ( i =0; i < num_bands; i++ )
		for ( j = 0; j < num_bands; j++ )
			if ( i ==j )
				b[i][i] = 1;
			else b[i][j] = 0;

	for(i=0;i<num_bands;i++)
	{
		if(!zero_col(a,b,i,num_bands))
		{
			//	 cout << " Error in inverse " << endl; 
			return(0);
		}
	}

	for(i=0;i<num_bands;i++)
		det1*=a[i][i];

	for(i=0;i<num_bands;i++)
	{
		for(j=0;j<num_bands;j++)
			b[i][j]/=a[i][i];
		a[i][i]=1;
	}
	return(det1);
}


void getNumCl(int **gtruth,const char *tr_file,int *count)
{
	int cnt;
	unsigned char *pchBuff;
	for(cnt =0; cnt <NUM_CL;cnt++)
		count[cnt]=0;
	FILE *fp;
	if( (fp=fopen(tr_file,"r")) == NULL )
		exit(1);
	pchBuff = (unsigned char *) malloc(sizeof(unsigned char)*(ncols + 5));
	for (int i= 0;i<nrows ;i++){
		fread((unsigned char *)pchBuff, sizeof(char), ncols, fp);
		for(int j= 0;j<ncols;j++){
			gtruth[i][j]=pchBuff[j];
			count[gtruth[i][j]]++;
		}
	}
	free(pchBuff);
	fclose(fp);
}

#ifdef rahul
int getNumClus(int **data,int **img,const char *seg_file)
{
	FILE *fp;
	int i,j,k, nregions=1;
	fp =fopen(seg_file,"r");if(!fp) {	
		cout<<"Input File Error\n";
		exit(0);
	}
	for(i= 0;i<nrows;i++){
		for(j=0;j<ncols;j++){
			fscanf(fp,"%d",&data[i][j]);
			//for the first row to be numbered
			if(!i){
				if(j>0 && data[i][j]==data[i][j-1]) img[i][j]=img[i][j-1];
				else img[i][j]=++nregions;
			}
			else {
				if(data[i][j]==data[i-1][j]) img[i][j]=img[i-1][j];
				else if((j>0) && (data[i][j-1]==data[i][j])) img[i][j]=img[i][j-1];
				else if((j>0) && (data[i-1][j-1]==data[i][j])) img[i][j]=img[i-1][j-1];
				else if((j<ncols-1) && (data[i-1][j+1]==data[i][j])) img[i][j]=img[i-1][j+1];
				else img[i][j]=++nregions;
			}
		}
	}
	
	fclose(fp);
	return nregions;
}
#else
int getNumClus(int **data,int **img,char *seg_file)
{	

	FILE *fp;
	int i,j,k, max=0, useless=0, **keeper, left=-1, right=0, x=0, y=0;
	keeper = new int*[nrows*ncols];
	int prod_jaw=nrows*ncols;
	for(i=0;i<prod_jaw;i++)
		keeper[i] = new int[2];

	fp =fopen(seg_file,"r");
	for(i= 0;i<nrows;i++){		
		for(j=0;j<ncols;j++){
			fscanf(fp,"%d",&data[i][j]);
			img[i][j]=-1;
		}
	}
	fclose(fp);
	
	for(i= 0;i<nrows;i++)
		for(j=0;j<ncols;j++){
          if(img[i][j]== -1){
				max++;
				
				keeper[right][0]=i;
				keeper[right][1]=j;
				do{
				
                                   x=keeper[left+1][0];
                                   y=keeper[left+1][1];
                                   left++;
                                   img[x][y]=max;
                                   if(x<(nrows-1) && (data[x+1][y] == data[x][y]) && (img[x+1][y] == -1)){
						  img[x+1][y]=-2;
                                                  right++;
                                                  keeper[right][0]=x+1;
                                                  keeper[right][1]=y;
                                   }
                                   if(x<(nrows-1)&& y<(ncols-1) && (data[x+1][y+1] == data[x][y])&&(img[x+1][y+1] == -1)){
						  //printf("\n x+1 = %d    y+1 = %d   data[x+1][y+1] = %d",x+1,y+1,data[x+1][y+1]);
                                                  img[x+1][y+1]=-2;
						  right++;
                                                  keeper[right][0]=x+1;
                                                  keeper[right][1]=y+1;
                                   }
                                   if(y<(ncols-1) && (data[x][y+1] == data[x][y]) && (img[x][y+1] == -1)){
						  //printf("\n x = %d    y+1 = %d   data[x][y+1] = %d",x,y+1,data[x][y+1]);
						  img[x][y+1]=-2;
                                                  right++;
                                                  keeper[right][0]=x;
                                                  keeper[right][1]=y+1;
                                   }
                                   if(x>0 && y<(ncols-1) && (data[x-1][y+1] == data[x][y]) && (img[x-1][y+1] == -1)){
						  //printf("\n x-1 = %d    y+1 = %d   data[x-1][y+1] = %d",x-1,y+1,data[x-1][y+1]);
						  img[x-1][y+1]=-2;
                                                  right++;
                                                  keeper[right][0]=x-1;
                                                  keeper[right][1]=y+1;
                                   }
                                   if((x>0) && (data[x-1][y] == data[x][y]) && (img[x-1][y] == -1)){
						  //printf("\n x-1 = %d    y = %d   data[x-1][y] = %d",x-1,y,data[x-1][y]);
						  img[x-1][y]=-2;
                                                  right++;
                                                  keeper[right][0]=x-1;
                                                  keeper[right][1]=y;
                                   }
                                   if(x>0 && y>0 && (data[x-1][y-1] == data[x][y]) && (img[x-1][y-1] == -1)){
						  //printf("\n x-1 = %d    y-1 = %d   data[x-1][y-1] = %d",x-1,y-1,data[x-1][y-1]);
						  img[x-1][y-1]=-2;
                                                  right++;
                                                  keeper[right][0]=x-1;
                                                  keeper[right][1]=y-1;
                                   }
                                   if(y>0 && (data[x][y-1] == data[x][y]) && (img[x][y-1] == -1)){
						  //printf("\n x = %d    y-1 = %d   data[x][y-1] = %d",x,y-1,data[x][y-1]);
						  img[x][y-1]=-2;
                                                  right++;
                                                  keeper[right][0]=x;
                                                  keeper[right][1]=y-1;
                                   }
                                   if(x<(nrows-1) && y>0 && (data[x+1][y-1] == data[x][y]) && (img[x+1][y-1] == -1)){
						  //printf("\n x+1 = %d    y-1 = %d   data[x+1][y-1] = %d",x+1,y-1,data[x+1][y-1]);
						  img[x+1][y-1]=-2;
                                                  right++;
                                                  keeper[right][0]=x+1;
                                                  keeper[right][1]=y-1;
                                   }
                                }while(left!=right);
                                left=-1;
                                right=0;
				
}

    }                               

	return max;

}
#endif

void readImg(int numbands,double ***image_data,char **image_file)
{
	FILE **fp_image_file;
	unsigned char *pchBuff;
	fp_image_file=new FILE*[numbands];
	//pchBuff = (unsigned char *) malloc(sizeof(unsigned char)*(ncols + 5));
	for(int k=0; k<numbands; k++){
		//printf("\tImage File %1d : %s\n",k+1, image_file[k]);
		fp_image_file[k] = fopen(image_file[k], "rb");
		if(fp_image_file[k] == NULL)
		{
			printf(" Error in file name %s \n", image_file[k]);
			exit(0);
		}
		for(int i=0; i<nrows; i++)
		{
			//fread((unsigned char *)pchBuff, sizeof(char), ncols, fp_image_file[k]);
			for(int j=0; j<ncols; j++)
			{
				image_data[k][i][j] = fgetc(fp_image_file[k]);//pchBuff[j];
			}
		}
		fclose(fp_image_file[k]);
	}
	//free(pchBuff);
	delete []fp_image_file;
}

int Classify(int numsensors, int numbands, int confidence_val, int nCols, int nRows, char **image_file, char *seg_file, char *cl_file, int numcl, int *band)
{
	nrows = nRows;
	ncols = nCols;

	int **data,**img,**conf_matrix,**test,count_test[NUM_CL],cnt;
    int i,j,k,l,m;
	int numclus,numcl_test=0;
	double ***image_data;
	char *test_file;
	double **all_mean, **cl_mean;
	int *all_numpixels, *cl_numpixels;
	double **one_ucl_cov, ***cl_cov, **combine_cov, **inv_combine_cov;
	double tsq, min=0.0,min2=0.0;
	double *mean_diff, *combine_cov_mul_by_mean_diff;
	int label = 0, *finallabelstatus;
	int numt=0,numt2=0,accr=0,accrcl=0;
	FILE *fp,*fp1,*fp2;
	
	if(confidence_val > 4)
		confidence_val=3;
	else if(confidence_val < 1)
		confidence_val=-1;
	else 	
		confidence_val=confidence_val-1;
	//test=new int *[nrows];
	///confidence_val=1; 
	data=new int *[nrows];
	img=new int *[nrows];
	image_data=new double**[numbands];
	//cout<<"\n Here2";

	for (j= 0;j<numbands;j++){
		image_data[j]=new double*[nrows];
		for(i= 0;i<nrows;i++)
			image_data[j][i]=new double[ncols];
	}
	for(i= 0;i<nrows;i++){
		//test[i]=new int[ncols];
		data[i]=new int[ncols];
		img[i]=new int[ncols];
	}
	
	for(cnt =1; cnt <NUM_CL;cnt++)
		if(count_test[cnt])
			numcl_test++;
	numclus = getNumClus(data, img, seg_file);
	i=0;

	readImg(numbands,image_data,image_file);


	fp1 = fopen("segment_min_relabel.asc","w");
	for(i=0;i<nrows;i++){
		for(j=0;j<ncols;j++)
			fprintf(fp1,"%d ",img[i][j]);
			fprintf(fp1,"\n");
	}
	fclose(fp1);
	
	cl_cov=new double **[numcl+1];
	all_mean = new double *[numclus+1];
	cl_mean = new double *[numcl+1];
	all_numpixels = new int[numclus+1];
	cl_numpixels = new int[numcl+1];
	
	
	for(i=0;i<numclus+1;i++){
		all_mean[i]=new double[numbands];
		for(j=0;j<numbands;j++)
				all_mean[i][j]=0.0;
		all_numpixels[i]=0;
	}
	for(i=0;i<numcl+1;i++){
		cl_mean[i]=new double[numbands];
		cl_cov[i]=new double*[numbands];
		for(j=0;j<numbands;j++){
			cl_mean[i][j]=0.0;
			cl_cov[i][j]=new double[numbands];
			for(k=0;k<numbands;k++)
				cl_cov[i][j][k]=0.0;
		}
		cl_numpixels[i]=0;
	}
	finallabelstatus = new int[numclus+1];
	for(i=0;i<numclus+1;i++)
		finallabelstatus[i]=0;

	

	//cout<<"numbands is "<<numbands<<"   "<<"numclus is "<<numclus<<"   "<<"rows is "<<nrows<<"   "<<"cols is  "<<ncols<<endl;

		for(i=0;i<nrows;i++){
			for(j=0;j<ncols;j++){	  
				++all_numpixels[img[i][j]];
				for(k=0;k<numbands;k++){
					if(all_numpixels[img[i][j]]==1)
						all_mean[img[i][j]][k] += image_data[k][i][j];
					else
						all_mean[img[i][j]][k] = ((all_mean[img[i][j]][k] * (double)(all_numpixels[img[i][j]]-1)) + image_data[k][i][j])/(double)(all_numpixels[img[i][j]]);
				}
			}
		}
	fflush(stdout);


//***************Reading the file***********************************************

	int n_bands=5;
	int n_cl=14;
	double **mean,***cov;
	mean = new double*[n_cl+1];
	cov  = new double**[n_cl+1];
	for(i=1;i<n_cl+1;i++){
		mean[i] = new double[n_bands];
		cov[i]  = new double*[n_bands];
		for(j=0;j<n_bands;j++)
			cov[i][j]=new double[n_bands];
	}

	fflush(stdout);
	FILE *lookup=fopen("lookup","r");
	for(i=1;i<n_cl+1;i++){
		fscanf(lookup,"%d",&cl_numpixels[i]);
		cl_numpixels[i]=1;
		for(j=0;j<n_bands;j++) fscanf(lookup,"%lf",&mean[i][j]);//fgetc(lookup);
		for(j=0;j<n_bands;j++) for(k=0;k<n_bands;k++) fscanf(lookup,"%lf",&cov[i][j][k]);
	}
	fclose(lookup);


	for(i=1;i<numcl+1;i++){
		l=0;
		for(j=0;j< n_bands;j++) 
		 if(band[j]){ 
		              cl_mean[i][l]=mean[i][j];//fgetc(lookup);
		              l++;
		}
		
		
		for(j=0,l=0;j<n_bands;j++) 
		if(band[j])
		{
			
			for(k=0,m=0;k<n_bands;k++) 
			if(band[k]){
				cl_cov[i][l][m]=cov[i][j][k];
		   		m++;
		 	}
		 	//else cout<<band[k];
		 	l++;
		 }
	}
	lookup=fopen("lookupnew.asc","w");


	for(i=1;i<numcl+1;i++){
		fprintf(lookup,"%d ",cl_numpixels[i]);
		for(j=0;j<numbands;j++) fprintf(lookup,"%lf ",cl_mean[i][j]);//fgetc(lookup);
		fprintf(lookup,"\n");
		for(j=0;j<numbands;j++) for(k=0;k<numbands;k++) fprintf(lookup,"%lf ",cl_cov[i][j][k]);
		fprintf(lookup,"\n");
	}
//*******************************COMPLETE**************************************
	for(i=1;i<numcl+1;i++)
		for(l=0;l<numbands;l++)
			for(m=0;m<numbands;m++)	
				cl_cov[i][l][m]*=(double)cl_numpixels[i];

	double *tmp,***all_cov=new double**[numclus+1];
	tmp=new double[numbands];
	int t;
	for(i=1;i<numclus+1;i++){
		all_cov[i]=new double*[numbands];
		for(j=0;j<numbands;j++){
			all_cov[i][j]=new double[numbands];
			for(k=0;k<numbands;k++)
					all_cov[i][j][k]=0.0;
			}
	}
	for(i=0;i<nrows;i++)
	  for(j=0;j<ncols;j++){
	  	t=img[i][j];
	  	for(k=0;k<numbands;k++)
	  		tmp[k]=image_data[k][i][j]-all_mean[t][k];
	  	for(k=0;k<numbands;k++)
	  		for(l=0;l<numbands;l++)
	  			all_cov[t][k][l]+=(tmp[k]*tmp[l]);	
	  }
//*************************************Complete*********************************
	mean_diff = new double[numbands];
	combine_cov_mul_by_mean_diff = new double[numbands];
	one_ucl_cov = new double*[numbands];
	combine_cov = new double*[numbands];
	inv_combine_cov = new double*[numbands];

	for(j=0;j<numbands;j++){
		mean_diff[j]=0;
		combine_cov_mul_by_mean_diff[j]=0;
		one_ucl_cov[j] = new double[numbands];
		combine_cov[j] = new double[numbands];
		inv_combine_cov[j] = new double[numbands];

		for(k=0;k<numbands;k++){
			one_ucl_cov[j][k]=0.0;
			combine_cov[j][k]=0.0;
			inv_combine_cov[j][k]=0.0;
		}
	}
	double F;
	//cout<<"numcl: "<<numcl<<endl;
	for(i=1;i<numclus+1;i++){
		//if(!(i%1000))cout<<"*";
		fflush(stdout);
		for(l=0;l<numbands;l++)
			for(m=0;m<numbands;m++)
				one_ucl_cov[l][m]=all_cov[i][l][m];
	
			min=DBL_MAX;//new
			
		for(j=1;j<numcl+1;j++){
			tsq=0;
			for(k=0;k<numbands;k++){
				combine_cov_mul_by_mean_diff[k]=0.0;
				mean_diff[k]=0.0;
				for(l=0;l<numbands;l++)
				{
					combine_cov[k][l]=0.0;
					inv_combine_cov[k][l]=0.0;
					combine_cov[k][l]= ((one_ucl_cov[k][l])+(cl_cov[j][k][l]))/((double)all_numpixels[i] + (double)cl_numpixels[j] - 2)* ((1/(double)all_numpixels[i])+(1/(double)cl_numpixels[j]));
				}
				mean_diff[k]=all_mean[i][k]-cl_mean[j][k];
			}
			inverse(combine_cov, inv_combine_cov, numbands);
			for(k=0;k<numbands;k++){
				for(l=0;l<numbands;l++)
					combine_cov_mul_by_mean_diff[k]+=inv_combine_cov[k][l] * mean_diff[l];
					tsq+=combine_cov_mul_by_mean_diff[k] * mean_diff[k];
				}

			if(confidence_val == -1)  min2=DBL_MAX;
		  else{
		  	F=GetF_Value_classify(confidence_val,all_numpixels[i]+cl_numpixels[j]-numbands-1,numbands);
				min2=(all_numpixels[i]+cl_numpixels[j]-2)*numbands*F/(all_numpixels[i]+cl_numpixels[j]-numbands-1);
			}
			
			if(tsq<min2)
			{
				if(tsq<min){
					min = tsq;
					//label=j;
				finallabelstatus[i]=j;//label;
				}
			}
			
			label = 0;
		}
	}

	fp2 = fopen(cl_file, "w");
	int temp;
	for(i=0;i<nrows;i++){
		for(j=0;j<ncols;j++){
			fprintf(fp2,"%d ",finallabelstatus[img[i][j]]);
		}
		fprintf(fp2,"\n");
	}
	fclose(fp2);
	
/************************************************************************/
/*For Showing the Image

	FILE *colorFile=fopen("colormap-final-14","rb");
	IplImage *image=0;
	if(numbands>3) t=3;
 	image=cvCreateImage(cvSize(ncols,nrows),IPL_DEPTH_32F,3);
 	CvScalar *colors = new CvScalar[14];
 	int colormap[][3]={//green blue red
		 	    {204, 204, 204},
                      {0,102,0},
                      {0, 0, 255},
                      {153, 153, 255},
                      {255, 255, 0},
                      {153, 153, 0},
                      {0, 153, 153},
                      {255, 255, 204},
                      {153, 102, 0},
                      {51, 0, 51},
                      {255, 153, 153},
                      {153, 0, 0},
                      {255, 153, 51},
                      {51, 255, 51},
			};
 	if(colorFile){
 		cout<<"Reading the color map file.....\n";
 		for(i=0;i<14;i++){
 		for(j=0;j<3;j++)
 		{
 			colors[i].val[2-j]=fgetc(colorFile);
 			//cout<<colors[i].val[2-j]<<" ";
 		}
 		//cout<<"\n";
        }
 	}
 	else {
         cout<<"Without color map file.....\n";
 		for(i=0;i<14;i++){
 		for(j=0;j<3;j++)
 		{
 			colors[i].val[2-j]=colormap[i][j];
 			//cout<<colors[i].val[2-j]<<" ";;
 		}
 	}
 		
   	}
 	cout<<"\nSegmentation completed!\n";
 	
 	for(i=0;i<nrows;i++)
 	for(j=0;j<ncols;j++){
 		for(l=0;l<t;l++)
 		//((uchar *)(image->imageData + i*image->widthStep))[j*image->nChannels + l]=(int)cl_mean[finallabelstatus[img[i][j]]][l]; // B
 		//((uchar *)(image->imageData + i*image->widthStep))[j*image->nChannels +l]=colors[finallabelstatus[img[i][j]]][l]; // B
 		cvSet2D(image,i,j,colors[finallabelstatus[img[i][j]]-1]);
	}
 
 	char outFileName[]="ClassifiedImage.jpg";
 	if(!cvSaveImage(outFileName,image)) printf("Could not save: %s\n",outFileName);
	cvReleaseImage(&image);
/**********************COMPLETE************************************************/	

	return 0;
}
