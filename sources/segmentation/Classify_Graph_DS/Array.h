#ifndef	ARRAY_H
#define	ARRAY_H	1000

typedef	unsigned long	ulong;
typedef	int	BOOL;

#ifndef	TRUE
#define	TRUE	1
#endif	//of TRUE

#ifndef	FALSE
#define	FALSE	0
#endif	//of FALSE

#define NON_INDEX  ((ulong)~1)
#define DEF_ALLOC_UNIT (10)

template <class T>
class CArray
{
public:
	CArray(ulong ulUnit = DEF_ALLOC_UNIT)
	{
		ALLOC_UNIT = ulUnit;
		m_pArray = NULL;
		m_ulNumElements = 0;
		m_ulArraySize = 0;
	};
	
	virtual	~CArray()
	{
		for(ulong u = 0; u < m_ulNumElements ; u ++)
			DestructElement(u);
	};
	
protected:
	ulong	ALLOC_UNIT;
	T		*m_pArray;
	ulong	m_ulNumElements;
	ulong	m_ulArraySize;

public:
	inline	virtual	BOOL	AddElement(const T &elm, BOOL Force = FALSE);
	inline	virtual	ulong	FindElementIndex(const T &elm);
	inline	virtual	T		&operator [](ulong ulIndex) const;
	inline	virtual	ulong	GetNumElements()const;
	inline	virtual	T		RemoveAt(ulong ulIndex);
	inline	virtual	BOOL	RemoveElement(T &elm);
	inline	virtual	BOOL	EliminateCommon();
protected:
	inline	virtual	BOOL	AllocateSpace(BOOL bForce = FALSE);
	inline	virtual	void	ConstructElement(const T &t, ulong ulIndex);
	inline	virtual	void	DestructElement (ulong ulIndex);
};

template <class T>
class CPtrArray
{
public:
	CPtrArray(ulong ulUnit = DEF_ALLOC_UNIT)
	{
		ALLOC_UNIT = ulUnit;
		m_pArray = NULL;
		m_ulNumElements = 0;
		m_ulArraySize = 0;
	};
	
	virtual	~CPtrArray()
	{
		for(ulong u = 0; u < m_ulNumElements ; u ++)
			delete m_pArray[u];
	};
	
protected:
	ulong	ALLOC_UNIT;
	T		**m_pArray;
	ulong	m_ulNumElements;
	ulong	m_ulArraySize;

public:
	inline	virtual	BOOL	AddElement( T *elm, BOOL bForce = FALSE);
	inline	virtual	ulong	FindElementIndex(const T *elm);
	inline	virtual	T		*operator [](ulong ulIndex) const;
	inline	virtual	ulong	GetNumElements()const;
	inline	virtual	T		*RemoveAt(ulong ulIndex);
	inline	virtual	T		*RemoveElement(T *elm);
	inline	virtual	BOOL	EliminateCommon();
protected:
	inline	virtual	BOOL	AllocateSpace(BOOL bForce = FALSE);
};

template <class T>
class CStack
{
private:
	CArray<T> m_aS;
public:
	CStack(){};
	~CStack(){};

	inline int Pop(T &tT);
	inline void Push(T tT);
	inline int IsEmpty()const;
};

template <class T>
class CMatrix
{
public:
	CMatrix();
	CMatrix(int nWidth, int nHeight, BOOL bCallCstr = FALSE);
	virtual ~CMatrix();
private:
	int	m_nWidth;
	int	m_nHeight;
	T	**m_ppT;
protected:
	void	Empty();
public:
	int	GetWidth()const { return m_nWidth; };
	int GetHeight()const { return m_nHeight; };
	inline int	Construct(int nWidth, int nHeight, BOOL bCallCstr = FALSE);
	inline T	*operator [](int nIndex)const;
	//void SetValue(int x, int y, T tT) { m_ppT[y][x] = tT;};
	T &GetValue(int x, int y)const { return ((*this)[y])[x];};
};

#include "Array.inl"
#endif	//of ARRAY_H;
