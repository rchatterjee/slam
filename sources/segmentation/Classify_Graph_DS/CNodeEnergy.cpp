
#ifndef CNodeCpp
#	include "CNode.cpp"
#endif

#include "RBTree.cpp"
#include <map>
using namespace std;
#define NEG -100000000
#define SAFENEG -90000000
#define PSZERO -70000000
#define SAFEZERO -60000000
typedef int itype;
typedef double vtype;

class CNodeEnergy{
	RBTree VMap;
public:
	void DeleteFromVMap(CNode *Node);
	void InsertInVMap(CNode *Node);
	itype GetMaxVIndex();
	void InitiallizeRBTree(int Size);
	//CNodeEnergy(int Size){VMap(Size);};
};

void CNodeEnergy::InitiallizeRBTree(int Size){
	VMap.AllocRBTree(Size);
}

void CNodeEnergy::DeleteFromVMap(CNode *Node){
	if(!Node)return;
	int F;
	vtype V = Node->V;
	itype index = Node->Index;
	if(V==0)V = PSZERO + index;
	if(V==NEG)V = NEG + index;
	VMap.Delete(index);
}

void CNodeEnergy::InsertInVMap(CNode *Node){
	vtype V = Node->V;
	if(V==0)V = PSZERO + Node->Index;
	if(V==NEG)V = NEG + Node->Index;
	itype index = Node->Index;
	VMap.Insert(V,index);
}

itype CNodeEnergy::GetMaxVIndex(){
	int Max;
	Max = VMap.RBTreeMaximum();
	return Max; 
}

