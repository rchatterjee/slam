// Image.cpp: implementation of the CImage class.
//
//////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <malloc.h>
#include <math.h>
#include <iostream>
#include "Image.h"
using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImage::CImage(const char *pszFileName, int nWidth, int nHeight, FileType fileType)
{
	m_nHeight = nHeight;
	m_nWidth = nWidth;
	m_ppnPixels = NULL;
	m_enFileType = fileType;
	strcpy(m_szFileName, pszFileName);
	ReadImageFromFile();
}

void CImage::CImagex(const char *pszFileName, int nWidth, int nHeight, FileType fileType){
	m_nHeight = nHeight;
	m_nWidth = nWidth;
	m_ppnPixels = NULL;
	m_enFileType = fileType;
	strcpy(m_szFileName, pszFileName);
	ReadImageFromFile();
}


CImage::CImage(int nWidth, int nHeight, int nPixel)
{
	m_ppnPixels = NULL;
	m_szFileName[0] = '\0';
	m_nWidth = nWidth;
	m_nHeight = nHeight;
	m_enFileType = ftBinary;
	AllocateImageSpace(m_nWidth, m_nHeight, nPixel);
}

CImage &CImage::Copy(const CImage &src)
{
	m_nHeight = src.m_nHeight;
	m_nWidth = src.m_nWidth;
	m_ppnPixels = NULL;
	m_enFileType = src.m_enFileType;
	strcpy(m_szFileName, src.m_szFileName);
	AllocateImageSpace(m_nWidth, m_nHeight);
	for(int u = 0; u < m_nHeight; u++)
		for(int v = 0; v < m_nWidth; v++)
			SetPixel(v, u, src.GetPixel(v, u));

	return (*this);
}

CImage::~CImage()
{
	FreeImageSpace();
}

BOOL CImage::ReadImageFromFile()
{
	BOOL	bRet = TRUE;
	FILE	*fp;
	if((fp = fopen(m_szFileName, "rb")) == NULL)
	{
		perror(m_szFileName);
		return FALSE;
	}
	AllocateImageSpace(m_nWidth, m_nHeight);
	switch(m_enFileType)
	{
	case ftBinary:
		bRet = ReadBinaryFile(fp);
		break;
	case ftText:
		bRet = ReadTextFile(fp);
		break;
	}
	fclose(fp);
	return bRet;
}


BOOL CImage::FreeImageSpace()
{
	if(!m_ppnPixels)
		return FALSE;
	for(int u = 0; u < m_nHeight ; u ++)
		delete m_ppnPixels[u];
	delete [] m_ppnPixels;
	return TRUE;
}

BOOL CImage::AllocateImageSpace(int nWidth, int nHeight, int nInitVal)
{
	if(m_ppnPixels)
		FreeImageSpace();
	m_ppnPixels = new int*[nHeight];
	for(int u = 0 ; u < nHeight; u ++)
	{
		m_ppnPixels[u] = new int[nWidth];
		for(int v = 0; v < nWidth; v++)
			m_ppnPixels[u][v] = nInitVal;
	}
	return TRUE;
}

BOOL CImage::ReadBinaryFile(FILE * fp)
{
	BOOL	bRet = TRUE;
	unsigned	char	*pchBuff;
	int	nX, nY;

	pchBuff = new unsigned char[m_nWidth + 5];

	for(nY	= 0 ; nY < m_nHeight ; nY ++)
	{
		if((int)(fread((unsigned char *)pchBuff, sizeof(char), m_nWidth, fp)) < m_nWidth)
			bRet = FALSE;
		for(nX = 0; nX < m_nWidth; nX ++)
			m_ppnPixels[nY][nX] = pchBuff[nX];
	}
	delete pchBuff;
	return bRet;
}

BOOL CImage::ReadTextFile(FILE * fp)
{
	BOOL	bRet = TRUE;
	int		nPel;
	int	nX, nY;

	for(nY	= 0 ; nY < m_nHeight ; nY ++)
	{
		for(nX = 0; nX < m_nWidth; nX ++)
		{
			fscanf(fp, "%d", &nPel);
			m_ppnPixels[nY][nX] = nPel;
		}
	}
	return bRet;
}

BOOL CImage::SaveAsBinary(FILE * fp)
{
	BOOL	bRet = TRUE;
	unsigned	char	*pchBuff;
	int	nX, nY;

	pchBuff = new unsigned char[m_nWidth + 5];

	for(nY	= 0 ; nY < m_nHeight ; nY ++)
	{
		for(nX = 0; nX < m_nWidth; nX ++)
			pchBuff[nX] = (unsigned char)(m_ppnPixels[nY][nX]);
		if((int)(fwrite((unsigned char *)pchBuff, sizeof(char), m_nWidth, fp)) < m_nWidth)
			bRet = FALSE;
	}
	delete pchBuff;
	return bRet;
}

BOOL CImage::SaveAsText(FILE * fp)
{
	BOOL	bRet = TRUE;
	int		nPel;
	int	nX, nY;

	for(nY	= 0 ; nY < m_nHeight ; nY ++)
	{
		for(nX = 0; nX < m_nWidth; nX ++)
		{
			fprintf(fp, "%d ", m_ppnPixels[nY][nX]);
		}
		fprintf(fp,"\n");
	}
	return bRet;
}

BOOL CImage::SaveImageAs(const char * pszFileName, FileType enType)
{
	BOOL	bRet = TRUE;
	char	szFileName[_MAX_PATH];
	FILE	*fp;

	if(pszFileName == NULL)
		strcpy(szFileName, m_szFileName);
	else
		strcpy(szFileName, pszFileName);

	if((fp = fopen(szFileName, "wb")) == NULL)
	{
		perror(szFileName);
		return FALSE;
	}
	switch(enType)
	{
	case ftBinary:
		bRet = SaveAsBinary(fp);
		break;
	case ftText:
		bRet = SaveAsText(fp);
		break;
	}
	fclose(fp);
	return bRet;
}

int * CImage::operator [ ](int nIndex)
{
	if(nIndex < m_nHeight)
		return m_ppnPixels[nIndex];
	return NULL;
}

int CImage::SubstitutePixel(int nOldVal, int nNewVal)
{
	int	nCount = 0;
	for(int u = 0 ; u < m_nHeight; u ++)
	{
		for(int v = 0; v < m_nWidth; v++)
		{
			if(m_ppnPixels[u][v] == nOldVal)
			{
				m_ppnPixels[u][v] = nNewVal;
				nCount++;
			}
		}
	}
	return nCount;
}

void CImage::Display()
{
	//for(int nY = 0; nY < GetImageHeight() ; nY ++)
	{
		//for(int nX = 0; nX < GetImageWidth(); nX ++)
		//	cout << "\t" << m_ppnPixels[nY][nX];
		//cout << endl;
	}
}


BOOL CImage::AdjustBoundingPixels(int nPixel)
{
	int	u;
	BOOL	bFlag = TRUE;
	for( u = 0; u < m_nWidth; u++)
	{
		if(m_ppnPixels[0][u] != nPixel)
		{
			bFlag = FALSE;
			break;
		}
	}
	if(bFlag)
		for(u = 0; u < m_nWidth; u++)
			m_ppnPixels[0][u] = m_ppnPixels[1][u];

	for( u = 0; u < m_nWidth; u++)
	{
		if(m_ppnPixels[m_nHeight - 1][u] != nPixel)
		{
			bFlag = FALSE;
			break;
		}
	}
	if(bFlag)
		for(u = 0; u < m_nWidth; u++)
			m_ppnPixels[m_nHeight - 1][u] = m_ppnPixels[m_nHeight - 2][u];

	for( u = 0; u < m_nHeight; u++)
	{
		if(m_ppnPixels[u][0] != nPixel)
		{
			bFlag = FALSE;
			break;
		}
	}
	if(bFlag)
		for(u = 0; u < m_nHeight; u++)
			m_ppnPixels[u][0] = m_ppnPixels[u][1];
	
	for( u = 0; u < m_nHeight; u++)
	{
		if(m_ppnPixels[u][m_nWidth - 1] != nPixel)
		{
			bFlag = FALSE;
			break;
		}
	}
	if(bFlag)
		for(u = 0; u < m_nHeight; u++)
			m_ppnPixels[u][m_nWidth - 1] = m_ppnPixels[u][m_nWidth - 2];
	return TRUE;
}

int	CImage::GetImageHeight()const
{
	return m_nHeight;
}

int	CImage::GetImageWidth()const
{
	return m_nWidth;
}

int CImage::GetPixel(int x, int y)const
{
	if(IsValidPixel(x, y))
		return m_ppnPixels[y][x];
	return -1;
}

void CImage::SetPixel(int x, int y, int nVal)
{	
	if(IsValidPixel(x, y))
		m_ppnPixels[y][x] = nVal;
}

BOOL CImage::IsValidPixel(int x, int y)const
{
	if(y < 0 || y >= m_nHeight || x < 0 || x > m_nWidth)
		return FALSE;
	return TRUE;
}

int	CImage::GetNextRegionColor(int x, int y, int nColor, BOOL bWrap)const
{
	BOOL bNeighborColor = FALSE;
	int nPixel = (nColor > 0) ? nColor : GetPixel(x, y);
	do {
		bNeighborColor = FALSE;
		nPixel += 15;
		for(int i = -1; i <= 1; i++)
		{
			for(int j = -1; j <= 1; j++)
			{
				if(GetPixel(x - i, y - j) == (bWrap ? (nPixel % 256) : nPixel))
				{
					bNeighborColor = TRUE;
					break;
				}
			}
			if(bNeighborColor)
				break;
		}
		
	}while(bNeighborColor);
	return nPixel;
}
