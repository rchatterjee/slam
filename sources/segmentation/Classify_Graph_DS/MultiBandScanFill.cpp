// MultiBandScanFill.cpp: implementation of the CMultiBandScanFill class.
//	Version fusion3.6 July 2003 derived from fusion2.0
//
//	Error in isSinglePixel() pixel neighbor checking fixed.
//	At present, isSinglePixel() condition is kept disabled in TestPixel()
//	 because the consequences of obtaining a 'singlepixel' are vague.
//	Removal of 'singlepixels', if done, must be in a better way.
//
//	Now option kept for enabling/disabling suppress of testing images
//	 at single pixel images.
//
//	(later) Option might be given for keeping black (zeroed) pixels with
//	 an OR condition and an AND condition
//
//////////////////////////////////////////////////////////////////////
#define MultiBandScanFillH

//uncomment following to for 'arbitrary' merging of single pixels with neighbours
//#define MERGESUPPRESSIMAGEATSINGLES


#include "MultiBandScanFill.h"
#include "Image.h"
#include "ScanFill.h"
#include <iostream>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMultiBandScanFill::CMultiBandScanFill()
{
}

CMultiBandScanFill::~CMultiBandScanFill()
{
}

void CMultiBandScanFill::AttachOutputImage(CImage* pOut)
{
	if(NULL == pOut)
		return;
	int nWidth = pOut->GetImageWidth();

	int nHeight = pOut->GetImageHeight();

	for(int nX = 0; nX < nWidth; ++nX)
		for(int nY = 0; nY < nHeight; ++nY)
			pOut->SetPixel(nX, nY, 0);

	AttachImage(pOut);  // what is being done here is unclear
}

void CMultiBandScanFill::AddBandImage(CImage* pImage)
{
	m_veBands.push_back(pImage);
}

int CMultiBandScanFill::TestPixel(int x, int y, int nFillColor, int nOldColor)
{
	int isSinglePixel (int x, int y, CImage* img);

	if(nOldColor == GetPixel(x, y))
	{
		FeatureVector::const_iterator itFe = m_veFeatures.begin();
		ImageVector::const_iterator it = m_veBands.begin();
		ImageVector::const_iterator itEnd = m_veBands.end();
		for(; it != itEnd; ++it, ++itFe)
		{
#ifdef MERGESUPPRESSIMAGEATSINGLES
			if(isSinglePixel(x, y, *it))
				continue;
#endif
			if ((*itFe) != (*it)->GetPixel(x, y))
				return 0;
		}
		return 1;
	}
	return 0;
}

int CMultiBandScanFill::ScanFill(int x, int y, int nFillColor, int nOldColor)
{

	if(m_veBands.empty())
		return -1;

	m_veFeatures.clear();

	ImageVector::const_iterator it = m_veBands.begin();
	ImageVector::const_iterator itEnd = m_veBands.end();

	for(; it != itEnd; ++it)
		m_veFeatures.push_back( (*it)->GetPixel(x, y));


	return CScanFill::ScanFill(x, y, nFillColor, nOldColor);
}

int nPixelSurround[][2] = {{1, 1}, {0, 1}, {-1, 1}, {-1, 0}, {-1, -1}, {0, -1}, {1, -1}, {1, 0}};

int isSinglePixel (int x, int y, CImage* img)
{
	int nPixel = img->GetPixel(x, y);

	int nDisable[] = {0, 0, 0, 0, 0, 0, 0, 0}; // Anjan added a 0

	if (x==0)
		nDisable[2] = nDisable[3] = nDisable[4] = 1;

	else if (x == img->GetImageWidth()-1)
		nDisable[6] = nDisable[7] = nDisable[0] = 1; // Anjan changed 5,6,0 to 6,7,0

	if (y==0)
		nDisable[4] = nDisable[5] = nDisable[6] = 1;

	else if (y == img->GetImageHeight()-1)
		nDisable[0] = nDisable[1] = nDisable[2] = 1;

	for (int count=0; count<8; count++) // Anjan changed 7 to 8
		if (!nDisable[count] &&
			nPixel == img->GetPixel(x + nPixelSurround[count][0], y + nPixelSurround[count][1]))
			return 0;

	//printf("Single pixel found at (%d, %d).\n", x, y);
	return 1;
}
