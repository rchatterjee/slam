#ifndef	RANDOM_H
#define	RANDOM_H

class	CRandom
{
public:
	CRandom();
	virtual	~CRandom();
private:
	const	static	long	IM1;	// = 2147483563
	const	static	long	IM2;	// = 2147483399
	const	static	double	AM ;	// = (1.0/IM1)
	const	static	long	IMM1;	// = (IM1 - 1)
	const	static	long	IA1;	// = 40014
	const	static	long	IA2;	// = 40692
	const	static	long	IQ1;	// = 53668
	const	static	long	IQ2;	// = 52774
	const	static	long	IR1;	// = 12211
	const	static	long	IR2;	// = 3791
	const	static	long	NTAB;	// = 32
	const	static	long	NDIV;	// = (1+IMM1/NTAB)
	const	static	double	EPS;	// = 1.2e-7
	const	static	double	RNMX;	// = (1.0 - EPS)
	const	static	double	F_val[][2][44];

	static	long	idum;			// = -19961001
protected:
	void    static	InitRandom();
public:
	double	static	GetF_Value(int enAccr, int index, int NumBands);
	double	static	frand();
	int		static	Random(int mx);
	double	static	Gauss(double mu,double sd);
};

typedef	unsigned long	ulong;

ulong CombinedMeanVar(double m1, double ssq1, ulong n1, 
									double m2, double ssq2, ulong n2,
									double * mu, double * ssq);
ulong AddOneMoreElement(double *mu_x, double *ssq_x, ulong num, double x);

#endif	/*RANDOM_H*/


