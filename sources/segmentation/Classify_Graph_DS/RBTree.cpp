
#define RBNEG -110000000
typedef double CKey;
typedef int CValue;



class TreeNode{//<class CKey, class CValue>
private:

public:
	int Parent;
	char Colour;
	int Left, Right;;
	CKey Key;
};

class RBTree{//<class CKey, class CValue>
	TreeNode *TreeArr;//<class CKey class CValue> 
	int Size;
	int Root,NIL;
public:
	//void AllocTree(int Size);
	//int Find(CKey Key);
	int TreeMinimum(int x);
	int RBTreeMaximum();
	int TreeSuccessor(int x);
	void TreeInsert(CKey Key, CValue Value);
	void Insert(CKey Key, CValue Value);
	//BOOL Erase(CKey Key, CValue Value);
	void LeftRotate(int x);
	void RightRotate(int x);
	void Delete(int z);
	void RBDeleteFixup(int x);
	//BOOL Erase(int ArrIndex);
	//RBTree();
	void AllocRBTree(int size);
	~RBTree(){
	delete[] TreeArr;
	//for(int i=0;i<Size+1;i++)
	//	free(TreeArr[i]);
	}
};

//RBTree::~RBTree(){
//	delete[] TreeArr;
	//for(int i=0;i<Size+1;i++)
	//	free(TreeArr[i]);
//}

void RBTree::AllocRBTree(int size){
	Size = size+4;
	TreeArr = new TreeNode[Size];
	NIL = Size-1;
	Root = NIL;
	for(int i=0;i<Size;i++){
		TreeArr[i].Parent = NIL;
		TreeArr[i].Right = NIL;
		TreeArr[i].Left = NIL;
		TreeArr[i].Key = RBNEG;
	}
	TreeArr[NIL].Colour = 'B'; 
}

void RBTree::LeftRotate(int x){
	int y;
	y = TreeArr[x].Right;
	TreeArr[x].Right = TreeArr[y].Left;
	if(TreeArr[y].Left!= NIL){
		TreeArr[TreeArr[y].Left].Parent = x;
	}
	TreeArr[y].Parent = TreeArr[x].Parent;
	if(TreeArr[x].Parent == NIL){
		Root = y;
	}
	else{
		if(x == TreeArr[TreeArr[x].Parent].Left)TreeArr[TreeArr[x].Parent].Left = y;
		else TreeArr[TreeArr[x].Parent].Right = y;
	}
	TreeArr[y].Left = x;
	TreeArr[x].Parent = y;
}


void RBTree::RightRotate(int x){
	int y;
	y = TreeArr[x].Left;
	TreeArr[x].Left = TreeArr[y].Right;
	if(TreeArr[y].Right!= NIL){
		TreeArr[TreeArr[y].Right].Parent = x;
	}
	TreeArr[y].Parent = TreeArr[x].Parent;
	if(TreeArr[x].Parent == NIL){
		Root = y;
	}
	else{
		if(x == TreeArr[TreeArr[x].Parent].Right)TreeArr[TreeArr[x].Parent].Right = y;
		else TreeArr[TreeArr[x].Parent].Left = y;
	}
	TreeArr[y].Right = x;
	TreeArr[x].Parent = y;
}


void RBTree::TreeInsert(CKey Key, CValue Value){
	TreeArr[Value].Key = Key;
	TreeArr[Value].Parent = NIL;TreeArr[Value].Left = NIL;TreeArr[Value].Right = NIL;
	int y = NIL;
	int x = Root;
	if(Value==1726){
	Value=Value;
	}
	while( x != NIL){
		y = x;
		if(Key<TreeArr[x].Key)x = TreeArr[x].Left;
		else x = TreeArr[x].Right;
	}
	TreeArr[Value].Parent = y;
	if( y == NIL){
		Root = Value;
	}
	else{
		if(TreeArr[Value].Key<TreeArr[y].Key)TreeArr[y].Left = Value;
		else TreeArr[y].Right = Value;
	}
	return;
}

void RBTree::Insert(CKey Key, CValue Value){
	int x = Value;
	int y = NIL;
	TreeInsert(Key, Value);
	TreeArr[x].Colour = 'R';
	while((x!=Root)&&(TreeArr[TreeArr[x].Parent].Colour=='R')){
		if(TreeArr[x].Parent == TreeArr[TreeArr[TreeArr[x].Parent].Parent].Left){
			y = TreeArr[TreeArr[TreeArr[x].Parent].Parent].Right;
			if(TreeArr[y].Colour=='R'){
				TreeArr[TreeArr[x].Parent].Colour='B';
				TreeArr[y].Colour='B';
				TreeArr[TreeArr[TreeArr[x].Parent].Parent].Colour='R';
				x = TreeArr[TreeArr[x].Parent].Parent;
			}
			else {
				if(x == TreeArr[TreeArr[x].Parent].Right){
					x = TreeArr[x].Parent;
					LeftRotate(x);
				}
				TreeArr[TreeArr[x].Parent].Colour='B';
				TreeArr[TreeArr[TreeArr[x].Parent].Parent].Colour='R';
				RightRotate(TreeArr[TreeArr[x].Parent].Parent);
			}
		}
		else{
			y = TreeArr[TreeArr[TreeArr[x].Parent].Parent].Left;
			if(TreeArr[y].Colour=='R'){
				TreeArr[TreeArr[x].Parent].Colour='B';
				TreeArr[y].Colour='B';
				TreeArr[TreeArr[TreeArr[x].Parent].Parent].Colour='R';
				x = TreeArr[TreeArr[x].Parent].Parent;
			}
			else {
				if(x == TreeArr[TreeArr[x].Parent].Left){
					x = TreeArr[x].Parent;
					RightRotate(x);
				}
				TreeArr[TreeArr[x].Parent].Colour='B';
				TreeArr[TreeArr[TreeArr[x].Parent].Parent].Colour='R';
				LeftRotate(TreeArr[TreeArr[x].Parent].Parent);
			}
		}
	}
	TreeArr[Root].Colour='B';
}

int RBTree::TreeMinimum(int x){
	while(TreeArr[x].Left!=NIL)x = TreeArr[x].Left;
	return x;
}

int RBTree::RBTreeMaximum(){
	int x = Root;
	if(x==NIL)return -1;
	while(TreeArr[x].Right!=NIL) x = TreeArr[x].Right;
	return x;
}

int RBTree::TreeSuccessor(int x){
	int y = NIL;
	if(TreeArr[x].Right!=NIL)return TreeMinimum(TreeArr[x].Right);
	y = TreeArr[x].Parent;
	while((y!=NIL)&&(x == TreeArr[y].Right)){
		x = y;
		y = TreeArr[y].Parent;
	}
	return y;
}


void RBTree::RBDeleteFixup(int x){
	int w=NIL;
	while((x!=Root)&&(TreeArr[x].Colour=='B')){
		if(x == TreeArr[TreeArr[x].Parent].Left){
			w = TreeArr[TreeArr[x].Parent].Right;
			if(TreeArr[w].Colour == 'R'){
				TreeArr[w].Colour = 'B';
				TreeArr[TreeArr[x].Parent].Colour = 'R';
				LeftRotate(TreeArr[x].Parent);
				w = TreeArr[TreeArr[x].Parent].Right;
			}
			if((TreeArr[TreeArr[w].Left].Colour=='B')&&(TreeArr[TreeArr[w].Right].Colour=='B')){
				TreeArr[w].Colour = 'R';
				x = TreeArr[x].Parent;
			}
			else{
				if(TreeArr[TreeArr[w].Right].Colour=='B'){
					TreeArr[TreeArr[w].Left].Colour = 'B';
					TreeArr[w].Colour = 'R';
					RightRotate(w);
					w = TreeArr[TreeArr[x].Parent].Right;
				}
				TreeArr[w].Colour = TreeArr[TreeArr[x].Parent].Colour;
				TreeArr[TreeArr[x].Parent].Colour = 'B';
				TreeArr[TreeArr[w].Right].Colour = 'B';
				LeftRotate(TreeArr[x].Parent);
				x = Root;
			}
		}
		else{
			w = TreeArr[TreeArr[x].Parent].Left;
			if(TreeArr[w].Colour == 'R'){
				TreeArr[w].Colour = 'B';
				TreeArr[TreeArr[x].Parent].Colour = 'R';
				RightRotate(TreeArr[x].Parent);
				w = TreeArr[TreeArr[x].Parent].Left;
			}
			if((TreeArr[TreeArr[w].Right].Colour=='B')&&(TreeArr[TreeArr[w].Left].Colour=='B')){
				TreeArr[w].Colour = 'R';
				x = TreeArr[x].Parent;
			}
			else{
				if(TreeArr[TreeArr[w].Left].Colour=='B'){
					TreeArr[TreeArr[w].Right].Colour = 'B';
					TreeArr[w].Colour = 'R';
					LeftRotate(w);
					w = TreeArr[TreeArr[x].Parent].Left;
				}
				TreeArr[w].Colour = TreeArr[TreeArr[x].Parent].Colour;
				TreeArr[TreeArr[x].Parent].Colour = 'B';
				TreeArr[TreeArr[w].Left].Colour = 'B';
				RightRotate(TreeArr[x].Parent);
				x = Root;
			}
		}
	}
	TreeArr[x].Colour = 'B';
}







void RBTree::Delete(int z){
	if(TreeArr[z].Key==RBNEG)return;
	int y=NIL, x=NIL;
	char Coly;
	if((TreeArr[z].Left==NIL)||(TreeArr[z].Right==NIL)){
		y = z;
	}
	else y = TreeSuccessor(z);
//	if((y==322)&&(z==101)){//Debug{
//		y = y;
//	}
	if(TreeArr[y].Left!=NIL) x = TreeArr[y].Left;
	else x = TreeArr[y].Right;
	TreeArr[x].Parent = TreeArr[y].Parent;
	if(TreeArr[y].Parent == NIL){
		Root = x;
	}
	else{
		if(y == TreeArr[TreeArr[y].Parent].Left)TreeArr[TreeArr[y].Parent].Left = x;
		else TreeArr[TreeArr[y].Parent].Right = x;
	}
	Coly = TreeArr[y].Colour;
	if(y!=z){
		//Coly = TreeArr[y].Colour;
		if(z == TreeArr[y].Parent)TreeArr[x].Parent = y;
		TreeArr[z].Key = TreeArr[y].Key;
//		if((y==101)&&(TreeArr[z].Right==322))printf("TTT");//Debug
		TreeArr[y] = TreeArr[z];
		//TreeArr[y].Parent = TreeArr[z].Parent;
		//TreeArr[y].Right = TreeArr[z].Right;
		//reeArr[y].Left = TreeArr[z].Left);
		if(TreeArr[y].Right!=NIL)TreeArr[TreeArr[y].Right].Parent = y;
		if(TreeArr[y].Left!=NIL)TreeArr[TreeArr[y].Left].Parent = y;
		if(Root == z)Root = y;
		
		if(TreeArr[z].Parent!=NIL){
			if(z == TreeArr[TreeArr[z].Parent].Left)TreeArr[TreeArr[z].Parent].Left = y;
			else TreeArr[TreeArr[z].Parent].Right = y;
		}
	}
	if(Coly == 'B')RBDeleteFixup(x);
	TreeArr[z].Key = RBNEG;
	TreeArr[z].Parent = NIL;TreeArr[z].Left = NIL; TreeArr[z].Right = NIL;
	//return z;
}
/*
int RBtree::FindNode(CKey Key, CValue Value){
	int x = Root;
	while(x!=NIL){
		if(Key==TreeArr[x].Key){break;}
		if(Key<TreeArr[x].Key)x = TreeArr[x].Left;
		else x = TreeArr[x].Right;
	}
	if(x==NIL)return -1;
	do{
		if(Value==xValue)return x;
	}while((x=TreeSuccessor(x))!=NIL);
	return NULL;
}
*/

