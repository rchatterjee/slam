// ScanFill.h: interface for the CScanFill class.
//	version fusion 3.4 July 2003 derived from fusion2.0
//	adjusted according to changes in ScanFill.cpp
//	added facility to choose between stacked and recursive scanfill()
//
//////////////////////////////////////////////////////////////////////
#define ScanFillH

//uncomment following for the stacked scanfill() instead of recursive
//#define STACKEDSCANFILL


#if !defined(AFX_SCANFILL_H__E31567C1_82D9_11D1_90B4_444553540000__INCLUDED_)
#define AFX_SCANFILL_H__E31567C1_82D9_11D1_90B4_444553540000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Array.h"

class CImage;
class CScanFill
{
private:
	CStack<int>	m_aX;
	CStack<int> m_aY;
	void Push(int x, int y);
	int Pop(int &x, int &y);
	int IsStackEmpty()const;
protected:
	CImage *m_pImg;
	int	X, Y;
	CImage * GetImage()const { return m_pImg;};
	virtual int  TestPixel(int x, int y, int nFillColor, int nOldColor);
	virtual void SetPixel(int x, int y, int nPixel);
	virtual int  GetPixel(int x, int y)const;

#ifdef STACKEDSCANFILL
	BOOL SetNextSeed(int x, int y, int xr, int nFillColor, int nOldColor);
#endif

public:
	CScanFill();
	~CScanFill();

	void AttachImage(CImage *pImg);

#ifdef STACKEDSCANFILL
	virtual int ScanFill(int x, int y, int nFillColor, int nOldColor);
#else
	virtual int ScanFill(int x, int y, int nFillColor, int nOldColor, int filledSoFar =0);
#endif

};

// !defined(AFX_SCANFILL_H__E31567C1_82D9_11D1_90B4_444553540000__INCLUDED_)
class CRAGScanFill : public CScanFill
{
protected:
	CImage *m_pOut;
	int m_nPixel;
public:
	CRAGScanFill()
		:CScanFill()
	{
		m_pOut = NULL;
	};
	void AttachOutImage(CImage *pOut) { m_pOut = pOut; };
	CImage *GetOutImage()const { return m_pOut; };
	void SetPixel(int x, int y, int nPixel)
	{ m_pOut->SetPixel(x, y, m_nPixel); };
	int TestPixel(int x, int y, int nFillColor, int nOldColor)
	{
		if(m_pOut->GetPixel(x, y))
			return 0;
		if(nOldColor == m_pImg->GetPixel(x, y))
			return 1;
		return 0;
	}
	int Segment();
};
#endif 

