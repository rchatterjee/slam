//	RagLoop.cpp Version fusion3.7
//	the sequence of data processing performed on the images.
//
//	now facilitates option to read DS labels from a label file,
//	 by calling RAGloop with MaxCard = 0.
//

//uncomment following to make the RAG only and no energy minimization
//#define MAKERAGONLY

#define DSFILE "DSLabels.asc"
#include <string>
//#include "mass1.cpp"
//#define DS
//#ifdef DS
//	define CLASSIFYPIXELS() ClassifyPixels()
using namespace std;
//#define DEMPSTER
/*
#ifdef DEMPSTER

#	ifdef KAUSTAVDS
#		define CLASSIFYPIXELS() ClassifyPixels(num_class, img, NumSensors, NumBandsArr, fileForDS, MaxCard, flag)
#	else
#		define CLASSIFYPIXELS() ClassifyPixels(num_class, img, NumSensors, NumBandsArr, fileForDS, MaxCard, PixelLabel)
#	endif
*/

//#define INITPIXELLABEL(ARG) \
{\
	if(ARG)\
        	CLASSIFYPIXELS();\
 	else\
        	ASCfileToMemory(PixelLabel, fileForDS, nWidth, nHeight);\
}\

#include <fstream>
void ASCfileToMemory(int** memory, char* filename, int width, int height)
{
	ifstream in(filename);

	for (int i=0; i<height; i++)
        	for (int j=0; j<width; j++)			
			in >> memory[i][j];

   	in.close();
}

//#endif


int **PixelLabel;

void RAGLoop(char pszOrgImg[][250],int NumBands, int NumSensors, int *NumBandsArr, int nWidth, int nHeight, int nOrgType,
 char *pszSegImg, int nSegType, int nLevel, int nDisplayLevel, int num_class, int MaxCard,
 const string& dsLambdaFile, int flag, int blue, int green, int red, int ir)
{
	CImage::FileType	ftOrg, ftSeg;
	ftOrg = (nOrgType > 0) ? CImage::ftText : CImage::ftBinary;
	ftSeg = (nSegType > 0) ? CImage::ftText : CImage::ftBinary;
	
	CImage	*imgOrg = new CImage[NumBands];

	CImage **img = new CImage*[NumSensors];

	int i, j, k = 0;
	for(j=0; j<NumSensors; j++){
		img[j] = new CImage[NumBandsArr[j]];
		for(i=0; i<NumBandsArr[j]; i++){
			img[j][i].CImagex(pszOrgImg[k], nWidth, nHeight, ftOrg);
			imgOrg[k].CImagex(pszOrgImg[k], nWidth, nHeight, ftOrg);
			k++;
		}
	}
	
	PixelLabel = new int*[nHeight];
	for(i=0; i<nHeight; i++)
        {
		PixelLabel[i] = new int[nWidth];
	}
	
	ClassifyPixels(num_class, img, NumSensors, NumBandsArr, (char *)"2", MaxCard, dsLambdaFile, blue, green, red, ir);
	ASCfileToMemory(PixelLabel, (char*)DSFILE, nWidth, nHeight);
	
	CImage	imgSeg(pszSegImg, nWidth, nHeight, ftSeg);

	printf("ClassifyPixels Completed...\n");
	//cout << "Adjusting bounding pixels : ";
	imgSeg.AdjustBoundingPixels();
	//cout << "Done." << endl;

	CGraph rag(num_class);
	rag.NumBands = NumBands;
	rag.AttachImagesx(&imgSeg, imgOrg);
	printf("\nMaking the RAG : ");
	fflush(stdout);

	rag.MakeRAG();
	
#ifndef MAKERAGONLY
	rag.ExhaustiveSearch();
	rag.UpdateSegImg();
	//if(!imgSeg.SaveImageAs("segment.bin"))
	//	cout <<"Image SaveAs failed.." << endl;

	if(!imgSeg.SaveImageAs("segment.asc", CImage::ftText))
		cout <<"Image SaveAs failed.." << endl;
#endif
	delete[] imgOrg;
	for(j=0; j<NumSensors; j++)delete[]	(img[j]);
	printf("Done\n");
}
