//	AdjRBTree.cpp, Version Fusion4.01
//	Defines the red black tree structure for keeping track of potentials with neighbours.
//
//	(version Fusion4.01) added function for enumerating node values
//	with keys above a specified threshold.
//
#include <list>

#include<iostream>
#include <sys/timeb.h>
#include <time.h>
#include<map>
#include<utility>
#define ADJRB_NEG -1000000
#define NILV -500000000
using namespace std;
typedef double CKey;
typedef int CValue;


class AdjTreeNode{//<class CKey, class CValue>
public:
	char Colour;
	AdjTreeNode *Parent;
	AdjTreeNode *Left;
	AdjTreeNode *Right;
	CKey Key;
	CValue Value;
	AdjTreeNode(AdjTreeNode* NIL=NULL){
		Key = ADJRB_NEG;
		Value = 0;
		Parent = Left = Right = NIL;
	}
	//AdjTreeNode(void);

	typedef pair<CKey, CValue> pairtype;
	int recurListAbove(CKey threshold, list<pairtype>& aboves, AdjTreeNode* NIL, int totalNeighbours =0);
};

int AdjTreeNode::recurListAbove(CKey threshold, list<pairtype>& aboves, AdjTreeNode* NIL, int totalNeighbours)
{
	if(this==NIL)
		return totalNeighbours;

	if(!(Key <= threshold))
		aboves.push_back(pairtype(Key, Value));

	totalNeighbours = Left->recurListAbove(threshold, aboves, NIL, totalNeighbours);
	totalNeighbours = Right->recurListAbove(threshold, aboves, NIL, totalNeighbours);

	return ++totalNeighbours;
}

class AdjRBTree{//<class CKey, class CValue>
public:
	AdjTreeNode *Root;//<class CKey class CValue>
	int Size;
	AdjTreeNode *NIL;

	//void AllocTree(int Size);
	//int Find(CKey Key);
	AdjTreeNode* TreeMinimum(AdjTreeNode* x);
	AdjTreeNode* TreeMaximum(AdjTreeNode* x);
	AdjTreeNode* RBTreeMaximum();
	AdjTreeNode* TreeSuccessor(AdjTreeNode* x);
	AdjTreeNode* TreePredecessor(AdjTreeNode* x);
	//AdjTreeNode* FindValue(CValue Value);
	AdjTreeNode* TreeInsert(CKey Key, CValue Value);
	void Insert(CKey Key, CValue Value);
	//BOOL Erase(CKey Key, CValue Value);
	void LeftRotate(AdjTreeNode* x);
	void RightRotate(AdjTreeNode* x);
	void Delete(AdjTreeNode* z);
	void RBDeleteFixup(AdjTreeNode* x);
	void DeleteRecur(AdjTreeNode* Root);
	AdjTreeNode* FindNode(CKey Key, CValue Value);
	//BOOL Erase(int ArrIndex);
	AdjRBTree();
	~AdjRBTree();

	int listAbove(CKey threshold, list<AdjTreeNode::pairtype>& aboves)
	{
		return Root->recurListAbove(threshold, aboves, NIL);
	}
};

void AdjRBTree::DeleteRecur(AdjTreeNode* tRoot){
	if(tRoot==NIL)return;
	DeleteRecur(tRoot->Right);
	DeleteRecur(tRoot->Left);
	delete tRoot;
}

AdjRBTree::~AdjRBTree(){
	DeleteRecur(Root);
	if(NIL)delete NIL;
}

AdjRBTree::AdjRBTree(){
	NIL = new AdjTreeNode;
	Root = NIL;
	Size = 0;
	NIL->Colour = 'B';
	NIL->Parent = NIL;
	NIL->Right = NIL->Left = NIL;
	NIL->Key = NILV;
	NIL->Value= NILV;
}

void AdjRBTree::LeftRotate(AdjTreeNode* x){
	AdjTreeNode* y;
	if(!x)return;
	y = x->Right;
	x->Right = y->Left;
	if(y->Left!= NIL){
		(y->Left)->Parent = x;
	}
	y->Parent = x->Parent;
	if(x->Parent == NIL){
		Root = y;
	}
	else{
		if(x == (x->Parent)->Left)(x->Parent)->Left = y;
		else (x->Parent)->Right = y;
	}
	y->Left = x;
	x->Parent = y;
}


void AdjRBTree::RightRotate(AdjTreeNode* x){
	AdjTreeNode* y;
	if(!x)return;
	y = x->Left;
	x->Left = y->Right;
	if(y->Right!= NIL){
		(y->Right)->Parent = x;
	}
	y->Parent = x->Parent;
	if(x->Parent == NIL){
		Root = y;
	}
	else{
		if(x == (x->Parent)->Right)(x->Parent)->Right = y;
		else (x->Parent)->Left = y;
	}
	y->Right = x;
	x->Parent = y;
}


AdjTreeNode* AdjRBTree::TreeInsert(CKey Key, CValue Value){
	AdjTreeNode* y = NIL;
	AdjTreeNode* x = Root;
	AdjTreeNode* New = new AdjTreeNode(NIL);
	New->Key = Key;
	New->Value = Value;
	while( x != NIL){
		y = x;
		if(Key<x->Key)x = x->Left;
		else x = x->Right;
	}
	New->Parent = y;
	if( y == NIL){
		Root = New;
	}
	else{
		if(New->Key<y->Key)y->Left = New;
		else y->Right = New;
	}
	return New;
}

void AdjRBTree::Insert(CKey Key, CValue Value){
	AdjTreeNode* x;
	AdjTreeNode* y = NIL;
	x = TreeInsert(Key, Value);
	x->Colour = 'R';
	while((x!=Root)&&((x->Parent)->Colour=='R')){
		if(x->Parent == ((x->Parent)->Parent)->Left){
			y = ((x->Parent)->Parent)->Right;
			if(y->Colour=='R'){
				(x->Parent)->Colour='B';
				y->Colour='B';
				((x->Parent)->Parent)->Colour='R';
				x = (x->Parent)->Parent;
			}
			else {
				if(x == (x->Parent)->Right){
					x = x->Parent;
					LeftRotate(x);
				}
				(x->Parent)->Colour='B';
				((x->Parent)->Parent)->Colour='R';
				RightRotate((x->Parent)->Parent);
			}
		}
		else{
			y = ((x->Parent)->Parent)->Left;
			if(y->Colour=='R'){
				(x->Parent)->Colour='B';
				y->Colour='B';
				((x->Parent)->Parent)->Colour='R';
				x = (x->Parent)->Parent;
			}
			else {
				if(x == (x->Parent)->Left){
					x = x->Parent;
					RightRotate(x);
				}
				(x->Parent)->Colour='B';
				((x->Parent)->Parent)->Colour='R';
				LeftRotate((x->Parent)->Parent);
			}
		}
	}
	(Root)->Colour='B';
}

AdjTreeNode* AdjRBTree::TreeMinimum(AdjTreeNode* x){
	if(!x){cout<<"RB_Error1";return NIL;}
	while(x->Left!=NIL)x = x->Left;
	return x;
}

AdjTreeNode* AdjRBTree::TreeMaximum(AdjTreeNode* x){
	if(!x){cout<<"RB_Error1";return NIL;}
	while(x->Right!=NIL)x = x->Right;
	return x;
}

AdjTreeNode* AdjRBTree::RBTreeMaximum(){
	AdjTreeNode* x = Root;
	if(x==NIL)return NULL;
	while(x->Right!=NIL)x = x->Right;
	return x;
}

AdjTreeNode* AdjRBTree::TreeSuccessor(AdjTreeNode* x){
	AdjTreeNode* y = NIL;
	if(!x){cout<<"RB_Error2";return NIL;}
	if(x->Right!=NIL)return TreeMinimum(x->Right);
	y = x->Parent;
	while((y!=NIL)&&(x == y->Right)){
		x = y;
		y = y->Parent;
	}
	return y;
}

AdjTreeNode* AdjRBTree::TreePredecessor(AdjTreeNode* x){
	AdjTreeNode* y = NIL;
	if(!x){cout<<"RB_Error2";return NIL;}
	if(x->Left!=NIL)return TreeMaximum(x->Left);
	y = x->Parent;
	while((y!=NIL)&&(x == y->Left)){
		x = y;
		y = y->Parent;
	}
	return y;
}


void AdjRBTree::RBDeleteFixup(AdjTreeNode* x){
	AdjTreeNode* w=NIL;
	if(!x){cout<<"RB_Error3";return;}
	while((x!=Root)&&(x->Colour=='B')){
		if(x == (x->Parent)->Left){
			w = (x->Parent)->Right;
			if(w->Colour == 'R'){
				w->Colour = 'B';
				(x->Parent)->Colour = 'R';
				LeftRotate(x->Parent);
				w = (x->Parent)->Right;
			}
			if(((w->Left)->Colour=='B')&&((w->Right)->Colour=='B')){
				w->Colour = 'R';
				x = x->Parent;
			}
			else{
				if((w->Right)->Colour=='B'){
					(w->Left)->Colour = 'B';
					w->Colour = 'R';
					RightRotate(w);
					w = (x->Parent)->Right;
				}
				w->Colour = (x->Parent)->Colour;
				(x->Parent)->Colour = 'B';
				(w->Right)->Colour = 'B';
				LeftRotate(x->Parent);
				x = Root;
			}
		}
		else{
			w = (x->Parent)->Left;
			if(w->Colour == 'R'){
				w->Colour = 'B';
				(x->Parent)->Colour = 'R';
				RightRotate(x->Parent);
				w = (x->Parent)->Left;
			}
			if(((w->Right)->Colour=='B')&&((w->Left)->Colour=='B')){
				w->Colour = 'R';
				x = x->Parent;
			}
			else{
				if((w->Left)->Colour=='B'){
					(w->Right)->Colour = 'B';
					w->Colour = 'R';
					LeftRotate(w);
					w = (x->Parent)->Left;
				}
				w->Colour = (x->Parent)->Colour;
				(x->Parent)->Colour = 'B';
				(w->Left)->Colour = 'B';
				RightRotate(x->Parent);
				x = Root;
			}
		}
	}
	x->Colour = 'B';
}







void AdjRBTree::Delete(AdjTreeNode* z){
	if(!z){cout<<"RB_Error4";return;}
	//if((z)->Key==ADJRB_NEG)return;
	if(z==NIL){cout<<"RB_Error5";return;}
	AdjTreeNode* y=NIL;
	AdjTreeNode* x=NIL;
	if(((z)->Left==NIL)||((z)->Right==NIL)){
		y = z;
	}
	else y = TreeSuccessor(z);
	if(y->Left!=NIL) x = y->Left;
	else x = y->Right;
	x->Parent = y->Parent;
	if(y->Parent == NIL){
		Root = x;
	}
	else{
		if(y == (y->Parent)->Left)(y->Parent)->Left = x;
		else (y->Parent)->Right = x;
	}
	if(y!=z){
		//Coly = y->Colour;
		z->Key = y->Key;
		z->Value = y->Value;
		//if(Root==z)Root=y;
	}
	if(y->Colour == 'B')RBDeleteFixup(x);
	//(z)->Key = ADJRB_NEG;
	delete y;
}
int equal(double x, double y){
	double diff = x-y;
	if(diff<0)diff = -diff;
	if(diff<1e-3)return 1;
	return 0;
}
AdjTreeNode* AdjRBTree::FindNode(CKey Key, CValue Value){
	AdjTreeNode* x = Root;
	AdjTreeNode* y;
	while(x!=NIL){
		if(equal(Key,x->Key)){break;}
		if(Key<x->Key)x = x->Left;
		else x = x->Right;
	}
	if(x==NIL)return NULL;
	y = x;
	do{
		if((Value==y->Value)&&equal(Key,y->Key))return y;
	}while(((y=TreeSuccessor(y))!=NIL)&&equal(Key,y->Key));
	do{
		if((Value==x->Value)&&equal(Key,x->Key))return x;
	}while(((x=TreePredecessor(x))!=NIL)&&equal(Key,x->Key));

	return NULL;
}


