//	Graph1.cpp, Version fusion3.9
//	CGraph is the implementation of the RAG used for MRF based classification.
//
//	(version 3.1) error in MakeEdges(), viz. missing out on some possible edges
//	 due to improper pixel traversal, has been fixed by generalizing the steps with
//	 which the pixels are traversed, setting a value of 1 for guaranteed correctness.
//
//	(version 3.3) option for enabling removesinglepixels() for multiple bands too.
//	(version 3.4) option for enabling DS fusion using DSNode instead of CNode.
//
//	(version 3.7) DS checking fixed. Now centerpixels & region DS counts both get
//	 updated on merging regions. Also facility added for saving RAG to a file.
//
//	WARNING: saveRagToFile works safest just after RAG has been constructed. Because
//	 after that nodes may be deleted and the indices will remain unoccupied. So a
//	 continuous iteration through the indices would not be correct.
//
//	(version 3.9) exported calcCentrePixels() to CNode.

//	comment the following line to disable removesinglepixels() for more than one bands.
#define REMOVESINGLES4ALLBANDS
#define MAKEDGESNODESTEP 1

//	uncomment for saving majority-DS labels on RAG to separate file 'ragDSlabels.asc'
#define SAVERAGDSLABELS

//	uncomment the following to save the RAG to a file 'RAG.txt'
#define SAVERAG

//	uncomment following for merging all possible neighbours of the best node at each step
//#define GRAPH1MERGEALLPOSSIBLE

//	for debugging purposes
#define GRAPH1CPPVERBOSE 1

#include <list>
#include <cstdio>
#include <iostream>

//#include <sys/timeb.h>
//#include <time.h>

#if defined(DEMPSTER)
#	if defined(CENTERPIXELSONLY)
#		define USEGRAPH1CENPIXNODE
#	else
#		define USEGRAPH1DSNODE
#		if defined(ORCENTERMAJOR)
#			define USEGRAPH1CENPIXNODE
#		endif
#	endif
#endif

#ifdef USEGRAPH1DSNODE
#	include "DSNode.cpp"
#endif

#ifdef USEGRAPH1CENPIXNODE
#	include "CenPixNode.cpp"
#endif

#ifdef ORCENTERMAJOR
#	include "OrCenterMajorNode.cpp"
#endif

#include "CNodeEnergy.cpp"
#include "CMerge.cpp"
#include "ScanFill.h"
#include "Globals.h"
using namespace std;
typedef int itype;
typedef double vtype;
#define NEG -100000000
#define SAFENEG -90000000
#define PSZERO -70000000
#define SAFEZERO -60000000

void algo2_pixel(int no,int no_wd,int nc);
double *SBij,*SWij,*dX,*dMuX,**dSqX;
double *dM, *dTemp, **dS, **inv_dS,*mu,**ssq;
int isSinglePixel (int x, int y, CImage* img);


#ifdef USEGRAPH1CENPIXNODE
int **isCentrePixel;
#endif


class CGraph
{
	int _nClasses; // required primarily for DS fusion, majority-pixel based

        public: CGraph(int nClasses):
         _nClasses(nClasses)
        {
        }

#ifdef SAVERAG
	void sendAdjTreeTo(FILE* f, AdjTreeNode* treenode, AdjTreeNode* NIL);
	void saveRagToFile(char* filename);
#endif

public:
	CNode **GraphArr;
	CImage **OrgImg,*SegImg;
	CMerge Merge;
	CNodeEnergy NodeEnergy;
	int NumNodes,ArrSize,NumBands;

	BOOL AllocGraph(itype size);
	BOOL MakeRAG();
	BOOL AddEdge(itype index1, itype index2);
	BOOL AttachImagesx(CImage *pSegImg, CImage *pOrgImg);
	void MakeEdges();
	vtype MergeNodes(itype index1,itype index2);
	void ConstructVMap();
	void DeleteNode(itype index);
	itype RemoveSinglePixels(int num, int flex);
	void ExhaustiveSearch();
	void AttachImages(CImage *imgSeg,CImage *imgOrg);
	void UpdateSegImg();
	void InitializeNodeEnergy();
	void UpdateNeighbours1(AdjTreeNode *Node, AdjTreeNode *NIL, itype Index);
	void UpdateNeighbours2(AdjTreeNode *Node, AdjTreeNode *NIL, itype Index);
	void NeighbourMerge(AdjTreeNode *Node, AdjTreeNode *NIL, double x0, int *flag,
 itype *Ngh);
	~CGraph();
	//CGraph(int Size){NodeEnergy(Size);};
};

CGraph::~CGraph(){
	int i;
	printf("\nDeleting graph...: %s", __FILE__);fflush(stdout);
	delete[] SWij;
	delete[] dMuX;
	delete[] dX;
	delete[] mu;
	for(i=0;i<NumBands;i++){
		delete[] ssq[i];
		delete[] dSqX[i];
		delete[] dS[i];
		delete[] inv_dS[i];
	}
	delete ssq;
	delete[] SBij;
	delete[] dSqX;
	delete[] dM;
	delete[] dS;
	delete[] dTemp;
	delete[] inv_dS;
	if(OrgImg){
		//for(i=0;i<NumBands;i++)delete(OrgImg[i]);
	delete[] OrgImg;
	}
	//if(SegImg)delete SegImg;
	for(i=0;i<ArrSize;i++)
		delete(GraphArr[i]);
	delete[] GraphArr;
	printf(".......successfully deleted.\n");
}
void CGraph::InitializeNodeEnergy(){
	if(!GraphArr){cout<<"\n Error12";return;}
	int i;
	for(i=0;i<ArrSize;i++){
		if(!GraphArr[i]){continue;}
		NodeEnergy.InsertInVMap(GraphArr[i]);
	}
}

void CGraph::UpdateSegImg(){
	int Height = SegImg->GetImageHeight();
	int Width = SegImg->GetImageWidth();
	int	offset = 0;
	Merge.MakeKeyLookup();
#define SAVERAGDSLABELS	
#ifdef SAVERAGDSLABELS
	FILE * f = fopen("ragDSlabels.asc", "w");
#endif
	int	u, v, Key;
	for(u = offset; u < Height - offset; u ++)
	{
		for(v = offset; v < Width - offset; v++)
		{
			Key = SegImg->GetPixel(v, u);
			SegImg->SetPixel(v, u, Merge.KeyLookup[Key]);

#ifdef SAVERAGDSLABELS
#	ifdef DEMPSTER
			fprintf(f, "%d ", ((DSNode*)GraphArr[Merge.KeyLookup[Key]]->_actualThis)->_dsLabel);
#	endif
#endif
		}
#ifdef SAVERAGDSLABELS
		fprintf(f, "\n");
#endif
	}
#ifdef SAVERAGDSLABELS
	fclose(f);
#endif
}
void CGraph::AttachImages(CImage *imgSeg,CImage *imgOrg){
	SegImg = imgSeg;
	//OrgImg = imgOrg;
}

BOOL CGraph::AttachImagesx(CImage *pSegImg, CImage *pOrgImg)

{
	SegImg = pSegImg;
	OrgImg = new CImage*[NumBands];
	for(int i=0; i<NumBands; i++)
		OrgImg[i] = &pOrgImg[i];
	return TRUE;
}
void CGraph::DeleteNode(itype index){
	if(!GraphArr[index])return;

	if((index<ArrSize)&&(GraphArr[index])){
		delete(GraphArr[index]);
		GraphArr[index] = NULL;
		NumNodes--;
	}
}


BOOL CGraph::AllocGraph(itype size){
	GraphArr = new CNode*[size];
	if(!GraphArr)return false;
	int i;
	for(i=0;i<size;i++){

#ifdef USEGRAPH1DSNODE

#	ifdef USEGRAPH1CENPIXNODE
		OrCenterMajorNode* newnode = new OrCenterMajorNode(_nClasses, PixelLabel, OrgImg);
#	else
        	DSNode* newnode = new DSNode(_nClasses);
#	endif

#else

#	ifdef USEGRAPH1CENPIXNODE
		CenPixNode* newnode = new CenPixNode(PixelLabel, OrgImg);
#	else
		CNode* newnode = new CNode;
#	endif

#endif
		GraphArr[i] = newnode;
		if(!GraphArr[i])return false;
		GraphArr[i]->Initialize(NumBands);
		GraphArr[i]->Index = i;
		GraphArr[i]->NumPixels = 0;
		GraphArr[i]->NumEdges = 0;
		GraphArr[i]->V = NEG;
		//GraphArr[i]->Mean[0] = 0;
		//SigmaSquare = 0;
		//GraphArr[i]->NumMergable = 0;
	}
	NumNodes = ArrSize = size;
	dM = new double[NumBands];
	dTemp = new double[NumBands];
	dS = new double*[NumBands];
	inv_dS = new double*[NumBands];
	SBij = new double[NumBands];
	SWij = new double[NumBands];
	dX = new double[NumBands];
	dMuX = new double[NumBands];
	dSqX = new double*[NumBands];
	mu = new double[NumBands];
	ssq = new double*[NumBands];
	for(i=0;i<NumBands;i++){
		ssq[i] = new double[NumBands];
		dS[i] = new double[NumBands];
		inv_dS[i] = new double[NumBands];
		dSqX[i] = new double[NumBands];
	}
	return true;
}

BOOL CGraph::AddEdge(itype index1, itype index2){
	if((index1>=ArrSize)||(index2>=ArrSize))return false;
	if(!(GraphArr[index1])&&(GraphArr[index2]))return false;
	GraphArr[index1]->AddEdge(GraphArr[index2],NumBands);
	GraphArr[index2]->AddEdge(GraphArr[index1],NumBands);
	return true;
}
void CGraph::MakeEdges(){

	int Height = SegImg->GetImageHeight();
	int Width = SegImg->GetImageWidth();
	int offset=1;
	int Key,Ngh;
	for(int u = offset; u < (Height - offset); u += MAKEDGESNODESTEP)
	{
		if(u % 9 == 0)
		{
			printf("$");
			fflush(stdout);
		}
		for(int v = offset; v < (Width - offset); v += MAKEDGESNODESTEP)
		{
			Key = SegImg->GetPixel(v, u);
			for(int i = u - 1; i <= (u + 1); i ++)
			{
				for(int j = v - 1; j <= (v + 1); j++)
				{
					Ngh = SegImg->GetPixel(j, i);
					if(Ngh == Key)
						continue;
					AddEdge(Key, Ngh);
				}
			}
		}
	}
}

void CGraph::UpdateNeighbours1(AdjTreeNode *Node, AdjTreeNode *NIL, itype	//apparently deletes neighbours
 Index){
	if(Node==NIL)return;
	UpdateNeighbours1(Node->Right, NIL, Index);
	UpdateNeighbours1(Node->Left, NIL, Index);
	NodeEnergy.DeleteFromVMap(GraphArr[Node->Value]);
	GraphArr[Node->Value]->DeleteEdge_i_to_j(Node->Key,GraphArr[Index]);
}
void CGraph::UpdateNeighbours2(AdjTreeNode *Node, AdjTreeNode *NIL, itype	//apparently inserts neighbours
 Index){
	if(Node==NIL)return;
	UpdateNeighbours2(Node->Right, NIL, Index);
	UpdateNeighbours2(Node->Left, NIL, Index);
	GraphArr[Node->Value]->AddEdge_i_to_j(Node->Key, GraphArr[Index]);
	NodeEnergy.InsertInVMap(GraphArr[Node->Value]);
}

vtype CGraph::MergeNodes(itype index_i,itype index_j){

	//printf("[begin..");
	//fflush(stdout);

	if(!(GraphArr[index_i])&&(GraphArr[index_j])){cout<<"\nError7";return NEG;}
	AdjRBTree *adj_i,*adj_j,*union_adj;

	vtype energyReleased = GraphArr[index_i]->Calc_Vij(GraphArr[index_j], NumBands); // anjan

	adj_i=GraphArr[index_i]->GetAdjMap();
	adj_j=GraphArr[index_j]->GetAdjMap();
	NodeEnergy.DeleteFromVMap(GraphArr[index_i]);

	UpdateNeighbours1(adj_i->Root, adj_i->NIL, index_i);
	UpdateNeighbours1(adj_j->Root, adj_j->NIL, index_j);

	GraphArr[index_i]->MergeNode(GraphArr[index_j],GraphArr,NumBands);
	union_adj = GraphArr[index_i]->GetAdjMap();

	UpdateNeighbours2(union_adj->Root, union_adj->NIL, index_i);

	NodeEnergy.InsertInVMap(GraphArr[index_i]);
	Merge.Merge_j_to_i(index_i,index_j);

	DeleteNode(index_j);

	//printf("..end].");
	//fflush(stdout);

	return energyReleased;
}


void CGraph::ConstructVMap(){
	for(int i=0;i<ArrSize;i++){
		if(!GraphArr[i])continue;
		NodeEnergy.InsertInVMap(GraphArr[i]);
	}
}

#	ifdef SAVERAG

void CGraph::sendAdjTreeTo(FILE* f, AdjTreeNode* treenode, AdjTreeNode* NIL)
{
	if(treenode==NULL || treenode==NIL)
		return;

	fprintf(f, "%d ", treenode->Value);

	sendAdjTreeTo(f, treenode->Left, NIL);
	sendAdjTreeTo(f, treenode->Right, NIL);
}

void CGraph::saveRagToFile(char* filename)
{
	FILE *f = fopen(filename, "w");

	for (int i=1; i<ArrSize; i++)
	{
		fprintf(f, "%d\t%d\t", GraphArr[i]->Index, GraphArr[i]->NumPixels);
		sendAdjTreeTo(f, GraphArr[i]->AdjMap.Root, GraphArr[i]->AdjMap.NIL);
		fprintf(f, "\n");
	}
	fclose(f);
}

#	endif


BOOL CGraph::MakeRAG()
{
	// INITIAL INITIALIZATIONS
	int i;
	if( (!OrgImg ) || (!SegImg) )
		return false;
	int Width, Height;

	Width = OrgImg[0]->GetImageWidth();
	Height= OrgImg[0]->GetImageHeight();

	if( (Width != SegImg->GetImageWidth()) ||
		(Height != SegImg->GetImageHeight()) )
		return false;

	// REGENERATING THE INITSEG/MERGE FILE
	CImage imgTemp(SegImg->GetImageWidth(), SegImg->GetImageHeight());
	CRAGScanFill scanFill;
	scanFill.AttachImage(SegImg);
	scanFill.AttachOutImage(&imgTemp);
	int nReg = scanFill.Segment();
	if(nReg > 0){
		printf("*%d*", nReg);
		fflush(stdout);
		SegImg->Copy(imgTemp);
	}

#ifdef GRAPH1CPPVERBOSE
	printf(" [Now initializing Graph Structures] ");
	fflush(stdout);
#endif
	// INITIALIZING GRAPH STRUCTURES
	AllocGraph(nReg);
	NodeEnergy.InitiallizeRBTree(nReg);
	Merge.Initialize(nReg);

#ifdef GRAPH1CPPVERBOSE
	printf(" [Now populating nodes with pixels");
	fflush(stdout);
#endif

	// POPULATING NODES WITH PIXELS
	int	offset = 1;
	int *pixs = new int[NumBands];
	int	u, v, Key, Ngh;
	for(u = offset; u < Height - offset; u ++)
	{
		if( u % 32 == 0)
		{
			printf("#");
			fflush(stdout);
		}
		for(v = offset; v < Width - offset; v++)
		{
			Key = SegImg->GetPixel(v, u);
			if(!GraphArr[Key]){cout<<"\nError3";return false;}
			for(int l=0;l<NumBands;l++)
				pixs[l] = OrgImg[l]->GetPixel(v, u);

			GraphArr[Key]->AddPixel(pixs,NumBands);

       		((DSNode*)GraphArr[Key]->_actualThis)->addClassLabel(PixelLabel[u][v]);

		}
	}

	// DS-RELATED PROCESSING

#ifdef DEMPSTER

#	ifdef USEGRAPH1DSNODE
        for(i=0; i<nReg; i++)

#		ifdef ORCENTERMAJOR
			((OrCenterMajorNode*)GraphArr[i]->_actualThis)->fixScores();
#		else
	        ((DSNode*)GraphArr[i]->_actualThis)->fixScores();
#		endif
#	endif
#endif

	// CONNECTING NEIGHBOURING NODES
    MakeEdges();
	InitializeNodeEnergy();
	//printf("\n*%d*", NumNodes);

#ifdef SAVERAG
	char rag[]="RAG.txt";
	saveRagToFile(rag);
#endif

	// HANDLING SINGLE PIXEL REGIONS
	int nSingle = 0;
	for(int nX = 0; nX < Width; nX++)
		for (int nY = 0; nY < Height; nY++)
			if (isSinglePixel(nX, nY, SegImg))
				nSingle++;

	//cout << "\nSingle Pixel count: " << nSingle << endl;

#ifndef REMOVESINGLES4ALLBANDS
	if(NumBands==1)
#endif
   {
	//printf("\nRemoving Single Pixels:\t");
	int nRemoved = RemoveSinglePixels(2, 3);

	//printf("#%d\n", nRemoved);

	//cout << "New Single Pixel count: " << nSingle - nRemoved << endl;
	//printf("*%d*\n", NumNodes);
	}
	fflush(stdout);
	return true;
}

void CGraph::NeighbourMerge(AdjTreeNode *Node, AdjTreeNode *NIL, double x0, int
 *flag, itype *Ngh){

	int fl=1;
	if(Node==NIL)return;
	if(!GraphArr[Node->Value]){cout<<"\nError4";return;}
				//pOther = GraphArr[Node->Value];
	if(GraphArr[Node->Value]->NumPixels > 1){
		double m1 = GraphArr[Node->Value]->Mean[0];
		double s1 = GraphArr[Node->Value]->SigmaSquare[0][0];
		s1 = sqrt(s1);
		if(fabs(x0-m1) < s1)
		{
			*flag = 1;
			*Ngh = Node->Value;
			fl = 0;
		}
	}
	if(fl){
		NeighbourMerge(Node->Right, NIL, x0, flag, Ngh);
		NeighbourMerge(Node->Left, NIL, x0, flag, Ngh);
	}
	return;
}



itype CGraph::RemoveSinglePixels(int num, int flex=3)
{
	int	NumRem = 0;
	double m1, s1, x0;
	int flag = 0;
	CNode	*pOther;
	AdjRBTree *AdjMap;
	fflush(stdout);
	for(itype u = 0; u < ArrSize; u++)
	{
		if(!(NumNodes%150))
		{
			printf(".");
			fflush(stdout);
		}
		if(!GraphArr[u])continue;
		CNode *pNode = GraphArr[u];
		if(pNode->NumPixels < num)
		{
			int	NumNgh = pNode->NumEdges;
			if(NumNgh < 1)
			{
				DeleteNode(pNode->Index);
				NumRem++;
				continue;
			}
			x0 = pNode->Mean[0];
			AdjMap = GraphArr[u]->GetAdjMap();
			itype Ngh;
			flag = 0;
			NeighbourMerge(AdjMap->Root, AdjMap->NIL, x0, &flag, &Ngh);

			if(flag == 0) continue;
			if(!GraphArr[Ngh]){cout<<"\nError5";return -1;}
			pOther = GraphArr[Ngh];

			//printf("%d.", pNode->Index);
			//fflush(stdout);

			//printf("MergeNodes(,)...");
			//fflush(stdout);
			MergeNodes(Ngh, pNode->Index);
			//printf("done.");
			//fflush(stdout);
			NumRem++;
		}
	}
	return NumRem;
}

void CGraph::ExhaustiveSearch()
{
	vtype totalDrop = 0;
	vtype currentDrop;

	itype MaxVIndex,MaxContributorIndex;
	fflush(stdout);
	while(1)
	{
		if((NumNodes%32)==0){
			printf("*");
			fflush(stdout);
		}
		MaxVIndex = NodeEnergy.GetMaxVIndex();
		if(!GraphArr[MaxVIndex]){
			cout<<"\nError6";return;}
		MaxContributorIndex = GraphArr[MaxVIndex]->MaxContributor(&currentDrop);
                //printf("\nthe max contributor : %d %d",MaxContributorIndex,MaxVIndex);
		if(MaxContributorIndex==-1)break;

		int count = 0;
		int total;
		count++;
		totalDrop += MergeNodes(MaxVIndex,MaxContributorIndex);
	}
	UpdateSegImg();
	fflush(stdout);
	return;
}

