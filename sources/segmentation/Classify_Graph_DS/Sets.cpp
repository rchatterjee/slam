//	Sets.cpp Version fusion3.6
//
//	CalcSubsetsCardinal() replaced by AddSubsetsWithPrev()
//	and appropriate adaptive changes made in CalcSubsets()
//
//	demo incorporated with all KSets & a NullSet
//	MaxCard has been left variable
//	STILL MIGHT GIVE ERRORS
//
#define SetsCpp

#ifndef MAIN
#	define MAIN
#	define SETSDEMO // DEMO BY ANJAN
#endif

#ifdef SETSDEMO
#	include <stdio.h>
#	include <iostream.h>
#endif

//#define MAXCARD 4
typedef int* ARRAY;
typedef int* SUBSET; // collection of indices to the elements of a set
#include <list>
using namespace std;

list<int*> NullSetsList(int** SubsetArray, int SubsetsSize, int NumSensors,  int NumClasses);
void CalcNullsetsCardinal(int** SubsetArray, list<int*> *NullSetList, int *SetNos, int Tot, int NumSensors, int NumClasses, int k, int c);
void CalcKsetsCardinal(int** SubsetArray, list<int*> *KSetList, int *SetNos, int Tot, int NumSensors, int NumClasses, int k, int c, int K);
list<int*> KSetsList(int** SubsetArray, int SubsetsSize, int NumSensors, int NumClasses, int K);
void CalcNullsetsCardinal(int** SubsetArray, list<int**> *NullList, int** Subset, int Tot, int Card, int k, int c);
SUBSET* CalcSubsets(int k, int* SubsetSize, int MaxCard);

//void CalcSubsetsCardinal(list<ARRAY> *SubsetList, ARRAY Subset, int Tot, int Card, int k, int c, int MaxCard);
void AddSubsetsWithPrev(list<SUBSET> *SubsetList,
	SUBSET previousElements, int nPrevElems, int subsetCardinality,
	int setCardinality, int MaxCard);

#ifdef SETSDEMO
main (int argc, char *argv[])
{
	int subsetCount;
	int setCardinality = 3;
	int subCardinalMax = 1;
	int nSensorsMax = 2;
	int kValue = 0;

	if (argc>1)
	{
		sscanf(argv[1], "%d", &setCardinality);

		if (argc>2)
		{
			sscanf(argv[2], "%d", &subCardinalMax);

			if (argc>3)
			{
				sscanf(argv[3], "%d", &nSensorsMax);

				/*if (argc>4)
					sscanf(argv[4], "%d", &kValue);*/
			}
		}
	}
	//else
		//setCardinality = 3;
		//sscanf("3", "%d", &setCardinality);

	ARRAY *subsetArray = CalcSubsets(setCardinality, &subsetCount, subCardinalMax);
	cout << "done that" << endl;

	int i, j;
	cout << "set: {";

	for (i=1; i<setCardinality; i++)
		cout << i << ", ";

	cout << i << "};\n\nsubsetArray: " << subsetCount << "\n{\n\t";

	for (i=0; i<subsetCount-1; i++)
	{
		cout << i << ": {";

		for (j=0; j<subCardinalMax; j++)
			cout << subsetArray[i][j] << ", ";

		cout << subsetArray[i][j] << "},\n\t";
	}{
		cout << i << ": {";

		for (j=0; j<subCardinalMax; j++)
			cout << subsetArray[i][j] << ", ";

		cout << subsetArray[i][j] << "}\n};\n\n";
	}
	list<ARRAY>::iterator it;

        for (int kValue=0; kValue<=setCardinality; kValue++)
        {
        	int intersectionsCount = 0;

	        list<ARRAY> intersectionsList = kValue? KSetsList(subsetArray, subsetCount,
		 nSensorsMax, setCardinality, kValue-1): NullSetsList(subsetArray, subsetCount,
		 nSensorsMax, setCardinality);

   		cout << "intersectionsList[" << kValue << "]:\n{";
		for (it=intersectionsList.begin(); it!=intersectionsList.end(); it++, intersectionsCount++)
		{
			cout << "\n\t";

			for (j=0; j<nSensorsMax-1; j++)
				cout << (*it)[j] << " U ";

			cout << (*it)[j] << ',';
                        delete *it;
		}
		cout << "\b.\n};\nintersectionsCount = " << intersectionsCount << "\n\n";
	}
	return 0;
}
#endif

SUBSET* CalcSubsets(int setCardinality, int* SubsetSize, int MaxCard){

	list<SUBSET> SubsetList;
	list<SUBSET>::iterator ListIt;
	int i;

		// DISABLED TO CATER TO ADDSUBSETSWITHPREV():

        //SUBSET Subset = new int[1];
		//Subset[0] = -1;
		//SubsetList.insert(SubsetList.begin(), Subset);

    for(i=0/*1*/; i<=MaxCard; i++)//{
		AddSubsetsWithPrev(&SubsetList, NULL, 0, i, setCardinality,
		 MaxCard);
        //CalcSubsetsCardinal(&SubsetList, Subset, setCardinality/*k*/, i, 1, 0/*i*/, MaxCard);
    //}

	*SubsetSize = SubsetList.size();
	SUBSET *SubsetArray = new SUBSET[*SubsetSize];
	for(i =0, ListIt = SubsetList.begin(); ListIt!= SubsetList.end(); ListIt++, i++){
		SubsetArray[i] = *ListIt;
	}
	return SubsetArray;
}

/*
	A refined version of old CalcSubsetsCardinal() with fewer parameters.

	Each execution of this function appends all possible subsets, that 
	already contain the elements indexed in 'previousElements', to
	'SubsetList', the master list of subsets.

	Class iteration is done using 'k' in the reverse order.
	Run the DEMO for more understanding.
*/
void AddSubsetsWithPrev(list<SUBSET> *SubsetList, SUBSET previousElements,
 int nPrevElems, int subsetCardinality, int setCardinality, int MaxCard)
{
	SUBSET newPrevElems = new int[MaxCard+1];

	for (int i=0; i<nPrevElems; i++)
		newPrevElems[i] = previousElements[i];

	if (nPrevElems==subsetCardinality) // i.e. if Subset is complete
	{
		newPrevElems[nPrevElems] = -1;
		SubsetList->insert(SubsetList->end(), newPrevElems);
		return; // dont delete newPrevElems as it is to be referenced
	}
	int kLowerBound = nPrevElems? previousElements[nPrevElems-1] + 1: 1;
	int kUpperBound = setCardinality + (nPrevElems-subsetCardinality+1);

	for (int k=kUpperBound; k >= kLowerBound; k--) 
	 // choose each element from the remainder of the set one by one
	{
		newPrevElems[nPrevElems] = k;

		AddSubsetsWithPrev(SubsetList, newPrevElems, nPrevElems+1,
		 subsetCardinality, setCardinality, MaxCard); // fillup the subsets
	}
	delete[] newPrevElems; // as newPrevElems wont be referenced
}

//	for (i=1 to maxcard)								subset		k		i						1						i		maxcard
/*void CalcSubsetsCardinal(list<ARRAY> *SubsetList, ARRAY Subset, int Tot, int subsetCardinality, int searchFromClassNumber, int c, int MaxCard){
	int& Card = subsetCardinality; // just a reference
	int& k = searchFromClassNumber; // just a reference

	SUBSET NewSubset;
	int i;
	if((Tot-k)>=c){
		NewSubset = new int[MaxCard];
		for(i=0;i<(Card - c);i++)
			NewSubset[i] = Subset[i];
		CalcSubsetsCardinal(SubsetList, NewSubset, Tot, Card, k+1, c, MaxCard);
	}
	Subset[Card - c] = k;
	if(c==1){
		if (Card<MaxCard) Subset[Card] = -1; // condition by Anjan
		SubsetList->insert(SubsetList->end(), Subset);
		return;
	}
	CalcSubsetsCardinal(SubsetList, Subset, Tot, Card, k+1, c-1, MaxCard);
}*/

int IsNullIntersection(int** SubsetArray, int* SetNos, int NumSensors, int NumClasses){
	int* Ele = new int[NumClasses];
	int i, j, k, Ret;
	for(i = 0; i < NumClasses; i++)Ele[i] = 1; 
	for(i = 0; i < NumSensors; i++){
		for(j = 0; j < NumClasses; j++){
			if(Ele[j] == 1){
				for(k = 0; SubsetArray[SetNos[i]][k] != -1; k++)
					if((j+1) == SubsetArray[SetNos[i]][k])break;
				if(SubsetArray[SetNos[i]][k]==-1)Ele[j] = 0;
			}
		}
	}
	Ret = 0;
	for(i = 0; i < NumClasses; i++)Ret = Ret||Ele[i]; 
	return (!Ret);
}

int IsKIntersection(int** SubsetArray, int* SetNos,  int NumSensors, int NumClasses, int K){
	int* Ele = new int[NumClasses];
	int i, j, k, Ret;
	for(i = 0; i < NumClasses; i++)Ele[i] = 1; 
	for(i = 0; i < NumSensors; i++){
		for(j = 0; j < NumClasses; j++){
			if(Ele[j] == 1){
				for(k = 0; SubsetArray[SetNos[i]][k] != -1; k++)
					if((j+1) == SubsetArray[SetNos[i]][k])break;
				if(SubsetArray[SetNos[i]][k]==-1)Ele[j] = 0;
			}
		}
	}
	Ret = 0;
	if(!Ele[K])return 0;
	for(i = 0; i < NumClasses; i++)if(i!=K)Ret = Ret||Ele[i];
	return (!Ret);
}
void CalcNullsetsCardinal(int** SubsetArray, list<int*> *NullSetList, int *SetNos, int Tot, int NumSensors, int NumClasses, int k, int c){
	int* NewSetNos;
	int i;
	if((Tot-k)>=c){
		CalcNullsetsCardinal(SubsetArray, NullSetList, SetNos, Tot, NumSensors, NumClasses, k+1, c);
	}
	SetNos[NumSensors - c] = k-1;
	if(c==1){
		NewSetNos = new int[NumSensors];
		for(i=0;i<=(NumSensors - c);i++)NewSetNos[i] = SetNos[i];
		if(IsNullIntersection(SubsetArray, NewSetNos, NumSensors, NumClasses))
			NullSetList->insert(NullSetList->end(), NewSetNos);
		else delete NewSetNos;
		return;
	}
	CalcNullsetsCardinal(SubsetArray, NullSetList, SetNos, Tot, NumSensors, NumClasses, k+1, c-1);
}


list<int*> NullSetsList(int** SubsetArray, int SubsetsSize, int NumSensors, int NumClasses){
	int* SetNos = new int[NumSensors];
	list<int*> NullSetList;
	CalcNullsetsCardinal(SubsetArray, &NullSetList, SetNos, SubsetsSize, NumSensors, NumClasses, 1, NumSensors);
	return NullSetList;
}

void CalcKsetsCardinal(int** SubsetArray, list<int*> *KSetList, int *SetNos, int Tot, int NumSensors, int NumClasses, int k, int c, int K){
	int* NewSetNos;
	int i;
	if((Tot-k)>=c){
		CalcKsetsCardinal(SubsetArray, KSetList, SetNos, Tot, NumSensors, NumClasses, k+1, c, K);
	}
	SetNos[NumSensors - c] = k-1;
	if(c==1){
		NewSetNos = new int[NumSensors];
		for(i=0;i<=(NumSensors - c);i++)NewSetNos[i] = SetNos[i];
		if(IsKIntersection(SubsetArray, NewSetNos, NumSensors, NumClasses, K))
			KSetList->insert(KSetList->end(), NewSetNos);
		else delete NewSetNos;
		return;
	}
	CalcKsetsCardinal(SubsetArray, KSetList, SetNos, Tot, NumSensors, NumClasses, k+1, c-1, K);
}


list<int*> KSetsList(int** SubsetArray, int SubsetsSize, int NumSensors, int NumClasses, int K){
	int* SetNos = new int[NumSensors];
	list<int*> NullSetList;
	CalcKsetsCardinal(SubsetArray, &NullSetList, SetNos, SubsetsSize, NumSensors, NumClasses, 1, NumSensors, K);
	return NullSetList;
}

