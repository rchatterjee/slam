#include <list>
using namespace std;
typedef int itype;
class CMerge{
public:
	list<itype>*MergeInf;
	int *KeyLookup;
	int Num;
	void Initialize(int size);
	BOOL Merge_j_to_i(itype i,itype j);
	void MakeKeyLookup();
};

void CMerge::Initialize(int size){
	MergeInf = new list<itype>[size];
	KeyLookup = new itype[size];
	for(int i=0;i<size;i++){
		MergeInf[i].insert(MergeInf[i].begin(),i);
	}
	Num=size;
}
BOOL CMerge::Merge_j_to_i(itype i,itype j){
	if((i>=Num)||(j>=Num))return false;
	MergeInf[i].merge(MergeInf[j]);
	return true;
}
void CMerge::MakeKeyLookup(){
	list<itype>::iterator p;
	for(int i=0;i<Num;i++){
		for(p = MergeInf[i].begin();p!=MergeInf[i].end();p++){
			KeyLookup[*p]=i;
		}
	}
}
