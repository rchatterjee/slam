//	CNode.cpp Version fusion3.9 July 2003.
//
//	uses matrixzero.cpp for matrix zeroing methods.
//	(fusion3.9) added protected member _actualThis to refer to the actual
//	 address of 'this' object as opposed to the address of the virtual base class.
//	It is to be referenced by virtual derived classes like DSNode & CenPixNode.
#define CNodeCpp

//	uncomment following to make Vij the key direction of optimization instead of Vi
//#define KEYISVijNOTVi

#include "matrixzero.cpp"

#include <map>
//#include "FTest.cpp"
#include "AdjRBTree.cpp"
#include "Globals.h"
using namespace std;
#define NEG -100000000
#define SAFENEG -90000000
#define PSZERO -70000000
#define SAFEZERO -60000000
typedef int itype;
typedef double vtype;

class CNode{
private:
	BOOL GetBijAndWij(CNode * pTwo, double * pdWij,  double *B_matrix, double *W_matrix, int NumBands);
	int CombinedMeanVar(CNode* pOther,double  *mu, double **ssq, int NumBands);

protected:
        virtual BOOL F_Test(CNode * pTwo, int accr, double *Bij, double *Wij,int NumBands);

public:
	void const* _actualThis; // as opposed to virtual 'this' in case of virtual inheritance.
	int NumPixels,NumEdges,NumMergable,NumBands;
	vtype V;
	double *Mean,**SigmaSquare;
	AdjRBTree AdjMap;
	itype Index;

	void InitializeSiMi();
	void AddEdge(CNode *Node,int NumBands);
	void AddEdge_i_to_j(vtype Vij,CNode *Node);
	void DeleteEdge_i_to_j(vtype Vij,CNode *Node);

	virtual void MergeNode(CNode *Node_j,CNode **Graph, int NumBands);

	AdjRBTree* GetAdjMap(){return &AdjMap;}
	void AddPixel(int *pixel,int NumBands);
	void InsertInList(AdjTreeNode *Node,list<itype> *MakeUnique, AdjTreeNode *NIL);
	vtype Calc_Vij(CNode *Node,int NumBands);
	itype MaxContributor(vtype* =NULL);
	virtual ~CNode();
	CNode(void const* =NULL);
	void Initialize(int NumBands);
};
CNode::CNode(void const* actualThis):
 _actualThis(actualThis==NULL? this: actualThis)
{
	Mean = NULL;
	SigmaSquare = NULL;
	NumBands = 0;
}
void CNode::Initialize(int NumBands1){
	NumBands = NumBands1;
	Mean = new double[NumBands];
	SigmaSquare = new double*[NumBands];
	for(int i=0;i<NumBands;i++)
		SigmaSquare[i] = new double[NumBands];
}
CNode::~CNode(){
	delete[] Mean;
	if(!SigmaSquare)return;
	for(int i=0;i<NumBands;i++)
		if(SigmaSquare[i])delete[] SigmaSquare[i];
	delete[] SigmaSquare;
}
void CNode::AddPixel(int *pixel,int NumBands){
	int i,j;
	if(NumPixels < 1)
	{
		for(i=0;i<NumBands;i++){
			Mean[i] = pixel[i];
			for(j=0;j<NumBands;j++)SigmaSquare[i][j] = 0;
		}
		NumPixels++;
		return;
	}
	double	dF, dIta;

	for(i=0;i<NumBands;i++)
	{
		dMuX[i] = Mean[i];
		for(j=0;j<NumBands;j++)
			dSqX[i][j] = SigmaSquare[i][j];
	}
	for(i=0;i<NumBands;i++)
		dX[i] = pixel[i];

	dF = (double)NumPixels/((double)NumPixels + 1.0);
	dIta = (NumPixels < 32)? 1.0 : 0.0;

	for(i=0;i<NumBands;i++)
		for(j=0;j<NumBands;j++)
			dSqX[i][j] = ((((double)NumPixels - dIta)*dSqX[i][j]) / ((double)NumPixels + 1 - dIta)) + (((dF*(dX[i] - dMuX[i])*(dX[j] - dMuX[j]))) / ((double)NumPixels + 1 - dIta));
	for(i=0;i<NumBands;i++)
	{
		dMuX[i] = dF*(dMuX[i] - dX[i]) + dX[i];
		Mean[i] = dMuX[i];
	}
	for(i=0;i<NumBands;i++)
		for(j=0;j<NumBands;j++)
			SigmaSquare[i][j] = dSqX[i][j];

	NumPixels++;
	return;
}

itype CNode::MaxContributor(vtype* contribution){
	AdjTreeNode* Max;
	Max = AdjMap.RBTreeMaximum();
	if(!Max)return -1;

	if(contribution)
		*contribution = Max->Key;

	if(Max->Key == NEG)return -1;
	return Max->Value;
}


void CNode::AddEdge(CNode *Node, int NumBands){
	if(!Node){cout<<"\n Error9";return;}
	vtype Vij=Calc_Vij(Node,NumBands);
	if(Vij==0){
		int yu;
		yu=0;
		Vij=Vij;
	}

	if(AdjMap.FindNode(Vij, Node->Index))return;
	AdjMap.Insert(Vij, Node->Index);
	NumEdges++;
	if(Vij!=NEG){
		if(V == NEG)V = 0;
		V += Vij;
		NumMergable++;
	}
#ifdef KEYISVijNOTVi
	MaxContributor(&V);
#endif
}

void CNode::AddEdge_i_to_j(vtype Vij,CNode *Node){
	if(!Node){cout<<"\n Error9";return;}
	AdjMap.Insert(Vij, Node->Index);
	NumEdges++;
	if(Vij!=NEG){
		if(V == NEG)V = 0;
		V += Vij;
		NumMergable++;
	}
#ifdef KEYISVijNOTVi
	MaxContributor(&V);
#endif
}

void CNode::DeleteEdge_i_to_j(vtype Vij,CNode *Node){

	itype index = Node->Index;
	AdjTreeNode *Del;
	if(!(Del = AdjMap.FindNode(Vij, Node->Index))){
		printf("\nDeletion Err14\n");
		return;
	}
	AdjMap.Delete(Del);
	NumEdges--;
	if(Vij!=NEG){
		if(V == NEG){
			cout<<"\n Error14";
		}
		V -= Vij;
		NumMergable--;
		if(NumMergable==0)V = NEG;
	}
#ifdef KEYISVijNOTVi
	MaxContributor(&V);
#endif
}

vtype CNode::Calc_Vij(CNode *Node, int NumBands){
	double temp=0;
	int i;
	if(!Node){cout<<"\n Error10";return 0;}
	if(!F_Test(Node,4, SBij, SWij, NumBands))return NEG;

	int n1=NumPixels, n2=Node->NumPixels;
	for(i=0; i<NumBands; i++)
		temp+= SigmaSquare[i][i] + Node->SigmaSquare[i][i] - SWij[i] ;//- (Bij[i]/(double)(n1+n2));

	return(temp);
}



void CNode::InsertInList(AdjTreeNode *Node,list<itype> *MakeUnique, AdjTreeNode *NIL){
	if(Node == NIL)return;
	InsertInList(Node->Right, MakeUnique, NIL);
	InsertInList(Node->Left, MakeUnique, NIL);
	MakeUnique->insert(MakeUnique->begin(),Node->Value);
}

void CNode::MergeNode(CNode *Node_j,CNode **GraphArr, int NumBands){

	list<itype>MakeUnique,MakeUnique_j;
	int i,j;

	AdjRBTree *AdjMap_j = Node_j->GetAdjMap();
	typedef list<itype>::iterator LI;

	// CENTREPIXELS BEING RECALCULATED IN CGRAPH::MERGENODE()
	/*if(NumPixels<Node_j->NumPixels){
		CentreX = Node_j->CentreX;
		CentreY = Node_j->CentreY;
	}*/


	CombinedMeanVar(Node_j, mu, ssq, NumBands);
	for(i=0;i<NumBands;i++){
		Mean[i] = mu[i];
		for(j=0;j<NumBands;j++)SigmaSquare[i][j] = ssq[i][j];
	}
	NumPixels = NumPixels + Node_j->NumPixels;
	InsertInList(AdjMap.Root, &MakeUnique, AdjMap.NIL);
	MakeUnique.remove(Node_j->Index);
	InsertInList(AdjMap_j->Root, &MakeUnique_j, AdjMap_j->NIL);
	MakeUnique_j.remove(Index);



	MakeUnique.merge(MakeUnique_j);
	MakeUnique.sort();
	MakeUnique.unique();
	NumEdges = MakeUnique.size();
	AdjMap.DeleteRecur(AdjMap.Root);
	AdjMap.Root = AdjMap.NIL;

	vtype Vij = 0; int index_k=0;
	V = NEG;
	NumMergable = 0;
	for(LI k = MakeUnique.begin(); k!=MakeUnique.end(); k++){
		index_k = *k;
		Vij = Calc_Vij(GraphArr[index_k],NumBands);
		AdjMap.Insert(Vij, index_k);
		if(Vij != NEG){
			if(V == NEG)V = 0;
			V += Vij;
			NumMergable++;
		}
	}

#ifdef KEYISVijNOTVi
	MaxContributor(&V);
#endif

}







/*#ifndef MATRIXZEROINGMETHODS

#	define MATRIXZEROINGMETHODS

int zerorow(double **a,double **b,int p, int k, int NumBands){
  int i;
  if(a[p][p] == 0.0)
  {
//	  printf("Divide by zero \n");
	  return (0);
  }
  double t=a[k][p]/a[p][p];
  for(i=0;i<NumBands;i++)
   {
    a[k][i]-=t*a[p][i];
    b[k][i]-=t*b[p][i];
   }
  return(1);
}

int zerocol(double **a,double **b,int p, int NumBands){
  int i;
  for(i=0;i<NumBands;i++)
   {
    if(i!=p)
	{
       if(!zerorow(a,b,p,i,NumBands))
	   return (0);
	}
   }
  return (1);
 }

#endif*/







double FindInverse(double **a, double **b, int NumBands){
  int i,j;
  double det1=1.0;

  if(NumBands == 1)
  {
  	if(a[0][0] != 0.0)
 	 	b[0][0] = 1.0/a[0][0];
  	return(a[0][0]);
  }
  for(i=0;i<NumBands;i++)
  {
     if(!zerocol(a,b,i, NumBands))
		 return 0.0;
  }

  for(i=0;i<NumBands;i++)
    det1*=a[i][i];

  for(i=0;i<NumBands;i++)
   {
    for(j=0;j<NumBands;j++)
     b[i][j]/=a[i][i];
    a[i][i]=1;
   }
  return(det1);
}

BOOL CNode::GetBijAndWij(CNode * pTwo, double * pdWij,  double *B_matrix, double *W_matrix, int NumBands)
{
	int	n1, n2,i,j;
	double	*m1, *m2, **s1, **s2;
	double	dF, dW, dB, det;

	n1 = NumPixels;
	m1 = Mean;
	s1 = SigmaSquare;

	n2 = pTwo->NumPixels;
	m2 = pTwo->Mean;
	s2 = pTwo->SigmaSquare;
	for(i=0;i<NumBands;i++)
	{
		dTemp[i] = 0.0;
		dM[i] = m1[i] - m2[i];
	}

	double dFactor = ( (double)n1 + (double)n2 - 2.0);

		if(n1 + n2 < 3)
			dFactor = 1;


	for(i=0;i<NumBands;i++) // Calculating S_pooled
	{
		for(j=0;j<NumBands;j++)
		{
		  if (i!=j) inv_dS[i][j] = 0.0;
			else	inv_dS[i][j] = 1.0;
		  dS[i][j] = ((((double)n1-1.0) * s1[i][j]) + (((double)n2-1.0) * s2[i][j])) / dFactor;
		}
		W_matrix[i] = dS[i][i];
	}


	det = FindInverse(dS, inv_dS, NumBands);

	if(det == 0.0)
	{
//		cout << "Error finding inverse " << endl;
		return FALSE;
	}

	dB = 1.0/( (double)n1 + (double)n2 );
	dB *= ( (double)n1 * (double)n2 );

	for(i=0; i<NumBands; i++)
		B_matrix[i] = dB*(dM[i]*dM[i]);
	

	if(pdWij)
	{
		for(i=0;i<NumBands;i++)
			for(j=0;j<NumBands;j++)
				dTemp[i]+=dM[j]*inv_dS[j][i];
		dW=0.0;
		for(i=0;i<NumBands;i++)
			dW+=dTemp[i]*dM[i];
		dW*=dB;
		*pdWij = dW;
	}
	
	return TRUE;
}
/*
BOOL CNode::F_Test(CNode * pTwo, int accr, double *Bij, double *Wij,int NumBands)
{

	double	dF,pWij,dFactor;
	if(!(GetBijAndWij(pTwo, &pWij, Bij, Wij, NumBands)))  return FALSE;
	dF = GetF_Value(accr, NumPixels + pTwo->NumPixels - NumBands - 1, NumBands);
	if(fabs(pWij) < 1.0e-99)
		return FALSE;
	int n1 = NumPixels;
	int n2 = pTwo->NumPixels;
	if((n1+n2)>(NumBands+1))dFactor = ((double)NumBands*((double)n1+(double)n2-2.0))/((double)n1+(double)n2-NumBands-1.0);
	else return FALSE;


	if(pWij < (dF*dFactor))
		return TRUE;
	return FALSE;
}*/


BOOL CNode::F_Test(CNode * pTwo, int accr, double *Bij, double *Wij,int NumBands)
{
	//double	Bij, Wij;
	double	dF,pWij,dFactor;
	if(!(GetBijAndWij(pTwo, &pWij, Bij, Wij, NumBands)))  return FALSE;
	dF = GetF_Value(accr, NumPixels + pTwo->NumPixels - NumBands - 1, NumBands);
	if(fabs(pWij) < 1.0e-99)
		return FALSE;
	int n1 = NumPixels;
	int n2 = pTwo->NumPixels;
	if((n1+n2)>(NumBands+1))dFactor = ((double)NumBands*((double)n1+(double)n2-2.0))/((double)n1+(double)n2-NumBands-1.0);
	else return FALSE;
//	printf("Wij=%f\tdF=%f\n",Wij,(dF*dFactor));

	BOOL testcondition = (pWij < (dF*dFactor));
	return testcondition? TRUE: FALSE;
}


int CNode::CombinedMeanVar(CNode* pOther,double  *mu, double **ssq, int NumBands){
	double *m1=Mean;
	double **ssq1=SigmaSquare;
	int n1 = NumPixels;
	double *m2 = pOther->Mean;
	double **ssq2 = pOther->SigmaSquare;
	int n2 = pOther->NumPixels;
	double	dF;
	double	dEta = 0.0;
	int		i,j;

	if( (n1 + n2) < 1)
		return 0;
	if( (n1 < 1) || ( n2 < 1))
	{
		for(i=0;i<NumBands;i++)
		{
			mu[i] = (m1[i] + m2[i])/(n1 + n2);
			for(j=0;j<NumBands;j++)
				ssq[i][j] = (ssq1[i][j] + ssq2[i][j])/(n1 + n2);
		}
		return (n1+n2);
	}

	if( (n1 + n2) < 32 )
		dEta = 1.0;
	dF = 1.0/((double)n1 + (double)n2 - dEta);
	for(i=0;i<NumBands;i++)
		mu[i] = ((double)n1*dF)*m1[i] + ((double)n2*dF)*m2[i];

	for(i=0;i<NumBands;i++)
	{
		for(j=0;j<NumBands;j++)
		{
			if( (n1 + n2) < 32 )
				dEta = 1.0;
			else
				dEta = 0.0;
			dF = 1.0/((double)n1 + (double)n2 - dEta);
			dEta = 0.0;
			if(n1 < 32)
				dEta = 1.0;
			ssq[i][j] = (((double)n1-dEta)*dF)*ssq1[i][j];
			dEta = 0.0;
			if(n2 < 32)
				dEta = 1.0;
			ssq[i][j]+= (((double)n2-dEta)*dF)*ssq2[i][j];
			dF *= ( 1.0 / ( (double)n1 + (double)n2 ) );
			dF *= (double)((double)n1*(double)n2);
			ssq[i][j] += ((m1[i]-m2[i])*(m1[j]-m2[j])*dF);
		}
	}

	return (n1+n2);
}
