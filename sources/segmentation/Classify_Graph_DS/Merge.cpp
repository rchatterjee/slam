//	merge.cpp Version fusion3.6 July 2003 taken from derived from fusion2.0
//
//	(version 1.0) reads two classification labelled images and writes
//	a new image formed by intersecting the regions of the two images
//	and declaring each nonempty intersection as a separate region.
//
//	DisplayUsage() corrected and made more user-friendly.
//	Image scan direction made compatible with other modules.
//	Labelling made more compact (no more multiples of 15).
//
//	option kept for using specified multiples in merge.asc
//
#define MergeCpp

//following is the number, multiples of which are in merge.asc
#define MERGEMAINLABELSTEP 1

#ifndef MAIN
#	define MAIN
#	define mergemain main
#endif

#include <iostream>
#include <stdlib.h>		// done simply to avoid compiler warning
#include "Image.h"
#include "ScanFill.h"

#include "MultiBandScanFill.cpp"

#ifdef mergemain
#	include "Image.cpp"
#	include "ScanFill.cpp"
#endif

int DisplayUsage(const char* module, int nRetVal = -1)
{
	//cout << "Usage : " << module << " ";
	//cout << "<no of images> <width> <height> <img1> <type: 0=binary 1=text> ..." << endl;
	//cout << "Output is stored in : merge.asc, merge.bin" << endl;
	return nRetVal;
}

int mergemain(int argc, char *argv[])
{
	if(argc < 6)
		return DisplayUsage(argv[0]);// no use of this code (?)

	int nBands = 0;

	sscanf(argv[1], "%d", &nBands); // nBands is 1 (probably the no. of segmented files)

	if(argc < (4 + 2*nBands))
		return DisplayUsage(argv[0]); // no use of this code (?)

	//cout << "No of images to merge : " << nBands << endl;

	int nWidth, nHeight;

	sscanf(argv[2], "%d", &nWidth);
	sscanf(argv[3], "%d", &nHeight);
	//cout << "Width : " << nWidth << " Height : " << nHeight << endl;

	typedef vector<CImage> ImageVector;

	ImageVector veBands;// veBands has the vector of segmented files represented CImage classes

	for(int n = 0; n < nBands; ++n)
	{
		CImage::FileType ft;
		sscanf(argv[5 + 2*n], "%d", (int*)&ft); // ft takes the filetype of segmented files
		//cout << "Band No. " << n + 1 << " : " << argv[4 + 2*n] << " Type : " << ft << endl;
		// prints the filename of the segmented file
		veBands.push_back(CImage(argv[4 + 2*n], nWidth, nHeight, ft));
	}
	//printf("before imgout\n");
	CImage imgOut(nWidth, nHeight);// imgOut is the output image.
	//printf("after imgout\n");
	CMultiBandScanFill merge;
	//printf("before attachoutputimage\n");
	merge.AttachOutputImage(&imgOut); // it is unclear what is finally being done
	//printf("after attachoutputimage\n");
	ImageVector::iterator it = veBands.begin();
	//printf("after veBands.begin()\n");
	ImageVector::const_iterator itEnd = veBands.end();
	//printf("after veBands.end()\n");
	for(; it != itEnd; ++it)
		merge.AddBandImage(&(*it));// this is making a vector of segmented files in the class merge.
  //printf("After loop\n");                                  // the vector is called m_vebands.
	int nRegion = 0;
	int nPixel = MERGEMAINLABELSTEP;

	int nY;

	for(nY = 0; nY < nHeight; ++nY)
	{
		for(int nX = 0; nX < nWidth; ++nX)
		{
			if(imgOut.GetPixel(nX, nY)!=0)
				continue;
			
			merge.ScanFill(nX, nY, nPixel, 0);
		
			nPixel += MERGEMAINLABELSTEP;
			
			++nRegion;
		}
	}
	//cout << "Region Count : " << nRegion << endl;

	int nSingle = 0;

	for( nY = 0; nY < nHeight; nY++)
		for (int nX = 0; nX < nWidth; nX++)
			if (isSinglePixel(nX, nY, &imgOut))
				nSingle++;

	//cout << "Single Pixel count: " << nSingle << endl;

	imgOut.SaveImageAs("merge.bin");
	imgOut.SaveImageAs("merge.asc", CImage::ftText);
	return 0;
}
