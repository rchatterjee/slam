// ScanFill.cpp: implementation of the CScanFill class.
//	version 2.0 July 2003
//	CScanFill::ScanFill() rewritten to exhibit simplicity
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "ScanFill.h"
#include "Image.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CScanFill::CScanFill()
{
	X = 0;
	Y = 0;
	m_pImg = NULL;
}

CScanFill::~CScanFill()
{
//	delete m_pImg;
}

void CScanFill::Push(int x, int y)
{
	m_aX.Push(x);
	m_aY.Push(y);
}

int CScanFill::Pop(int &x, int &y)
{
	m_aX.Pop(x);
	return m_aY.Pop(y);
}

int CScanFill::IsStackEmpty()const
{
	return m_aX.IsEmpty();
}

void CScanFill::AttachImage(CImage *pImg)
{
	m_pImg = pImg;
	if(m_pImg)
	{
		X = m_pImg->GetImageWidth();
		Y = m_pImg->GetImageHeight();
	}
}

void CScanFill::SetPixel(int x, int y, int nPixel)
{
	m_pImg->SetPixel(x, y, nPixel);
}

int  CScanFill::GetPixel(int x, int y)const
{
	return m_pImg->GetPixel(x, y);
}

int CScanFill::TestPixel(int x, int y, int nFillColor, int nOldColor)
{
	if(GetPixel(x, y) == nOldColor)
		return 1;
	return 0;
}

#	ifdef STACKEDSCANFILL

BOOL CScanFill::SetNextSeed(int x, int y, int xr, int nFillColor, int nOldColor)
{
	if((y < 0) || (y > (Y - 1)))
		return FALSE;

	while((x <= xr) && (TestPixel(x, y, nFillColor, nOldColor) < 1))
		x++;

	while(x <= xr)
	{
		while((x <= xr) && (TestPixel(x, y, nFillColor, nOldColor) > 0))
			x++;
		if(TestPixel(x - 1, y, nFillColor, nOldColor) > 0)
			Push(x - 1, y);
		x++;
		while((x <= xr) && (TestPixel(x, y, nFillColor, nOldColor) < 1))
			x++;
	}
	return TRUE;
}

int CScanFill::ScanFill(int nPosX, int nPosY, int nFillColor, int nOldColor)
{
	//printf("hi0\n");
	int	x, xl, xr, sx;
	int	y, sy;
	int	cnt=0;
	//printf("hi1\n");

	Push(nPosX, nPosY);
	while(!IsStackEmpty())
	{
		//printf("hi2\n");
		Pop(x, y);
		//printf("hi3\n");
		SetPixel(x, y, nFillColor);
		cnt++;

		sx = x;
		x --;
		if(x < 0)
		{
			if(IsStackEmpty())
				x = 0;
			else
				continue;
		}
		while((x >= 0) && (TestPixel(x, y, nFillColor, nOldColor) > 0))
		{
			SetPixel(x, y, nFillColor);
			cnt++;
			x--;
		}
		xl = (x > 0) ? x : 0;

		x = sx + 1;
		if(x > (X - 1))
		{
			if(IsStackEmpty())
				x = X - 1;
			else
				continue;
		}
		while((x < X) && (TestPixel(x, y, nFillColor, nOldColor) > 0))
		{
			SetPixel(x, y, nFillColor);
			cnt++;
			x++;
		}

		xr = (x < X - 1) ? (x) : (X - 1);
		sy = y;

		SetNextSeed(xl, sy + 1, xr, nFillColor, nOldColor);
		SetNextSeed(xl, sy - 1, xr, nFillColor, nOldColor);
	}
	return cnt;
}

#	else

const int secondOrderNbd[][2] = {{1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0},
 {-1, -1}, {0, -1}, {1, -1}};

const int secondOrderNbdSize = 8;

int CScanFill::ScanFill(int x, int y, int fillColor, int oldColor, int filledSoFar)
{

	if (x < 0 || x >= X || y < 0 || y >= Y) // pixel is out of range
		return filledSoFar;
	

	if (!TestPixel(x, y, fillColor, oldColor)) // pixel is in a wrong region
		return filledSoFar;


	SetPixel(x, y, fillColor);	// color this pixel
	filledSoFar++;

	//printf("visited (%d, %d). %d replaced with %d.\n", x, y, oldColor, fillColor);

	// attempt to color the neighbours
	for (int i=0; i<secondOrderNbdSize; i++)
		filledSoFar = ScanFill(x + secondOrderNbd[i][0],
		 y + secondOrderNbd[i][1], fillColor, oldColor, filledSoFar);

	return filledSoFar;
}

#	endif

int CRAGScanFill::Segment()
{
	int nPixel, x, y, nX, nY;

	nX = (int)GetImage()->GetImageWidth();
	nY = (int)GetImage()->GetImageHeight();

/*	m_nPixel = GetImage()->GetPixel(0, 0);
	for(y = 0; y < nY; y++)
		for(x = 0; x < nX; x++)
			if(m_nPixel < GetImage()->GetPixel(x, y))
				m_nPixel = GetImage()->GetPixel(x, y);

	/*m_nPixel =m_nPixel ;
	if(m_nPixel > 256)
		return 0;/

	m_nPixel += 15;*/
	m_nPixel = 1;
	for(y = 0; y < nY; y++)
	{
		for(x = 0; x < nX; x++)
		{
			if(m_pOut->GetPixel(x, y) > 0)
				continue;
			nPixel = GetImage()->GetPixel(x, y);
			ScanFill(x, y, m_nPixel, nPixel);
			m_nPixel += 1;
			
		}
	}
	m_pOut->SaveImageAs("temp.asc", CImage::ftText);
	return m_nPixel;
}

