//	mass.cpp Version fusion4.02
//
//	**** includes LOOK-UP TABLE facility as a replacement for GT for DS 
//       classification ****** 
//
//	functions for Dempster-Shafer classification of image pixels
//	with classes obtained from the ground truth file and
//	class conditional probabilities assumed Gaussian.
//
//	originally designed specially for centre pixel classification
//	now modified to classify all pixels.
//
//	saves pixel labels in 'DSLabels.asc'.
//
//	WARNING: det[] gets overwritten with multiple multiband sensors,
//	so it cannot be used when there are more than one multiband sensor.
//
//	DOES NOT use matrixzero.cpp for matrix zeroing methods.
//
//	Center Pixel checking removed because center pixels are not fixed
//	once and for all but change with merging of regions on the RAG.
//
//	(version fusion3.8) facility for calculating a mass for each region.
//
//	(version fusion4.01) facility for weighing sensors using lambdas. NOT TESTED.
//
#define massCpp

//#define NORMALIZEMASSES
//#define NORMALIZEFUSION

//#define REGIONMASSCALC

#define DSFILE "DSLabels.asc"

#ifdef REGIONMASSCALC
#	define NORMALIZEMASSES
#	define NORMALIZEFUSION
#endif

//#include "matrixzero.cpp"

//#define MASSCPPVERBOSE // for debugging purposes

#define NUM 4		// 'maximum possible' bands for a sensor, bad progg
#define CLASSMAX 32	// 'maximum possible' classes, bad progg
#include "Globals.h"
#include<list>
#include"Sets.cpp"
#include"Image.h"
#include<math.h>
#include <vector>
#include <string>
#define E 2.7182
using namespace std;
typedef double Mean[NUM][CLASSMAX];
typedef double Incov[CLASSMAX][NUM][NUM];


char file[NUM][130], fng[130];
double cov[CLASSMAX][NUM][NUM];
Mean *mean;
Incov *incov;
double det[30];
int MAXCLASS, MAXSENSORS = 4, nr, nc, ncl;
//CImage SegImg; uncomment if groundtruth file is provided.
double Lowest = 1;
double Lowest_1 = 1;
double Lowest_2 = 1;



void zerorow(double a[][NUM],double b[][NUM],int p,int k, int num_bands);
void zerocol(double a[][NUM],double b[][NUM],int p, int num_bands);
void CompIntensity(CImage*);
void Lookup(int num_bands, int sensor, int blue, int green, int red, int ir);
void Initialize(int num_bands, int sensor, CImage *img);
int PixelClass(int NumSensors, int *num_bands, int **img,int xpix,int ypix, float **lambda);
int PixelClass1(int NumSensors, int *num_bands, int **img, float **lambda);
void CalcPerms(int **Perms, int Tot);
double mult(double a[],double b[][NUM], int num_bands);
double inver(double a[][NUM], double b[][NUM], int num_bands);


int* initLabel(int NumSensors, int *num_bands, int **img, int xpix, int ypix, float **lambda);

list<int*> *KSetList;
list<int*> NullSetList;
int Factorial = 1;
int SubsetSize;
int** SubsetArray;
int SubsetArray1[3000][3000];
int** Perms;

double **F;
double *Mass;
double **InitialMass;
int countCommon=0;


#include"createindex.c"
#ifdef REGIONMASSCALC

int **regionLabels;


void initRegionMasses(int nReg, int nClasses)
{
  int i;
  vector<float> v;
  //cout<<"NO OF REGIONS "<<nReg<<endl;
  for (i=0; i<nClasses; i++)
    v.push_back(0.0);
  
  for (i=0; i<nReg; i++)
    betPFunction.push_back(v);
  //printf("initRegionMasses() done.\n");
  fflush(stdout);
}

void fixRegionMasses(int nReg, vector<int>& numPixels, int nClasses)
{
  FILE *fpt;
  fpt = fopen("betPFunction.txt","w");
  for (int i=0; i<nReg; i++)
    {
      for (int j=0; j<nClasses; j++)
	{
	  //cout<<""<<i<<"***"<<numPixels[i]<<endl;
	  betPFunction[i][j] /= numPixels[i];
	  fprintf(fpt,"%f ",betPFunction[i][j]);
	}
      fprintf(fpt,"\n");
    }
  fclose(fpt);
}

#endif

void ClassifyPixels(int num_class, CImage **img, int NumSensors, int *num_bands, char GroundTruth[], int MaxCard, const string& lambdaFile, int blue, int green, int red, int ir, int flag =0){
  MAXCLASS = num_class;
  FILE *fpt;
  int ***index=createindex(num_class);
  int *coef=createmultiplier(num_class);
  
  fpt = fopen( DSFILE,"w" );
  
  KSetList = new list<int*>[MAXCLASS];
  mean = new Mean[NumSensors];
  incov = new Incov[NumSensors];
  MAXSENSORS = NumSensors;
  ncl = num_class;
  int i, k, j, l; // K;
  int **pixel_img  = new int*[MAXSENSORS];
  for(i=0; i < MAXSENSORS; i++)pixel_img[i] = new int[num_bands[i]];
  nr = img[0][0].GetImageHeight();
  nc = img[0][0].GetImageWidth();

#ifdef REGIONMASSCALC
  FILE* f = fopen("merge.asc", "r");
  regionLabels = new int*[nr];
  
  for (i=0; i<nr; i++)
    {
      regionLabels[i] = new int[nc];
      
      for (j=0; j<nc; j++)
	  {
	  fscanf(f, "%d", &regionLabels[i][j]);
	  regionLabels[i][j]--;
	  }
    }
  fclose(f);
#endif
  
  //SegImg.CImagex(GroundTruth, nc, nr, CImage::ftBinary);		uncomment if groundtruth file is provided.
  SubsetArray = CalcSubsets(num_class, &SubsetSize, MaxCard);
  
#ifdef MASSCPPVERBOSE
  //printf("Done calculating subsets.\n");
  fflush(stdout);
#endif
  
  double Prod = 1;
  NullSetList = NullSetsList(SubsetArray, SubsetSize, MAXSENSORS, MAXCLASS);
  for(k = 0; k < MAXCLASS; k++){
    KSetList[k] = KSetsList(SubsetArray, SubsetSize, MAXSENSORS, MAXCLASS, k);
  }
#ifdef MASSCPPVERBOSE
  //printf("Done calculating intersections.\n");
  fflush(stdout);
#endif
  
  for(i = 1; i<=MAXSENSORS; i++)Factorial *= i;
  Perms = new int*[Factorial];
  CalcPerms(Perms, MAXSENSORS);
  for(i=0; i<NumSensors; i++)
    {
      //if(i==0) CompIntensity(img[i]);			uncomment if groundtruth file is provided.
      //Initialize(num_bands[i], i, img[i]);		uncomment if groundtruth file is provided.
      Lookup(num_bands[i], i, blue, green, red, ir);
    }
    
  F = new double*[NumSensors];
  Mass = new double[MAXCLASS];
  for(i=0; i<NumSensors; i++)
    {
      F[i] = new double[ncl+1];
      for(int aa=0;aa<=ncl;aa++)
	F[i][aa]=0.00;
      //if(F[i][aa]==0.00) cout << "OK" << endl;
    }
  //exit(1);
  
  InitialMass = new double*[NumSensors];
  for(i=0; i<NumSensors; i++)InitialMass[i] = new double[SubsetSize];
  printf("\nPerforming DS pixel classification");
  fflush(stdout);
  
  // GETTING LAMBDA VALUES
  
  float **lambda = new float*[MAXCLASS];
  for(i=0; i<MAXCLASS; i++)
    lambda[i] = new float[NumSensors];
	
  FILE *f = fopen(lambdaFile.c_str(), "r");
  for(i=0; i<MAXCLASS; i++)
    for(j=0; j<NumSensors; j++)
      {
	if(f!=NULL)
	  fscanf(f, "%f", &lambda[i][j]);
	else
	  lambda[i][j] = 1;
      }
  
  // GOT LAMBDA VALUES

  if(flag==1){
    
    //cout<<"Flag = 1";
    //This is the function which manipulates the creation of the subsets through the Kneser's graph
    // kneser1234(num_class);
    for(i =0; i<nr; i++){
      if(i==i)
	{
	  printf(".");fflush(stdout);
	}
      for(j=0; j<nc; j++)
	{
	  
	  /*
	    if(!CentrePixel[i][j]) continue;
	  */
	  for(k=0; k<NumSensors; k++)
	    for(l=0; l<num_bands[k]; l++)
	      pixel_img[k][l] = img[k][l][i][j];
	  // in case of the Pixel Class now the SubsetArray reads from the Knesers graph
	  PixelLabel[i][j] = PixelClass1(NumSensors, num_bands, pixel_img, lambda);
	  
	  fprintf(fpt,"%d ",PixelLabel[i][j]);
	}
      fprintf(fpt,"\n");
    }
  }
  else
    {
      
      for(i=0; i<nr; i++)
	{
	  if(i%5==0)
	    {
	      printf("~");fflush(stdout);
	    }
	  for(j=0; j<nc; j++)
	    {
	      
	      //		   if(!CentrePixel[i][j]) continue;
	      
	      for(k=0; k<NumSensors; k++)
		for(l=0; l<num_bands[k]; l++)
		  pixel_img[k][l] = img[k][l][i][j];
	      // in this case the subset array reads from the brute force method
	      int *ar=initLabel(NumSensors, num_bands, pixel_img, j, i, lambda);
	      
	      if(ar[0]==ar[1])
		{
		  
		  PixelLabel[i][j] = ar[0];
		  //cout<<"Common Pixel : "<<++countCommon<<endl;
		  //			countCommon++;
		}
	      else   
		{
		 	PixelLabel[i][j] =PixelClass(NumSensors, num_bands, pixel_img, j, i, lambda,MAXCLASS,index,coef);
		}
	      fprintf(fpt,"%d ",PixelLabel[i][j]);
	      //		   cout<<"\nPix("<<i<<","<<j<<") : "<<PixelLabel[i][j];
		/*30.12*/delete []ar;
	    }
	  fprintf(fpt,"\n");
	}
    }
  //cout<<"END OF DS"<<endl;
  //cout<<"Lowest : "<<Lowest<<endl;
  
  fclose(fpt);
  for(i=0; i<NumSensors; i++)delete[](F[i]);
  delete[](F);
  for(i=0; i<NumSensors; i++)delete[](InitialMass[i]);
  delete[](InitialMass);
  delete[](Mass);
  for(i=0; i<MAXCLASS; i++)delete[] lambda[i];
  delete[] lambda;
	
#ifdef REGIONMASSCALC
  //cout<<"END OF DS FUSION"<<endl;
  for (i=0; i<nr; i++)
    delete[] regionLabels[i];
  
  delete[] regionLabels;
#endif

	printf("\nEnd of classify pixel.%d, %s\n", __LINE__, __FILE__);
}

// stores the masses of classes for each sensor in F
int *initLabel(int NumSensors, int *num_bands, int **img, int xpix, int ypix, float **lambda){
  int i, k, m, MAXSENSORS = NumSensors;
  double x[NUM], temp;
  int *cl=(int *)malloc(2*sizeof(int));
  
  //cout<<"Initlabel()";
  
  // CLASS CONDITIONAL PROBABILITY CALCULATIONS
  
  for(i=0;i<NumSensors;i++){
    double mass = 0.00;
    for(k=1;k<=ncl;k++){
		if(num_bands[i]>1){
			for(m=0;m<num_bands[i];m++)
			  x[m]=img[i][m]-mean[i][m][k];
			temp=mult(x, incov[i][k], num_bands[i]);
			temp*=(-0.5);
			temp+=(-0.5*log(fabs((double)det[k])));
			F[i][k] = pow(E, temp);
		}
		else{
			temp = -pow(img[i][0] - mean[i][0][k], 2);
			temp /= incov[i][k][0][0];
			F[i][k] = pow(E, temp)/pow(incov[i][k][0][0], 0.5);
		}
		if(mass < F[i][k])
		{
		  mass = F[i][k];
		  cl[i]=k;
		  
		}
      
	}
		
  }
  return cl;
 
}



//------------------------------------------------------------------//
// FOR ALL PIXELS S, labels pixel S by DS fusion
int PixelClass(int NumSensors, int *num_bands, int **img, int xpix, int ypix, float **lambda){
  int i, k, m, MAXSENSORS = NumSensors;
  double x[NUM], temp;
  
  //cout<<"PixelClass()";
  // CLASS CONDITIONAL PROBABILITY CALCULATIONS

  // MASS INITIALIZATIONS
  int j;
  double SumOverUnions, massNotNormalized;
  for(k=0; k<MAXSENSORS; k++){
    SumOverUnions = 0;
    for(i=0; i<SubsetSize; i++){
      massNotNormalized = 0; //1;
      for(j=0; SubsetArray[i][j]!=-1; j++){
		massNotNormalized +=  F[k][SubsetArray[i][j]];
      }
#ifdef NORMALIZEMASSES
      //UNCOMMENT SUMOVERUNIONS UPDATE FOR NORMALIZATION
      SumOverUnions += massNotNormalized;
#endif
      InitialMass[k][i] = massNotNormalized;
      //cout<<""<<InitialMass[k][i]<<endl;
    }
#ifdef NORMALIZEMASSES
    // UNCOMMENT LOOP TO NORMALIZE
    for(i=0; i<SubsetSize; i++)
      InitialMass[k][i] /= SumOverUnions;
#endif
  }
  // FUSION
  list<int*>::iterator KSetIt, NullSetIt;
  int* KArray;
  
  int FactNo;
  double Sum, Prod;
  
#ifdef NORMALIZEFUSION
  double NullSum = 0;
  for(NullSetIt = NullSetList.begin(); NullSetIt != NullSetList.end(); NullSetIt++){
    Prod = 1;
    KArray = *NullSetIt;
    for(FactNo = 0; FactNo<Factorial; FactNo++){
      Prod = 1;
      for(j=0; j<MAXSENSORS; j++){
	Prod *= ::pow(InitialMass[Perms[FactNo][j]][KArray[j]], lambda[k][j]);
      }
      NullSum += Prod;
    }
  }
#endif
  
  for(k=0; k<MAXCLASS; k++){
    Sum = 0;
    for(KSetIt = KSetList[k].begin(); KSetIt != KSetList[k].end(); KSetIt++){
      Prod = 1;
      KArray = *KSetIt;
      for(FactNo = 0; FactNo<Factorial; FactNo++){
		Prod = 1;
		for(j=0; j<MAXSENSORS; j++){
		  Prod *= ::pow(InitialMass[Perms[FactNo][j]][KArray[j]], lambda[k][j]);
		}
		Sum += Prod;
      }
    }
    Mass[k] = Sum;
    
#ifdef NORMALIZEFUSION
    Mass[k] /= NullSum;
#endif
  }
  
#ifdef REGIONMASSCALC
  for (k=0; k<MAXCLASS; k++)
    {
      betPFunction[regionLabels[ypix][xpix]][k] += Mass[k];
      //cout<<"MASS "<<Mass[k]<<endl;
    }
  //return 0;
#endif
  double MaxM = -9999;
  int MaxK;
  for(k=0; k<MAXCLASS; k++){
    //cout<<"MASS[k]"<<Mass[k]<<endl;
    if(MaxM<Mass[k]){
      MaxM = Mass[k];
      MaxK = k;
    }
    
  }
  //cout<<"the value returned "<<MaxK<<endl;
  
  return MaxK;
}



//------------------------------------------------------------------//
// FOR ALL PIXELS S, labels pixel S by DS fusion
int PixelClass1(int NumSensors, int *num_bands, int **img, float** lambda){
  int i, k, m, MAXSENSORS = NumSensors;
  double x[NUM], temp;
  
  // CLASS CONDITIONAL PROBABILITY CALCULATIONS
  
  for(i=0;i<NumSensors;i++){
    for(k=1;k<=ncl;k++){
      if(num_bands[i]>1){
	for(m=0;m<num_bands[i];m++)
	  x[m]=img[i][m]-mean[i][m][k];
	temp=mult(x, incov[i][k], num_bands[i]);
	temp*=(-0.5);
	temp+=(-0.5*log(fabs((double)det[k])));
	F[i][k] = pow(E, temp);
      }
      else{
	temp = -pow(img[i][0] - mean[i][0][k], 2);
	temp /= incov[i][k][0][0];
	F[i][k] = pow(E, temp)/pow(incov[i][k][0][0], 0.5);
      }
    }
  }
  // MASS INITIALIZATIONS
  int j;
  double SumOverUnions, massNotNormalized;
  for(k=0; k<MAXSENSORS; k++){
    SumOverUnions = 0;
    for(i=0; i<SubsetSize; i++){
      massNotNormalized = 0; //1;
      for(j=0; SubsetArray[i][j]!=-1; j++){
	massNotNormalized +=  F[k][SubsetArray1[i][j]];
      }
      //UNCOMMENT SUMOVERUNIONS UPDATE FOR NORMALIZATION
      InitialMass[k][i] = massNotNormalized;
    }
    // UNCOMMENT LOOP TO NORMALIZE
  }
  // FUSION
  list<int*>::iterator KSetIt, NullSetIt;
  int* KArray;
  double NullSum = 0;
  int FactNo;
  double Sum, Prod;
  for(k=0; k<MAXCLASS; k++){
    Sum = 0;
    for(KSetIt = KSetList[k].begin(); KSetIt != KSetList[k].end(); KSetIt++){
		Prod = 1;
		KArray = *KSetIt;
		for(FactNo = 0; FactNo<Factorial; FactNo++){
			Prod = 1;
			for(j=0; j<MAXSENSORS; j++){
			  Prod *= ::pow(InitialMass[Perms[FactNo][j]][KArray[j]], lambda[k][j]);
			}
		Sum += Prod;
      }
    }
    Mass[k] = Sum; ///NullSum;
	}
  double MaxM = -9999;
  int MaxK;
  for(k=0; k<MAXCLASS; k++){
    if(MaxM<Mass[k]){
      MaxM = Mass[k];
      MaxK = k;
    }
    
  }
  
  return MaxK;
}



// the kneser function which evaluates the combination of 1,2,3 subset of the classes

// CALCULATION OF PERMUTATIONS
int *Visited;
void CalcPermsRecur(int** Perms, int *PermList, int Tot, int k, int *PermNo);
void CalcPerms(int **Perms, int Tot){
  Visited = new int[Tot];
  int i;
  for(i=0; i<Tot; i++)Visited[i] = 0;
  int *PermList = new int[Tot];
  int PermNo = 0;
  CalcPermsRecur(Perms, PermList, Tot, 0, &PermNo);
}

void CalcPermsRecur(int** Perms, int *PermList, int Tot, int k, int *PermNo){
  int *NewPermList;
  int i;
  if(k==Tot){
    NewPermList = new int[Tot];
    for(i=0; i<Tot; i++)NewPermList[i] = PermList[i];
    Perms[(*PermNo)++] = NewPermList;
  }
  for(i=0; i<Tot; i++){
    if(!Visited[i]){
      Visited[i] = 1;
      PermList[k] = i;
      CalcPermsRecur(Perms, PermList, Tot, k+1, PermNo);
      Visited[i] = 0;
    }
  }
}

//NORMALIZATION OF RGB TO I   (divide pixel intensities by (Rm + Gm + Bm) in all bands)
void CompIntensity(CImage *img){
  int i,j,k;
  double rgbmean,bandmean[3];
  for(k=0;k<3;k++){
    for(i=0;i<nr;i++)
      for(j=0;j<nc;j++)
	bandmean[k]+=img[k][i][j];
    bandmean[k]/=nr;
    bandmean[k]/=nc;
  }
  rgbmean = (bandmean[1]+bandmean[2]+bandmean[0]);
  
  for(k=0;k<3;k++)
    for(i=0;i<nr;i++)
      for(j=0;j<nc;j++){
	img[k][i][j]*=255;
	img[k][i][j]/=rgbmean;
      }	
}

//INITIALIZATION USING LOOKUP TABLE

void Lookup(int num_bands, int sensor, int blue, int green, int red, int ir){
  
  int i,j,k,m,n,cl,band,jctr,kctr;
  char input[10];
  FILE* f = fopen("LookupTable_14.txt","r");
  
  for(i=0;i<num_bands;i++)
    for(j=1;j<=ncl;j++)
      mean[sensor][i][j]=0;
  
			
  if(sensor==1){
    fscanf(f,"%s",input);
    while(input[1]!='-')
      fscanf(f,"%s",input);
    input[2]='\0';
    
    for(i=1;i<=ncl;i++){
        for(m=0;m<num_bands;m++){
          for(n=0;n<num_bands;n++)
    			   cov[i][m][n]=0;
        }
        
        fscanf(f,"%d", &cl);					
        for(j=0;j<num_bands;j++){
          for(k=0;k<num_bands;k++){
            fscanf(f,"%lf", &cov[cl][j][k]);
          
          }
          
        }
    }   
  
      for(i=1;i<=ncl;i++){
        for(j=0;j<num_bands;j++){
          fscanf(f,"%s%s",input,input);fscanf(f,"%d",&cl);
          fscanf(f,"%s%s",input,input);fscanf(f,"%d",&band);
          fscanf(f,"%s%s",input,input);fscanf(f,"%lf",&mean[sensor][band][cl]);
        }
      }
  
  }
  if(sensor==0){
                
      for(i=1;i<=ncl;i++){
        for(m=0;m<num_bands;m++){
          for(n=0;n<num_bands;n++)
    			   cov[i][m][n]=0;
        }
        
        fscanf(f,"%d", &cl);					
        for(j=0,jctr=0;j<4;j++,jctr++){
          for(k=0,kctr=0;k<4;k++,kctr++){
            fscanf(f,"%lf", &cov[cl][jctr][kctr]);
            if((k==0 && blue==0)||(k==1 && green==0)||(k==2 && red==0)&&(k==3 && ir==0))
                     kctr--;
          }
          if((j==0 && blue==0)||(j==1 && green==0)||(j==2 && red==0)&&(j==3 && ir==0))
                     jctr--;
        }
      }
      
   for(i=1;i<=ncl;i++){
    for(j=0,jctr=0;j<4;j++,jctr++){
      fscanf(f,"%s%s",input,input);fscanf(f,"%d",&cl);
      fscanf(f,"%s%s",input,input);fscanf(f,"%d",&band);
      fscanf(f,"%s%s",input,input);fscanf(f,"%lf",&mean[sensor][jctr][cl]);
      if((j==0 && blue==0)||(j==1 && green==0)||(j==2 && red==0)&&(j==3 && ir==0))
                     jctr--;
    }
  }
      
  }			
  
  fclose(f);

// reading from the lookup table by Rahul..
  
  /*...	printing MEAN & COVARIANCE MATRIX...
  for(i=1;i<=ncl;i++)
    for(j=0;j<num_bands;j++)
      cout<<"class : "<<i<<"band : "<<j<<"mean : "<<mean[sensor][j][i]<<endl;
  for(i=1;i<=ncl;i++){
    cout<<endl<<"Class :"<<i<<endl;
    for(j=0;j<num_bands;j++){
      for(k=0;k<num_bands;k++)
	cout<< cov[i][j][k]<<" ";
      cout<<endl;
    }
    cout<<endl;
  }
  //...	printing DONE.......................*/
	
  if(num_bands > 1){
    for(k=1;k<=ncl;k++){
      det[k]=inver(cov[k],incov[sensor][k], num_bands);
    }
  }
  else{
    for(k=1;k<=ncl;k++)incov[sensor][k][0][0] = cov[k][0][0];
  }
}

// IMAGE INITIALIZATION & CLASS MATRIX INITIALIZATIONS  
/*											uncomment if groundtruth file is provided.
void	Initialize(int num_bands, int sensor, CImage *img){
  //char file[NUM][130], fng[130], ftg[130];
  //FILE *fm[NUM],*fp;
  int i,j,k,m,n,tot_pix,o; //l;
  int **gmg,cl_pix[30];
  double t[NUM];
  gmg = (int **)malloc(nr*sizeof(int *));
  for(i=0; i<nr; i++)
    gmg[i] = (int*)malloc(nc*sizeof(int));
  tot_pix=0;
  for(i=0;i<30;i++)
    cl_pix[i]=0;
  for(i=0;i<nr;i++){
    for(j=0;j<nc;j++){
      gmg[i][j]=SegImg[i][j];
      if(gmg[i][j]!=0) tot_pix++;
      cl_pix[gmg[i][j]]++;
    }
  }
  for(i=0;i<num_bands;i++)
    for(j=0;j<=ncl;j++)
      mean[sensor][i][j]=0;
  for(i=0;i<nr;i++){
    for(j=0;j<nc;j++){
      if (gmg[i][j]==0) continue;
      for(k=0;k<num_bands;k++)
	mean[sensor][k][gmg[i][j]]+=img[k][i][j];
    }
  }
  for(k=1;k<=ncl;k++){
    for(m=0;m<num_bands;m++){
      t[m]=0;
      for(n=0;n<num_bands;n++)
	cov[k][m][n]=0;
    }
    
    for(i=0;i<nr;i++){
      for(j=0;j<nc;j++){
	if(gmg[i][j]!=k) continue;
	for(m=0;m<num_bands;m++)
	  t[m]+=img[m][i][j];
	for(m=0;m<num_bands;m++)
	  for(n=0;n<num_bands;n++)
		cov[k][m][n]+=img[m][i][j]*img[n][i][j];
      }
    }
    
    for(o=0;o<num_bands;o++)
      mean[sensor][o][k]/=cl_pix[k];
    for(m=0;m<num_bands;m++)
      {
	for(n=0;n<num_bands;n++){
	  cov[k][m][n]-=((double)t[m]*(double)t[n]/(double)cl_pix[k]);
	  cov[k][m][n]/=(double)cl_pix[k];
	}
      }
    
  }
  if(num_bands > 1){
    for(k=1;k<=ncl;k++){
      det[k]=inver(cov[k],incov[sensor][k], num_bands);
    }
  }
  else{
    for(k=1;k<=ncl;k++)incov[sensor][k][0][0] = cov[k][0][0];
  }
}
*/

// MATRIX OPERATIONS

double mult(double a[],double b[][NUM], int num_bands)
 {  
  int i,j;
  double c[NUM],t1=0;
  for(i=0;i<num_bands;i++)
    {
      c[i]=0;
      for(j=0;j<num_bands;j++)
	c[i]+=a[j]*b[j][i];
    }
  t1=0;
  for(i=0;i<num_bands;i++)
    t1+=c[i]*a[i];
  return(t1);
 }  


double inver(double a[][NUM],double b[][NUM], int num_bands)
{
  int i,j;
  double det1=1;
  for(i=0;i<num_bands;i++)
    {
      for(j=0;j<num_bands;j++)
      b[i][j]=0;
      b[i][i]=1;
    }
  for(i=0;i<num_bands;i++)
    zerocol(a,b,i, num_bands);
  for(i=0;i<num_bands;i++)
    det1*=a[i][i];
  for(i=0;i<num_bands;i++)
    {
      for(j=0;j<num_bands;j++)
	b[i][j]/=a[i][i];
      a[i][i]=1;
   }
  return(det1);
}


//*	FOLLOWING TWO FUNCTIONS ARE COMMENTED BECAUSE
//	THEY WERE CLASHING WITH METHODS IN ANOTHER FILE

void zerocol(double a[][NUM],double b[][NUM],int p, int num_bands)
{
  int i;
  // void zerorow(double a[][NUM],double b[][NUM],int i,int j, num_bands);
  for(i=0;i<num_bands;i++)
    {
    if(i!=p)
      zerorow(a,b,p,i, num_bands);
    }
}

void zerorow(double a[][NUM],double b[][NUM],int p,int k, int num_bands)
{
  int i;
  double t= a[k][p]/a[p][p];
  for(i=0;i<num_bands;i++)
   {
     a[k][i]-=t*a[p][i];
     b[k][i]-=t*b[p][i];
   }
}
