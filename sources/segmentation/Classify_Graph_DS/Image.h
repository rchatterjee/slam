// Image.h: interface for the CImage class.
//
//////////////////////////////////////////////////////////////////////

#ifndef	IMAGE_H
#define IMAGE_H

#ifndef	_MAX_PATH
#define	_MAX_PATH	256
#endif	//of _MAX_PATH

typedef	unsigned long	ulong;
typedef	int	BOOL;

#ifndef	TRUE
#define	TRUE	1
#endif	//of TRUE

#ifndef	FALSE
#define	FALSE	0
#endif	//of FALSE

class CImage 
{
public:
	enum	FileType{ftBinary = 0, ftText = 1};
	CImage &Copy(const CImage &src);
	CImage(const char *pszFileName, int nWidth, int nHeight, 
				FileType fileType = ftBinary);
	CImage(int nWidth, int nHeight, int nPixel = 0);
	CImage(const CImage &src) { Copy(src); };
	CImage(){};
	virtual ~CImage();
public:
	CImage & operator = (const CImage &src) { return Copy(src); };
	virtual BOOL AdjustBoundingPixels(int nPixel = 0);
	virtual	void Display();
	int * operator[](int nIndex);
	void	SetPixel(int x, int y, int nVal);
	int		GetPixel(int x, int y)const;
	int	SubstitutePixel(int nOldVal, int nNewVal);
	int	GetImageHeight()const;
	int	GetImageWidth()const;
	void CImagex(const char *pszFileName, int nWidth, int nHeight,FileType fileType = ftBinary);
	BOOL	IsValidPixel(int x, int y)const;
	BOOL	FreeImageSpace();
	BOOL	SaveImageAs(const char *pszFileName = NULL,FileType enType = ftBinary );
protected:
	BOOL	SaveAsText(FILE *fp);
	BOOL	SaveAsBinary(FILE *fp);
	BOOL	ReadTextFile(FILE *fp);
	BOOL	ReadBinaryFile(FILE *fp);
	BOOL	AllocateImageSpace(int nWidth, int nHeight, int nInitVal = 0);
	BOOL	ReadImageFromFile();
protected:
	FileType m_enFileType;
	int		**m_ppnPixels;
	char	m_szFileName[_MAX_PATH];
	int		m_nWidth;
	int		m_nHeight;
public:
	int	GetNextRegionColor(int x, int y, int nColor = 0, BOOL bWrap = TRUE)const;
};

#endif //of IMAGE_H
