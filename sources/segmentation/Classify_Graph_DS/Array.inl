#include <iostream>
#include <memory.h>
#include <malloc.h>
//////////////////////////////////////////////////////////////////////
// CArray<class T> Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/*
template<class T>
inline CArray<T>::CArray<T>(ulong ulUnit)
{
	ALLOC_UNIT = ulUnit;
	m_pArray = NULL;
	m_ulNumElements = 0;
	m_ulArraySize = 0;
}

template<class T>
inline CArray<T>::~CArray<T>(void)
{
	for(ulong u = 0; u < m_ulNumElements ; u ++)
		DestructElement(u);
}
*/

template<class T>
inline void	CArray<T>::ConstructElement(const T &t, ulong ulIndex)
{
	memcpy(&m_pArray[ulIndex], &t, sizeof(T));
}

template<class T>
inline void	CArray<T>::DestructElement (ulong ulIndex)
{
	m_pArray[ulIndex].~T();
}

template<class T>
inline ulong	CArray<T>::FindElementIndex(const T &elm)
{
	for(ulong u = 0 ; u < m_ulNumElements; u++)
		if(m_pArray[u] == elm)
			return u;
	return NON_INDEX;
}

template<class T>
inline BOOL	CArray<T>::AddElement(const T &elm, BOOL bForce)
{
	if(!bForce)
	{
		if(FindElementIndex(elm) != NON_INDEX)
			return FALSE;
	}
	if(m_ulNumElements >= m_ulArraySize)
	{
		try
		{
			AllocateSpace();
		}
		catch(int)
		{
		//	cout << "Unable to allocate memory..."<<endl;
		//	cout << ">>>>Throwing exception..."<<endl;
			throw;
		}
	}
	
	ConstructElement(elm, m_ulNumElements);
	m_ulNumElements ++;

	return TRUE;
}

template<class T>
inline ulong	CArray<T>::GetNumElements()const
{
	return m_ulNumElements;
}

template<class T>
inline BOOL	CArray<T>::AllocateSpace(BOOL bForce)
{
	if(!bForce && (m_ulNumElements < m_ulArraySize))
		return FALSE;
	ulong	ulNewSize = m_ulArraySize + ALLOC_UNIT;
	if((m_pArray = (T *)realloc(m_pArray, ulNewSize*sizeof(T))) == NULL)
		throw -1;
	m_ulArraySize = ulNewSize;
	return TRUE;
}

template<class T>
inline BOOL	CArray<T>::RemoveElement(T &elm)
{
	ulong	ulIndex;
	while((ulIndex = FindElementIndex(elm)) != NON_INDEX)
		RemoveAt(ulIndex);
	return TRUE;
}

template<class T>
inline T	CArray<T>::RemoveAt(ulong ulIndex)
{
	T	tRet = m_pArray[ulIndex];
	for(ulong u = ulIndex; u < (m_ulNumElements - 1); u++)
		m_pArray[u] = m_pArray[u+1];
	m_ulNumElements--;

	ulong	ulNewSize = (m_ulArraySize - ALLOC_UNIT);
	if(m_ulNumElements < ulNewSize)
	{
		if((m_pArray = (T *)realloc(m_pArray, ulNewSize*sizeof(T))) == NULL)
			throw - 1;
		m_ulArraySize = ulNewSize;
	}
	return tRet;
}

template<class T>
inline BOOL	CArray<T>::EliminateCommon()
{
	BOOL	bRet = FALSE;
	T	elm;
	for(ulong u = 0; u < m_ulNumElements; u++)
	{
		elm = m_pArray[u];
		for(ulong v = u + 1; v < m_ulNumElements; v++)
		{
			if(elm == m_pArray[v])
			{
				RemoveAt(v);
				bRet = TRUE;
			}
		}
	}
	return bRet;
}

template<class T>
inline T &	CArray<T>::operator [](ulong ulIndex) const
{
	static	T	elm;
	if(ulIndex >= m_ulNumElements)
	{
		throw m_ulNumElements;
		return elm;
	}
	return m_pArray[ulIndex];
}

////////////////////////////////////////////////////////////
// CPtrArray implementation
/*
template<class T>
inline CPtrArray<T>::CPtrArray<T>(ulong ulUnit)
{
	ALLOC_UNIT = ulUnit;
	m_pArray = NULL;
	m_ulNumElements = 0;
	m_ulArraySize = 0;
}

template<class T>
inline CPtrArray<T>::~CPtrArray<T>(void)
{
	for(ulong u = 0; u < m_ulNumElements ; u ++)
		delete m_pArray[u];
}
*/
template<class T>
inline ulong	CPtrArray<T>::FindElementIndex(const T *elm)
{
	for(ulong u = 0 ; u < m_ulNumElements; u++)
		if((*m_pArray[u]) == (*elm))
			return u;
	return NON_INDEX;
}

template<class T>
inline BOOL	CPtrArray<T>::AddElement( T *elm, BOOL bForce)
{
	if(!bForce)
	{
		if(FindElementIndex(elm) != NON_INDEX)
			return FALSE;
	}
	if(m_ulNumElements >= m_ulArraySize)
	{
		try
		{
			AllocateSpace();
		}
		catch(int)
		{
			//cout << "Unable to allocate memory..."<<endl;
			//cout << ">>>>Throwing exception..."<<endl;
			throw;
		}
	}
	
	m_pArray[m_ulNumElements] = elm;
	m_ulNumElements ++;

	return TRUE;
}

template<class T>
inline ulong	CPtrArray<T>::GetNumElements()const
{
	return m_ulNumElements;
}

template<class T>
inline BOOL	CPtrArray<T>::AllocateSpace(BOOL bForce)
{
	if(!bForce && (m_ulNumElements < m_ulArraySize))
		return FALSE;
	ulong	ulNewSize = m_ulArraySize + ALLOC_UNIT;
	if((m_pArray = (T **)realloc(m_pArray, ulNewSize*sizeof(T *))) == NULL)
		throw -1;
	m_ulArraySize = ulNewSize;
	return TRUE;
}


template<class T>
inline T *	CPtrArray<T>::RemoveElement(T *elm)
{
	ulong	ulIndex;
	while((ulIndex = FindElementIndex(elm)) != NON_INDEX)
		return RemoveAt(ulIndex);
	return NULL;
}

template<class T>
inline T	*CPtrArray<T>::RemoveAt(ulong ulIndex)
{
	T	*tRet = m_pArray[ulIndex];
	for(ulong u = ulIndex; u < (m_ulNumElements - 1); u++)
		m_pArray[u] = m_pArray[u+1];
	m_ulNumElements--;

	ulong	ulNewSize = (m_ulArraySize - ALLOC_UNIT);
	if(m_ulNumElements < ulNewSize)
	{
		if((m_pArray = (T **)realloc(m_pArray, ulNewSize*sizeof(T*))) == NULL)
			throw - 1;
		m_ulArraySize = ulNewSize;
	}
	return tRet;
}

template<class T>
inline BOOL	CPtrArray<T>::EliminateCommon()
{
	BOOL	bRet = FALSE;
	T	*elm;
	for(ulong u = 0; u < m_ulNumElements; u++)
	{
		elm = m_pArray[u];
		for(ulong v = u + 1; v < m_ulNumElements; v++)
		{
			if((*elm) == (*m_pArray[v]))
			{
				RemoveAt(v);
				bRet = TRUE;
			}
		}
	}
	return bRet;
}

template<class T>
inline T *	CPtrArray<T>::operator [](ulong ulIndex) const
{
	if(ulIndex >= m_ulNumElements)
	{
		throw m_ulNumElements;
		return NULL;
	}
	return m_pArray[ulIndex];
}

///////////////////////////////////////////////////
// CStack

template<class T>
inline int CStack<T>::Pop(T &tT)
{
	ulong ulN = m_aS.GetNumElements();
	if( ulN < 1)
		return 0;
	ulN --;
	tT = m_aS.RemoveAt(ulN);
	return 1;
}

template<class T>
inline void CStack<T>::Push(T tT)
{
	m_aS.AddElement(tT, TRUE);
}

template<class T>
inline int CStack<T>::IsEmpty()const
{
	if(m_aS.GetNumElements() > 0)
		return 0;
	return 1;
}

//CMatrix constructions/destruction
template<class T>
inline CMatrix<T>::CMatrix()
{
	m_nWidth = 0;
	m_nHeight = 0;
	m_ppT = NULL;
}

template<class T>
inline	CMatrix<T>::CMatrix(int nWidth, int nHeight, BOOL bCallCstr)
{
	m_nWidth = 0;
	m_nHeight = 0;
	m_ppT = NULL;
	Construct(nWidth, nHeight, bCallCstr);
}

template<class T>
inline CMatrix<T>::~CMatrix<T>()
{
	Empty();
}

template<class T>
inline void	CMatrix<T>::Empty()
{
	if(m_ppT)
	{
		for(int nIndex = 0; nIndex < GetHeight(); nIndex++)
		{
			for(int j = 0; j < GetWidth(); j++)
				m_ppT[nIndex][j].~T();
			free(m_ppT[nIndex]);
		}
		free(m_ppT);
		m_ppT = NULL;
	}
	m_nWidth = 0;
	m_nHeight = 0;
}

template<class T>
inline	int	CMatrix<T>::Construct(int nWidth, int nHeight, BOOL bCallCstr)
{
	Empty();
	if( (m_ppT = (T **)malloc(nHeight * sizeof(T *))) == NULL)
		return 0;
	m_nHeight = nHeight;
	for(int nIndex = 0; nIndex < m_nHeight; nIndex++)
		m_ppT[nIndex] = (T *)malloc(nWidth * sizeof(T));
	m_nWidth = nWidth;
	if(bCallCstr)
	{
		static const T tT;
		for(int i = 0; i < GetHeight(); i++)
			for(int j = 0; j < GetWidth(); j++)
				memcpy(&m_ppT[i][j], &tT, sizeof(T));
	}
	return 1;
}

template<class T>
T	*CMatrix<T>::operator [](int nIndex)const
{
	return m_ppT[nIndex];
}
