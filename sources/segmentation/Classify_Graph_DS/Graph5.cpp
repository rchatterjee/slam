// Graph.cpp: implementation of the CGraph class.
//
//////////////////////////////////////////////////////////////////////
#define MAIN //DO NOT COMMENT THIS

#define DEMPSTER
#define KAUSTAVDS
//#define CENTERPIXELSONLY
//#define MAKERAGONLY

//for debugging purposes
//#define GRAPH5VERBOSE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <map>
#include <utility>
#include <list>
#include <string>
#include "FTest.cpp"
#include "Image.h"
#include "Image.cpp"
#include "Random.h"
#include "Random.cpp"
#include "ScanFill.h"
#include "ScanFill.cpp"
#include "CGraph.cpp"
#include "RagLoop.cpp"
#include "Merge.cpp"
#include "classify.cpp"

#define ARGUMENTMAXCHARS 63

using namespace std;

int main(int argc, char *argv[]){
	char szOrg[10][250], szSeg[50];

	string dsLambdaFile;
	//char fileForDS[130];
      int subsetCardinalityMax;
	int nWidth, nHeight, nOrgType, nSegType, accrLevel=1,NumBands =0,NumMerge;
	int nDisplayLevel = 3, num_class, NumSensors, *NumBandsArr, totArgs = 0;
	int blue, green, red, ir;
	int bands[5];

		int i, j, dsKneserEnableFlag;
		sscanf(argv[1], "%d", &NumSensors);
		NumBandsArr = new int[NumSensors];
		for(i=0; i<NumSensors; i++){
			sscanf(argv[2+i], "%d", &NumBandsArr[i]);
			NumBands += NumBandsArr[i];
		}

		for(i=0; i<NumBands; i++)
			strcpy(szOrg[i], argv[NumSensors + i + 2]);

		totArgs = NumSensors + NumBands + 2;
		sscanf(argv[totArgs++], "%d", &nWidth);
		sscanf(argv[totArgs++], "%d", &nHeight);
		sscanf(argv[totArgs++], "%d", &NumMerge);

//#ifdef GRAPH5VERBOSE
		//printf("%d %d %d\n", nWidth , nHeight , NumMerge) ;
//#endif

		if(NumMerge>1){
			char** arg = new char*[2*NumMerge+4];

			for (i=0; i<2*NumMerge+4; i++)
				arg[i] = new char[ARGUMENTMAXCHARS+1];

			for(i=0;i<NumMerge;i++)
				strcpy(arg[i*2+4], argv[i+totArgs]);
			totArgs += NumMerge;
			for(i=0;i<NumMerge;i++)
				strcpy(arg[i*2+5], "1");

			sprintf(arg[1],"%d",NumMerge);
			sprintf(arg[2],"%d",nWidth);
			sprintf(arg[3],"%d",nHeight);
			mergemain(i*2+5,arg);
			strcpy(szSeg,"merge.asc");


		}
		else{
			strcpy(szSeg,argv[totArgs++]);
		}
	    //    printf("\nENTER THE .asc file for initial segmentation");
		sscanf(argv[totArgs++], "%d", &accrLevel);
		chaccr(accrLevel);


#ifdef DEMPSTER
		sscanf(argv[totArgs++], "%d", &num_class);
		//strcpy(fileForDS, argv[totArgs++]);
		//cout << "File used for Dempster Shafer:" << fileForDS << endl;
            sscanf(argv[totArgs++], "%d", &subsetCardinalityMax);
		//cout << "SubsetCardinalityMax (use 0 if DS already done): " << subsetCardinalityMax << endl;
		
		dsLambdaFile = argv[totArgs++];		
		
		sscanf(argv[totArgs++], "%d", &blue);
		sscanf(argv[totArgs++], "%d", &green);
		sscanf(argv[totArgs++], "%d", &red);
		sscanf(argv[totArgs++], "%d", &ir);

		bands[0] = blue;
		bands[1] = green;
		bands[2] = red;
		bands[3] = ir;
		bands[4] = 1;				//radar
#endif

		//cout << "Original Image File(s) : ";
		//for(i=0;i<NumBands;i++)
			//cout << szOrg[i] << endl;


		//cout << "Image Size (horiz, vert) : ";
		//cout << nWidth << "," << nHeight << endl;


		//cout << "Image Type (0 -> Binary, 1 -> Text) : ";
		nOrgType=0;
		//cout << nOrgType << endl;
		//cout << "Segmented Image File : ";
		//cout << szSeg << endl;
		//cout << "Image Type (0 -> Binary, 1 -> Text)";
		nSegType = 1;
		//cout<< nSegType << endl;
		//cout << "Image size is same as original image, assumed" << endl;
		nDisplayLevel=12;

#ifdef DEMPSTER
		//KEPT FOR LATER
		//printf("\nENTER THE NUMBER (0 OR 1):\n");
		//printf("\nOPTIONS : ( 0 - USE SINGLETON SETS FOR FIRST SENSOR)\n");
		//printf("(1- USE THE VARIOUS COMBINATIONS FOR ALL SENSORS):");
		//scanf("%d",&flag);


#endif

	//cout << "Now RAGLoop ..." << flush;

	RAGLoop(szOrg, NumBands, NumSensors, NumBandsArr, nWidth, nHeight, nOrgType, szSeg, nSegType, accrLevel, nDisplayLevel, num_class, subsetCardinalityMax, dsLambdaFile, dsKneserEnableFlag, blue, green, red, ir);

	char s1[] = "segment.asc";
	char s2[] = "out.asc";

	char **szOrgPtr = new char*[10];
	for(i=0; i<10; i++){
		szOrgPtr[i] = new char[250];
		szOrgPtr[i] = szOrg[i];
		
	}

	Classify(NumSensors, NumBands, 0, nWidth, nHeight, szOrgPtr, s1, s2, num_class, bands);
	return 0;
}
