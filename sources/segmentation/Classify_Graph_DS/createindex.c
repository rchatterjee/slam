
//to be ran only once during the program
int ***createindex(int K)
{
	int ***index;
	index = (int***)malloc(sizeof(int**)*K);
	int i,s1,s2;
	s1=2*K-2;
	s2=K*K-3*K+2;
	for(i=0;i<K;i++)
	{
	index[i]=(int**)malloc(3*sizeof(int*));
        index[i][0]=(int*)malloc(sizeof(int));
        index[i][1]=(int*)malloc(sizeof(int)*s1);   
	index[i][2]=(int*)malloc(sizeof(int)*s2);   

	}
	int j,l,m,n;
	for(i=0;i<K;i++)
	{
	   index[i][0][0]=i*K+i;
           j=0;
	   n=0;
	   for(l=0;l<K;l++)
	   {
	      if(l!=i)
	      {
		for(m=0;m<K;m++)
		{
			if(l!=m&&m!=i)
			{
			index[i][2][n]=l*K+m;
			n++;
			}
			
		}
		index[i][1][j]=i*K+l;
		j++;
		index[i][1][j]=l*K+i;
		j++;
	      }
	   }
	
	}
	return index;
}


//to be ran only once during program
int *createmultiplier(int K)
{
	int *coef,i;
	coef = (int*)malloc(sizeof(int)*K);
	coef[0] = pow(2,K)-1;
	if(K>1)
	coef[1] = pow(2,K-1)-1;
	if(K>2)
	coef[2] = pow(2,K-2)-1;

	return coef;
}

//to be run separately for every pixel
double *createmassvalues(int K)
{
   double *massvalues = (double*)malloc(sizeof(double)*K*K);//Number of sensors is assumed to be 2 if otherwise use power function K^NumSensers
   int i,j,l;
   l=0;
   for(i=1;i<=K;i++)
   {
	for(j=1;j<=K;j++)
	{
	   massvalues[l]=F[0][i]*F[1][j];
	   l++;
	}
   }
   return massvalues;
   
}


//to be run separately for every pixel
int PixelClass(int NumSensors, int *num_bands, int **img, int xpix, int ypix, float **lambda,int K,int ***index,int *coef)
{

   double *massvalues=createmassvalues(K);
   int k,i,j,l,s1,s2;
   s1=2*K-2;
   s2=K*K-3*K+2;
   for(k=0;k<K;k++)
   {
	Mass[k]=0;
	Mass[k]+=coef[0]*massvalues[index[k][0][0]];
	for(i=0;i<s1;i++)
	  Mass[k]+=coef[1]*massvalues[   index[k][1][i]   ];
        for(i=0;i<s2;i++)
	  Mass[k]+=coef[2]*massvalues[   index[k][2][i]   ];
    }

	
   	double MaxM = -9999;
	int MaxK=1;
	for(k=0; k<K; k++){
	       //cout<<"MASS[k]"<<Mass[k]<<endl;
		if(MaxM<Mass[k]){
			MaxM = Mass[k];
			MaxK = k;
		}
	}
    free(massvalues);
        return MaxK;

}
