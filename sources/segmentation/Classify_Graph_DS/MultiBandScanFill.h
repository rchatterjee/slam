// MultiBandScanFill.h: interface for the CMultiBandScanFill class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MULTIBANDSCANFILL_H__D338AB53_9EE9_4C67_B85B_A8E1BE9190CE__INCLUDED_)
#define AFX_MULTIBANDSCANFILL_H__D338AB53_9EE9_4C67_B85B_A8E1BE9190CE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
//#include "ScanFill.cpp"

using std::vector;
class CImage;
class CMultiBandScanFill : public CScanFill  
{
public:
	CMultiBandScanFill();
	virtual ~CMultiBandScanFill();

protected:
	virtual int TestPixel(int x, int y, int nFillColor, int nOldColor);

public:
	virtual	int ScanFill(int x, int y, int nFillColor, int nOldColor);
	void AttachOutputImage(CImage* pOut);
	void AddBandImage(CImage* pImage);

private:
	typedef vector<int>     FeatureVector;
	typedef vector<CImage*> ImageVector;

	FeatureVector m_veFeatures;
	ImageVector   m_veBands;
};

#endif // !defined(AFX_MULTIBANDSCANFILL_H__D338AB53_9EE9_4C67_B85B_A8E1BE9190CE__INCLUDED_)
