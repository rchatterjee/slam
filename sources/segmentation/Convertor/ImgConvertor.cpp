#include<iostream>
#include<stdio.h>
#include<stdlib.h>

using namespace std;

int main(int argc, char *argv[])
{
   int height,width,i,j,k,l,numclus;
   height=atoi(argv[3]);
   width=atoi(argv[2]);
   numclus=atoi(argv[4]);
   int startsWith=atoi(argv[5]);
   FILE* fp=fopen(argv[1],"r");
   if(!fp) cout<<"File read ERROR!!!!!!!!!!!!!!!"<<endl;
   int c[30][3]={
   		 {255,255,255},
		 {204, 204, 204},
		 {0, 102, 0},
		 {255, 0, 0},
		 {255, 153, 153},
		 {0, 255, 255},
		 {255, 153, 153},
		 {153, 153, 0},
		 {204, 255, 255},
		 {0, 102, 153},
		 {51, 0, 51},
		 {153, 153, 255},
		 {0, 0, 153},
		 {51, 153, 255},
		 {51, 255, 51},
		 {255,100, 0}
   };
   FILE* fp1=fopen("colormap-final-14","rb");
   if(fp1){// cout<<"Color File read ERROR!!!!!!!!!!!!!!!"<<endl;
      for(i=1;i<numclus+1;i++){
	 for(j=0;j<3;j++){
	    c[i][2-j]=fgetc(fp1);
	 }
      }
      c[0][0]=255;
      c[0][1]=255;
      c[0][2]=255;
      fclose(fp1);
   }
   fp1=fopen("out.ppm", "w");
   fprintf(fp1, "P3\n#out.ppm\n%d %d\n255\n",width, height);
   for(i=0;i<height;i++){
      for(j=0;j<width;j++){
	 //l=fgetc(fp);
	 fscanf(fp,"%d",&l);
	 fprintf(fp1,"%d %d %d\t",c[l-startsWith][2], c[l-startsWith][1], c[l-startsWith][0]);
      }
      fprintf(fp1, "\n");
   }
   fclose(fp);
   fclose(fp1);
   return 0;
}
