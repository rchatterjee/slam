blue=1_1.raw
green=1_2.raw
red=1_3.raw
ir=1_4.raw
radar=1_5.raw
width=587
height=642
echo "You entered:"
echo "Files for the"
echo "Blue Band: $blue"
echo "Green Band: $green"
echo "Red Band: $red"
echo "IR Band: $ir"
echo "Radar Band: $radar"
echo "Width: $width"
echo "Height: $height"
echo "Proceed? (y/n)"
read com
if [ $com = y ]
then
./uniform.out $green $width $height initseg1.asc 0 3 -f 2.1 
./uniform.out $radar $width $height initseg2.asc 0 3 -f 2.1
./graph.out 2 4 1 $blue $green $red $ir $radar $width $height 2 initseg1.asc initseg2.asc 2 15 0 dsl 1 1 1 1

mv out.asc Result/
make clean
./convertor.out Result/out.asc $height $width 15 0
mv DSfile.jpg Result/out.jpg
echo "PROCESS COMPLETED SUCCESFULLY"
fi
