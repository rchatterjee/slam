echo "Enter the .raw files:"
echo "Enter blue band:"
read blue
echo "Enter green band:"
read green
echo "Enter red band:"
read red
echo "Enter IR band:"
read ir
echo "Enter radar band:"
read radar
echo "Enter Dimensions:"
echo "Width:"
read width
echo "Height:"
read height
echo "You entered:"
echo "Files for the"
echo "Blue Band: $blue"
echo "Green Band: $green"
echo "Red Band: $red"
echo "IR Band: $ir"
echo "Radar Band: $radar"
echo "Width: $width"
echo "Height: $height"
echo "Proceed? (y/n)"
read com
if [ $com = y ]
then
./uniform.out $green $width $height initseg1.asc 0 3 -f 2.1 
./uniform.out $radar $width $height initseg2.asc 0 3 -f 2.1
./graph.out 2 4 1 $blue $green $red $ir $radar $width $height 2 initseg1.asc initseg2.asc 2 14 0 dsl 1 1 1 1

mv out.asc Result/
make clean
./convertor.out Result/out.asc $height $width 14 0
mv DSfile.jpg Result/out.jpg
echo "PROCESS COMPLETED SUCCESFULLY"
fi
