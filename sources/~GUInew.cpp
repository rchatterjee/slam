#include<vector>
#include <stdlib.h>
#include <highgui.h>
#include <cv.h>
#include <iostream>
#include <stdio.h>

#define MAX_NUM_MOVE 100
#define SCALE 2
using namespace std;
using namespace cv;

const Scalar RED = Scalar(0, 0, 255);
const Scalar PINK = Scalar(230, 130, 255);
const Scalar BLUE = Scalar(255, 0, 0);
const Scalar LIGHTBLUE = Scalar(255, 255, 160);
const Scalar GREEN = Scalar(0, 255, 0);

enum {
    NOT_SET = 0, IN_PROCESS = 1, SET = 2
};
//void showOriginal(int numfiles, char *files, int w, int h);

class pose{
public:
   CvPoint p;
   float angle;
   pose(CvPoint p1=cvPoint(0,0), float ang= 0.0){
      p=cvPoint(p1.x, p1.y);
      angle=ang;
   }
   pose(int x, int y,float ang){
      p=cvPoint(x,y);
      angle=ang;
   }      
};


CvPoint *RotateRect(CvPoint r, float angle, CvSize s)
{                                                                                                                                                            
   CvPoint *p=new CvPoint[4];
   int w=s.width/2, h=s.height/2;   
   float ct=cos(angle), st=sin(angle);
   p[0]=cvPoint(cvRound(r.x+((-w)*ct-(-h)*st)),cvRound(r.y+((-w)*st+(-h)*ct)));
   p[1]=cvPoint(cvRound(r.x+((-w)*ct-h*st)),   cvRound(r.y+((-w)*st+h*ct)));
   p[2]=cvPoint(cvRound(r.x+(w*ct-h*st)),    cvRound(r.y+(w*st+h*ct)));
   p[3]=cvPoint(cvRound(r.x+(w*ct-(-h)*st)),   cvRound(r.y+(w*st+(-h)*ct)));
   return p;
}

float findAngle(CvPoint p1, CvPoint p2){
   return atan2(p2.y-p1.y, p2.x-p1.x);
}
float findDist(CvPoint p1, CvPoint p2){
   float d;
   d=(p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y);
   return sqrt(d);
}

class GUI{
public:
   IplImage *img;
   pose *mv_points; //mv_points[0]=start point
   CvSize cutImg_s;
   int nMove;
private:
   CvPoint **pt;
   int radius, lineWidth;
   CvRect start;
   char *windowName;
   int decideState, movementState;
public:
   GUI(char *imgName=NULL, char *winName=NULL){
      img=cvLoadImage(imgName);
      windowName=new char[strlen(winName)];
      strcpy(windowName, winName);
      if(!img){
	 printf("File Loading Error in GUI_new.\n");
	 exit(0);
      }
      mv_points=new pose[MAX_NUM_MOVE];
      pt=new CvPoint*[MAX_NUM_MOVE];
      
      lineWidth=2; radius=3;
      for(int i=0;i<MAX_NUM_MOVE;i++)pt[i]=new CvPoint[4];
      nMove=0;
      decideState=movementState=NOT_SET;
      cvShowImage(windowName, img);

   }
   void reset(){
      nMove=0;
      decideState=movementState=NOT_SET;
      cutImg_s=cvSize(0,0);
   }

   void showImage(int x=0, int y=0){
      CvArr *res=cvCloneImage(img);
      //printf("d_state: %d, m_State: %d, nMoves: %d\n", decideState, movementState, nMove);
            //      cvShowImage(windowName, res);
      if(decideState + movementState){
      if( decideState == IN_PROCESS ){
	 cvRectangle(res, cvPoint(start.x, start.y), cvPoint(x,y), GREEN, lineWidth);
      }
      else if(decideState == SET ){
	 int nCurvePts[nMove];
	 //nCurvePts[0]=4;
	 for(int i=0;i<nMove;i++){
	    nCurvePts[i]=4;
	    cvCircle(res, mv_points[i].p, radius, GREEN, lineWidth);
	 if(i)
	    cvLine(res, mv_points[i-1].p, mv_points[i].p, RED, lineWidth);
	 //printf("%d, %d\t",pt[i][2].x, pt[i][2].y);
	 }
	 cvPolyLine(res, pt, nCurvePts, nMove, 1, BLUE, lineWidth);
	 // cvSaveImage("dekhi.jpg", res);
      }
      int nCurvePts[1]={4};;
      if( movementState == IN_PROCESS ){
	 CvPoint *x[1]={RotateRect(mv_points[nMove].p, mv_points[nMove].angle, cutImg_s)};
	 cvPolyLine(res, x, nCurvePts, 1, 1, LIGHTBLUE, lineWidth);
      }
      else if( movementState == SET ){
	 CvPoint *x[1]={RotateRect(mv_points[nMove-1].p, mv_points[nMove-1].angle, cutImg_s)};
	 cvPolyLine(res, x, nCurvePts, 1, 1, PINK, lineWidth);
	 cvCircle(res, mv_points[nMove-1].p, radius, RED, lineWidth);
      }
      else {
	 CvPoint *x[1]={RotateRect(mv_points[nMove-1].p, mv_points[nMove-1].angle, cutImg_s)};
	 cvPolyLine(res, x, nCurvePts, 1, 1, PINK, lineWidth);
	 cvCircle(res, mv_points[nMove-1].p, radius, BLUE, lineWidth);
      }
      }
      cvShowImage(windowName, res);
	cvRelease(&res);
   }
   int t;
   void mouseClick(int event, int x, int y, int flags, void* param) {
      switch (event) {
      case CV_EVENT_LBUTTONDOWN:
	 {
	    if (decideState == NOT_SET) {
	       decideState = IN_PROCESS;
	       start = cvRect(x, y, 1, 1);
            } else if (decideState == SET && movementState == IN_PROCESS) {
	       movementState = SET;
	       //rect[rectcnt] = Rect(x - info.width / 2, y - info.height / 2, info.width, info.height);
	       mv_points[nMove] = pose(x,y,findAngle(mv_points[nMove-1].p,cvPoint(x,y)));
	       pt[nMove]=RotateRect(mv_points[nMove].p, mv_points[nMove].angle, cutImg_s);
	       nMove++;
	       //printf("nMove: %d\n", nMove);
            }
	 }
	 break;
      case CV_EVENT_LBUTTONUP:
	 {
            if (decideState == IN_PROCESS) {
	       //start = Rect(cvPoint(start.x, start.y), Point(x, y));
	       // both start point and size are set here.
	       mv_points[nMove] = pose((start.x+x)/2, (start.y+y)/2, 0.0);
	       cutImg_s=cvSize(fabs(x-start.x), fabs(y-start.y));
	       pt[0]=RotateRect(mv_points[nMove].p, mv_points[nMove].angle, cutImg_s);
	       decideState = SET;
	       //printf(">>>\t%d, %d, %f\t\tSize:   %d, %d\n", mv_points[nMove].p.x,mv_points[nMove].p.y, mv_points[nMove].angle, cutImg_s.width, cutImg_s.height);
	       nMove++;
            } else if (movementState == SET) {
	       
	       movementState = NOT_SET;
	       if (nMove > MAX_NUM_MOVE - 1) {
		  cout << nMove << "Exceeded" << endl;
		  exit(0);
	       }
            }
	 }
	 break;
      case CV_EVENT_MOUSEMOVE:
	 // printf("mouse Move: %d\n", decideState);
	 if (decideState == IN_PROCESS) {
	    start = Rect(Point(start.x, start.y), Point(x, y));
	 } else if (decideState == SET && (movementState != SET)) {
	    movementState = IN_PROCESS;
	    // x -= cutImg_s.width / 2;
	    //y -= cutImg_s.height / 2;
	    mv_points[nMove] =  pose(x,y,findAngle(mv_points[nMove-1].p,cvPoint(x,y)));
	    pt[nMove]=RotateRect(mv_points[nMove].p, mv_points[nMove].angle, cutImg_s);	    
	 }
	 break;
      default:
	 break;
      }
      //printf("@ <%d, %d> \n",x,y);
      showImage(x,y);
   }

};

void usage() {
    printf("<numrawimg> <rawimages> <width> <height>\n");
    printf("Please Note: Max num of cut image = %d\n", MAX_NUM_MOVE);
    exit(0);
    return;
}

GUI *g;

void on_mouse(int event, int x, int y, int flags, void* param) {
    g->mouseClick(event, x, y, flags, param);
}

int main(int argc, char** argv) {
    if (argc < 3) usage();
    char rawfiles[1000];
    int numrawimg, height, width, numParticles, error, flag=0;
    numrawimg = atoi(argv[1]);
    sprintf(rawfiles, "");
    for (int i = 0; i < numrawimg; i++)
        sprintf(rawfiles, "%s%s ", rawfiles, argv[i + 2]);
    height = atoi(argv[3 + numrawimg]);
    width = atoi(argv[2 + numrawimg]);

    char command[1000];
    sprintf(command, "./executables/raw2jpg.out 3 %s %s %s %d %d out.jpg", argv[2], argv[3], argv[4], width, height);
    //cout << command << endl;
    system(command);

    cvNamedWindow("Full Image", 1);
    IplImage *org = cvLoadImage("out.jpg");
    IplImage *resized = cvCreateImage(cvSize(org->width/SCALE, org->height/SCALE), 8, 3);
    cvResize(org, resized);
    cvSaveImage("out.jpg", resized);
    cvReleaseImage(&org);cvReleaseImage(&resized);
    
    g=new GUI((char *)"out.jpg", (char *)"Full Image");
    g->showImage();    
    cvSetMouseCallback("Full Image",on_mouse, 0);
    g->showImage();

    FILE* fp;
    float r=0;
    int c=0;
    do{
       switch ((char) c) {
       case '\x1b':
	  cout << "Exiting ..." << endl;
	  exit(0);
       case 'r':
	  cout << endl;
	  g->reset();
	  g->showImage();
	  break;
       case 'n':
       case 'N': 
       case 10:
	  //cvDestroyWindow(winName.c_str());
	  char s[1000];
	  int k;
	 
	  sprintf(s, "./segmentation.sh");
	  sprintf(s, "%s %d \'%s\' %d %d", s, numrawimg, rawfiles, width, height);
	  sprintf(s, "%s %d %d %d \'", s, g->cutImg_s.width*SCALE, g->cutImg_s.height*SCALE, g->nMove-1);
	  sprintf(s, "%s%d %d %d", s, g->mv_points[0].p.x*SCALE, g->mv_points[0].p.y*SCALE, 0);
	  double x1,x2,y1,y2;
	  fp = fopen("control", "w");
	  fprintf(fp, "%d\n%d %d\n0 0 0\n", g->nMove ,g->cutImg_s.width*SCALE, g->cutImg_s.height*SCALE);
	  for (int i = 1; i < g->nMove; i++) {
	     r=findDist(g->mv_points[i].p, g->mv_points[i-1].p);
	     sprintf(s, "%s %f %f", s, r*SCALE, g->mv_points[i].angle - g->mv_points[i-1].angle);
	     fprintf(fp, "%d ", (g->mv_points[i].p.x - g->mv_points[i-1].p.x)*SCALE);
	     fprintf(fp, "%d ", (g->mv_points[i].p.y - g->mv_points[i-1].p.y)*SCALE);
	     fprintf(fp, "%f\n",g->mv_points[i].angle - g->mv_points[i-1].angle);
	  }
	  fclose(fp);
	  sprintf(s, "%s\'", s);
	  //printf("command: %s\n", s);
	  cout<<"Enter no of particles: ";
	  cin>>numParticles;
	  cout<<"Enter error: ";
	  cin>> error;
	  system(s);
	  flag=1;

	   case 'p':
       case 'P':
       	  if(flag==0)
	  { 	
		  cout<<"Enter no of particles: ";
		  cin>>numParticles;
		  cout<<"Enter error: ";
		  cin>> error;
	  }
	  sprintf(s,"./executables/run.out results/ %d %d < control", error, numParticles);
	  system(s);
       }
    }    
    while((c=cvWaitKey(0))!=27);
    
 exit_main:
    cvDestroyWindow("Full Image");
    return (EXIT_SUCCESS);
}
