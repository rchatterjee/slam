/************************************************************
 Program # run.cpp
 Work		#	runs the whole particleMain and Map building files
 Authur 	#	RChat
 Date 		# 13th August, 2010
 ____________________________________________________________
 dependecies:
 particleMain <executable>
 ....its dependencies
 map <executable>
 ....its dependencies

 Control File should contain:
 num of images
 width height
 0 0 0
 <control info-1>[i.e. xx yy theta]
 <control info-2>
 .
 .
 .
 <control info-(num_img-1)>
 folders name:
 e.g classified/, abc/ [ending slash is important]
 **********************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<fstream>

using namespace std;


int main(int argc, char *argv[]) {
	int n, height, width;
	float r, th, x, y;
	if (argc < 2) {			// Print usage on wrong no of parameters being passed
		printf(
				"Usage: %s <folder's name> <error> <num Particle> \n",
				argv[0]);
		exit(0);
	}
	FILE *control_fp = fopen("control", "r");
	
	float error = atof(argv[2]);				
	int numParticle = atoi(argv[3]);
	fscanf(control_fp, "%d", &n);
	fscanf(control_fp, "%d%d", &width, &height);
	char call[100];
	sprintf(call, "./executables/reset.out %d", numParticle);
	system(call);
	system("rm thetaInfo.txt");

	double dh;  
	printf("Running SLAM:...");
	for (int i = 0; i < n; i++) {
		fscanf(control_fp, "%f%f%f", &x, &y, &th);
		printf(".");
		fflush(stdout);
		sprintf(call,
				"./executables/particleMain.out %sout%d.asc %d %d %d %f %f %f %d",
				argv[1], i + 1, width, height, numParticle, x, y, th, i+1);
		printf("%s\n", call);
		system(call);
		ifstream fPart("maxWPar.txt");
		int tPart;
		fPart>>tPart;
		fPart.close();

		sprintf(call, "./executables/map.out %d %d %d %d", width, height,
					numParticle, tPart);
		system(call);
		sprintf(call, "mv out.ppm at%d.ppm", i + 1);
		system(call);
	}
	fclose(control_fp);

	system("cat thetaInfo.txt | awk '{print $1, $2}' > particle_points; tail -n +3 control | awk '{x=x+$1; y=y+$2; print x,y}' > tempfile; tail -n +3 realpath | awk '{p=p+$1; q=q+$2; print p,q}' > tempfile2");

	FILE *f_r = fopen("tempfile2", "r");
	FILE *f_er = fopen("tempfile", "r");
	FILE *f_sr = fopen("slampath_global", "r");
	FILE *f_pr = fopen("particle_points", "r");
	FILE *f_ew = fopen("rel_error", "w");
	FILE *f_sw = fopen("rel_slampath", "w");
	FILE *f_pw = fopen("rel_particle_points", "w");

	double x_g=0.0, y_g=0.0, x_s=0.0, y_s=0.0, x_e=0.0, y_e=0.0, x_p=0.0, y_p=0.0;
	double x_g0, y_g0, x_s0, y_s0, x_e0, y_e0, z_dummy;
	for (int i=0; i<n; i++){
		x_g0=x_g; y_g0 = y_g; x_s0=x_s; y_s0=y_s; x_e0=x_e; y_e0=y_e;
		fscanf(f_r, "%lf %lf", &x_g, &y_g);
		fscanf(f_sr, "%lf %lf %lf", &x_s, &y_s, &z_dummy);
		fscanf(f_er, "%lf %lf", &x_e, &y_e);
		fprintf(f_ew, "%lf %lf\n", x_e - (x_e0 - x_g0), y_e - (y_e0 - y_g0));
		fprintf(f_ew, "%lf %lf\n", x_g, y_g);
		fprintf(f_sw, "%lf %lf\n", x_s - (x_s0 - x_g0), y_s - (y_s0 - y_g0));
		fprintf(f_sw, "%lf %lf\n", x_g, y_g);
		for (int j=0; j<numParticle; j++){
		  	fscanf(f_pr, "%lf %lf", &x_p, &y_p);
		  	fprintf(f_pw, "%lf %lf\n", x_p - (x_s0 - x_g0), y_p - (y_s0 - y_g0));
		}
	}
		  
	fclose(f_r); fclose(f_sr); fclose(f_pr); fclose(f_sw); fclose(f_pw); fclose(f_er); fclose(f_ew);
		  
//		  system("echo \"set size ratio -1; set xtics 50; set ytics 50; plot \\\"<awk '{print \\$1,-\\$2}' rel_error\\\" using 1:2 title \\\"Path with //error\\\" with lines, \\\"<awk '{print \\$1,-\\$2}' tempfile2\\\" using 1:2 title \\\"Real Path\\\" with linespoints,  \\\"<awk '{print \\$1,-\\$2}' rel_slampath\\\" using 1:2 title \\\"SLAM Path\\\" with linespoints, \\\"<awk '{print \\$1,-\\$2}' rel_particle_points\\\" using 1:2 title \\\"Particle Points\\\" with points\" | gnuplot -persist");
		  
	system("echo \"set size ratio -1; set xtics 50; set ytics 50; plot \\\"<awk '{print \\$1,-\\$2}' tempfile\\\" using 1:2 title \\\"Path with error\\\" with lines, \\\"<awk '{print \\$1,-\\$2}' tempfile2\\\" using 1:2 title \\\"Real Path\\\" with linespoints,  \\\"<awk '{print \\$1,-\\$2}' slampath_global\\\" using 1:2 title \\\"SLAM Path\\\" with linespoints, \\\"<awk '{print \\$1,-\\$2}' particle_points\\\" using 1:2 title \\\"Particle Points\\\" with points\" | gnuplot -persist");
	system("for i in *.ppm; do convert $i {$i/ppm/jpg}; done; rm *.ppm");	  
	printf("...Complete\n");
	return 0;
}
