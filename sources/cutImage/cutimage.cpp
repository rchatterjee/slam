#include "cv.h"
#include "highgui.h"
#include "math.h"
#include<stdio.h>
#include<stdlib.h>

/* Inputs

<num raw images> <raw images> <width< <height> <widthof output images>
<height of output images> <noise matrix ( only xx and yy and theta_theta is considered)>
<no. of points of path> <initial position( x , y , theta )> <list of displacements> 
*/
// width height xcentre ycentre angle
int main( int argc, char** argv )
{
    if(argc<9)
    {
		printf("<num raw images> <raw images> <width< <height> <widthof output images><height of output images> <noise matrix ( only xx and yy and theta_theta is 				considered)><no. of points of path> <initial position( x , y , theta )> <list of displacements> \n");
		exit(0);
	}	
  	//  IplImage* src = cvLoadImage(argv[1], 1);    
  	//  IplImage* dst = cvCloneImage( src );
	int numImages=atoi(argv[1]);
	int width=		atoi(argv[numImages+2]);
	int height=		atoi(argv[numImages+3]);
	int cutwidth=		(int)(atoi(argv[numImages+4]));
	int cutheight=		(int)(atoi(argv[numImages+5]));
	float noisexx=		(float)(atof(argv[numImages+6]));
	float noiseyy=		(float)(atof(argv[numImages+7]));
	float noisetheta=	(float)(atof(argv[numImages+8]));
	int numPoints = 	(int)atoi(argv[numImages+9]);
	int xcentre=		(int)(atoi(argv[numImages+10]));
	int ycentre=		(int)(atoi(argv[numImages+11]));
	float angle = 		(float)(atof(argv[numImages+12]));
	float *r = (float*)malloc((numPoints+1)*sizeof(float));
	float *theta = (float*)malloc((numPoints+1)*sizeof(float));
	
	
	r[0] = 0.0;
	theta[0] = 0.0;
	for(int i=0;i<numPoints;i++)
	{
		r[i+1] = (float)(atof(argv[numImages+13+2*i]));
		theta[i+1] = (float)(atof(argv[numImages+13+2*i+1]));
	}
	int delta = 1;
  				//  angle =(int)(angle + delta) % 360;
        	           	// 0:  rotate only
	
 	for(int numPic=0;numPic<=numPoints;numPic++)
	{
		angle += theta[numPic];
		xcentre = xcentre + r[numPic] * cos(angle);
		ycentre = ycentre + r[numPic] * sin(angle);
		//printf("\nx:%d, y:%d\n", xcentre, ycentre);
		for(int fn=0;fn<numImages;fn++)
		{ 
			
			FILE *f= fopen(argv[2+fn],"rb");
			IplImage* dst  = cvCreateImage(cvSize(width,height),IPL_DEPTH_8U,1);
			height     = dst->height;
			width      = dst->width;
			int step       = dst->widthStep/sizeof(uchar);
			uchar* data    = (uchar *)dst->imageData;
	
			for( int i=0;i<height;i++)
			for( int j=0;j<width;j++)	
				data[i*step+j] = fgetc(f);
			fclose(f);	
	
			f= fopen(argv[2+fn],"rb");
			IplImage* src  = cvCreateImage(cvSize(width,height),IPL_DEPTH_8U,1);
			height     = src->height;
			width      = src->width;
			step       = src->widthStep/sizeof(uchar);
			data    = (uchar *)src->imageData;
		
			for( int i=0;i<height;i++)
			for( int j=0;j<width;j++)	
				data[i*step+j] = fgetc(f);
			fclose(f);



    			double factor;
    			//cvNamedWindow("src", 0);
    			//cvShowImage("src", src);

    			float m[6];
    			CvMat M = cvMat(2, 3, CV_32F, m);
    	
        		factor = 1;
     			m[0] = (float)(factor*cos(-angle));
     			m[1] = (float)(factor*sin(-angle));
     			m[3] = -m[1];
     			m[4] = m[0];
     			m[2] = xcentre;  		// distance along width
     			m[5] = ycentre;  		// distance along height
        	
	     		cvGetQuadrangleSubPix( src, dst, &M);
	 	
	 		CvRect rect=cvRect(width/2-cutwidth/2, height/2-cutheight/2, cutwidth, cutheight); 
	 		cvSetImageROI( dst, rect);
	
			IplImage *dst2 = cvCreateImage(cvGetSize(dst),
                	               dst->depth,
                	               dst->nChannels);
			cvCopy(dst, dst2, NULL);
     	 		//height     = dst2->height;
	 		//width      = dst2->width;
	 		//printf("%d %d\n", height, width);
	 		step       	= dst2->widthStep/sizeof(uchar);
	 		data    	= (uchar *)dst2->imageData;
	 		char ch[20];
	 		sprintf(ch, "%d_%d.raw",numPic+1,fn+1);	
	 		f= fopen(ch,"wb");
	 		for( int i=0;i<dst2->height;i++)
	 		for( int j=0;j<dst2->width;j++)	
				fputc(data[i*step+j],f);
	 		//cvNamedWindow(ch, 0);
     			//cvShowImage(ch, dst);
     		cvReleaseImage(&src);
     		cvReleaseImage(&dst);     			
  		}
	
     	}
            
    	return 0;
}
