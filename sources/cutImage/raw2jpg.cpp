#include<stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include<iostream>

using namespace cv;
using namespace std;

void readImage(int ***raw_images, char **files, int width, int height, int n)
{
	int i,j,k;
	raw_images=new int**[n];
	FILE *fp;
	
	for(i=0;i<n;i++)
	{
		raw_images[i]=new int *[height];
		fp=fopen(files[i],"rb");
		for(j=0;j<height;j++){
			raw_images[i][j]=new int[width];
			for(k=0;k<width;k++)
				raw_images[i][j][k] = fgetc(fp);
		}
		fclose(fp);	
	}
	//delete []fp;
	return ;
}

int main(int argc, char *argv[])
{
  IplImage* img = 0; 
  int height,width,step,channels,n;
  uchar *data;
  int i,j,k;
	char **files;
  if(argc<4){
    printf("Usage: %s <num of raw files> <<raw files>> <width> <height> <output image-file-name>\n\7", argv[0]);
    exit(0);//
  }
	
	sscanf(argv[1],"%d",&n);
	files = new char*[n];
	for(i=0;i<n;i++){
			files[i]=new char[100];
			strcpy(files[i],argv[i+2]);
	}
	sscanf(argv[n+2],"%d",&width);
	sscanf(argv[n+3],"%d",&height);
	//cout<<"file[0]: "<<files[0]<<"\nwidth: "<<width<<"\nheight: "<<height<<"\n";
  // load an image  
  img = cvCreateImage(cvSize(width,height), IPL_DEPTH_32F, n);

	 // get the image data
/*height    = img->height;
  width     = img->width;
  step      = img->widthStep;
  channels  = img->nChannels;
 
  printf("Processing a %dx%d image with %d channels\n",height,width,channels); 
*/ 

	data      = (uchar *)img->imageData;
	int ***raw_images;

	/*readImage(raw_images,files,width,height,n);*/
	raw_images=new int**[n];
	FILE *fp;
	
	for(i=0;i<n;i++)
	{
		raw_images[i]=new int* [height];
		fp=fopen(files[i],"rb");
		if(!fp){
			printf("File Error\n");
			exit(0);
		}
		for(j=0;j<height;j++){
			raw_images[i][j]=new int[width];
			for(k=0;k<width;k++)
				raw_images[i][j][k] = fgetc(fp);
		}
		fclose(fp);	
	}

	channels=img->nChannels;
	step=img->widthStep;
	CvScalar s = cvScalar(0.0,0.0,0.0,0.0);
  for(i=0;i<height;i++) for(j=0;j<width;j++) 
  {
  	for(k=0;k<img->nChannels;k++)
  		s.val[k]=raw_images[k][i][j]%255;
    cvSet2D(img,i,j,s);
  }

  IplImage* nimg=cvCreateImage(cvSize(width,height), IPL_DEPTH_32F, n);
  cvCvtColor(img,nimg,CV_BGR2RGB);
  //cout<<"converted\n";
  char outFileName[100];
  strcpy(outFileName, argv[n+4]);
	if(!cvSaveImage(argv[n+4],img)) printf("Could not save: %s\n",outFileName);
	//if(!cvSaveImage(argv[n+4]+1,nimg)) printf("Could not save: %s\n",outFileName);
  // release the image
  cvReleaseImage(&img );
  return 0;
}
