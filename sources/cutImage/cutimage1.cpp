#include "cv.h"
#include "highgui.h"
#include "math.h"
#include<stdio.h>
#include<stdlib.h>

int c[16][3];
int lastPic = 10, totCount=0;
double dist(CvScalar a, int t){
	double dist = (a.val[0] - c[t][0])*(a.val[0] - c[t][0]);
	dist += (a.val[1] - c[t][1])*(a.val[1] - c[t][1]);
	dist += (a.val[2] - c[t][2])*(a.val[2] - c[t][2]);
	return dist;
}


int classOfPixel(CvScalar a){
	int tmin=0;
	double d, min=9999999;
  for(int i=0;i<14;i++){
  	d = dist(a, i);
  	if(d<min) {tmin = i; min = d;}
    if(min==0.0) return i;
  }
  return tmin;
}

int main( int argc, char** argv )
{
  int numclus = 14;
  FILE* fp1=fopen("colormap-final-14","rb");
  if(fp1){// cout<<"Color File read ERROR!!!!!!!!!!!!!!!"<<endl;
    for(int i=1;i<numclus+1;i++){
      for(int j=0;j<3;j++)
	c[i][2-j]=fgetc(fp1);
      printf("%d> %d %d %d\n", i, c[i][0],  c[i][1],  c[i][2]);
    }
    c[0][0] = c[0][1] = c[0][2] = 255;
  }
  if (argc<2){
    printf("<widthof output images> <height of output images> <no. of points of path> <initial position( x , y , theta )> <list of displacements> \n");
    exit(0);
  }	
  int cutwidth=		(int)(atoi(argv[1]));
  int cutheight=	(int)(atoi(argv[2]));
  int numPoints = 	(int)atoi(argv[3]);
  int xcentre=		(int)(atoi(argv[4]));
  int ycentre=		(int)(atoi(argv[5]));
  float angle = 	(float)(atof(argv[6]));
  float *r = 		(float*)malloc((numPoints+1)*sizeof(float));
  float *theta = 	(float*)malloc((numPoints+1)*sizeof(float));
  r[0] = 0.0;
  theta[0] = 0.0;
  printf("%d--xcenter:%d\n", numPoints, xcentre);
  for(int i=0;i<numPoints;i++)
    {
      r[i+1] = (float)(atof(argv[7+2*i]));
      theta[i+1] = (float)(atof(argv[7+2*i+1]));
    }
  int delta = 1, height, width;
  //  angle =(int)(angle + delta) % 360;
  // 0:  rotate only
  FILE *f;
  for(int numPic=0;numPic<=numPoints;numPic++){
    angle += theta[numPic];
    xcentre = xcentre + r[numPic] * cos(angle);
    ycentre = ycentre + r[numPic] * sin(angle);
    //printf("\nx:%d, y:%d\n", xcentre, ycentre);
    IplImage* src  = cvLoadImage("images/FullImage.ppm");
    IplImage *dst = cvCloneImage(src);
    height     = src->height;
    width      = src->width;
    float m[6];
    CvMat M = cvMat(2, 3, CV_32F, m);  
    m[0] = (float)cos(-angle);
    m[1] = (float)sin(-angle);
    m[3] = -m[1];
    m[4] = m[0];
    m[2] = xcentre;  		// distance along width
    m[5] = ycentre;  		// distance along height
	  
    cvGetQuadrangleSubPix( src, dst, &M);  //to rotate	  
    CvRect rect=cvRect(width/2-cutwidth/2, height/2-cutheight/2, cutwidth, cutheight); 
    cvSetImageROI( dst, rect);
    IplImage *dst2 = cvCreateImage(cvGetSize(dst),
				   dst->depth,
				   dst->nChannels);
    cvCopy(dst, dst2, NULL);
    char ch[20];
    sprintf(ch, "results/out%d.asc",numPic+1);
    f = fopen(ch,"w");
    for( int i=0;i<dst2->height;i++){
      for( int j=0;j<dst2->width;j++)
	fprintf(f, "%d ", classOfPixel(cvGet2D(dst, i,j)));
      fprintf(f, "\n");
    }
    fclose(f);
    char cmd[200];
    sprintf(cmd, "./executables/convertor.out %s %d %d 15 0;mv out.ppm ./results/out%d.ppm", ch, dst2->width, dst2->height, numPic+1);
    system(cmd);
    cvReleaseImage(&dst2);
    cvReleaseImage(&dst);
  }
  printf("\n>>>>>>>>>>>Total Number of unlabeled pixels: %d\n", totCount);
return 0;
}
