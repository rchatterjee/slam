#include<stdio.h>
#include<stdlib.h>
#include<iostream>
using namespace std;

int min( int a, int b, int c)
{
	if(a<b && a<c)
		return a;
	if(b<c)
		return b;
	return c;		
}


int max( int a, int b)
{
	return a>b?a:b;
	
}
int tree1[600], tree2[600], TD[600][600];

float getTreeDistance( int *t1, int len1, int *t2, int len2, int partition_cost )
{

	FILE *g=fopen("LUT.txt","r");
	int ND[14][14];
	int i,j;
	for(i=0;i<14;i++)
	{
		for(j=0;j<14;j++)
			fscanf(g,"%d",&ND[i][j]);
			//ND[i][j]=i==j?0:1;
	}		
	fclose(g);


	for( i=0;i<len1-1;i++)
	{
		tree1[i]=t1[i+1];
		tree1[i+len1-1]=t1[i+1]; 
	}	

	for( i=0;i<len2-1;i++)
	{
		tree2[i]=t2[i+1];
		tree2[i+len2-1]=t2[i+1]; 
	}	
	
	for( i=0;i<(2*len1-1);i++)
	{
		TD[i][0]=0;
	}	

	for( i=0;i<(2*len2-1);i++)
		TD[0][i]=0;
	
	for( i=1;i<=2*len1-2;i++)
	for( j=1;j<=2*len2-2;j++)
	{
	//	printf("i=%d j=%d %d %d\n",i,j,tree1[i-1],len1);
		if(tree1[i-1]==tree2[j-1])
		  TD[i][j]=TD[i-1][j-1]+1;
		else
		  TD[i][j]=max(TD[i-1][j], TD[i][j-1] );
	}
	if( t1[0] != t2[0])
		return 0.0;
	//cout<< (TD[2*len1-2][2*len2-2]*1.0)/max( 2*len1-2, 2*len2-2)<<endl;
	return (TD[2*len1-2][2*len2-2]*1.0)/max( 2*len1-2, 2*len2-2);
	
}	
 
 
 
