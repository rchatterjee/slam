#define PI  3.14159265
#define EXP 2.71828183
#include<stdio.h>
class Matrix
{
	public:
	int nrows, ncols;
	float value[3][3];
	

	Matrix(int n_rows, int n_cols)
	{
		nrows = n_rows;
		ncols = n_cols;
	}
	Matrix()
	{
		nrows = 3;
		ncols = 3;
	}	
	Matrix( int m, int n, int a)
	{
		nrows = m;
		ncols = n;
		for(int i=0;i<m;i++)
			for(int j=0;j<n;j++)
				value[i][j]=(i==j)?a:0;		
	}
	
	friend Matrix operator+ ( Matrix A, Matrix B)
	{
		if(A.nrows!=B.nrows || A.ncols!=B.ncols)
			printf("CHAOS in ADD <%d,%d><%d,%d>\n", A.nrows, A.ncols, B.nrows, B.ncols );
		Matrix C( A.nrows, A.ncols);
		for(int i=0;i<A.nrows;i++)
		{
			for(int j=0;j<A.ncols;j++)
				C.value[i][j]=A.value[i][j]+B.value[i][j];
		}		
		return C;
	}	
			
	friend Matrix operator- (Matrix A, Matrix B)
	{
		if(A.nrows!=B.nrows || A.ncols!=B.ncols)
			printf("CHAOS in SUB\n");
		Matrix C( A.nrows, A.ncols);
		for(int i=0;i<A.nrows;i++)
		{
			for(int j=0;j<A.ncols;j++)
				C.value[i][j]=A.value[i][j]-B.value[i][j];
		}		
		return C;
	}	
	
	friend Matrix operator* (Matrix A, Matrix B)
	{
		if(A.ncols!=B.nrows)
			printf("CHAOS in MULT<%d,%d><%d,%d>\n", A.nrows, A.ncols, B.nrows, B.ncols );
		Matrix C( A.nrows, B.ncols);
		for(int i=0;i<C.nrows;i++)
		{
			for(int j=0;j<C.ncols;j++)
			{
				C.value[i][j]=0;
				for(int k=0;k<A.ncols;k++)
					C.value[i][j]+=A.value[i][k]*B.value[k][j];
			}
		}
		return C;
	}		

	public:
	Matrix transpose();
	Matrix inverse3Mat();
	Matrix inverse2Mat();
	Matrix inverse();
	float det();
	void print();
	void g( Matrix, Matrix);
	void inv_g( Matrix, Matrix);
	void sample( Matrix, Matrix);
};
/*	
	Matrix operator=(Matrix A)
	{
		for(int i=0;i<A.nrows;i++)
		for(int j=0;j<A.ncols;j++)
			value[i][j]=A.value[i][j];
		return *this;
	}
	
};
*/

	
		
	
	Matrix Matrix::transpose()
	{
		Matrix A( ncols, nrows);
		for(int i=0;i<ncols;i++)
		{
			for(int j=0;j<nrows;j++)
				A.value[i][j]=value[j][i];
		}		
		return A;
	}


	Matrix Matrix::inverse3Mat ()
	{
		Matrix A(3,3);
		float determinant= det();
		
		A.value[0][0]=(value[1][1]*value[2][2]-value[1][2]*value[2][1])/determinant; 
		A.value[0][1]=(value[0][2]*value[2][1]-value[0][1]*value[2][2])/determinant; 
		A.value[0][2]=(value[0][1]*value[1][2]-value[0][2]*value[1][1])/determinant; 
		A.value[1][0]=(value[1][2]*value[2][0]-value[1][0]*value[2][2])/determinant; 
		A.value[1][1]=(value[0][0]*value[2][2]-value[0][2]*value[2][0])/determinant; 
		A.value[1][2]=(value[0][2]*value[1][0]-value[1][2]*value[0][0])/determinant; 
		A.value[2][0]=(value[1][0]*value[2][1]-value[1][1]*value[2][0])/determinant; 
		A.value[2][1]=(value[0][1]*value[2][0]-value[0][0]*value[2][1])/determinant; 
		A.value[2][2]=(value[1][1]*value[0][0]-value[1][0]*value[0][1])/determinant; 
		
		return A;

	}

	Matrix Matrix::inverse2Mat ()
	{
		Matrix A(2,2);
		float determinant= det();
		
		A.value[0][0]=value[1][1]/determinant; 
		A.value[0][1]=(-1)*value[0][1]/determinant; 
		A.value[1][0]=(-1)*value[1][0]/determinant; 
		A.value[1][1]=value[0][0]/determinant;
		
		return A;
	}

	Matrix Matrix::inverse()
	{
		if( nrows ==2)
			return inverse2Mat();
		else
			return inverse3Mat();

	}

	
	
	float Matrix::det()
	{
		float d=0;
		if(nrows==3)
		{
			d+=value[0][0]*(value[1][1]*value[2][2]-value[1][2]*value[2][1]);
			d-=value[0][1]*(value[1][0]*value[2][2]-value[1][2]*value[2][0]);
			d+=value[0][2]*(value[1][0]*value[2][1]-value[1][1]*value[2][0]);
		}
		if(nrows==2)
		{
			d+=value[0][0]*value[1][1]-value[0][1]*value[1][0];
		}
		if(nrows==1)
			d=value[0][0];
			
		return d;
	}	
	
	void Matrix::print()
	{
		for(int i=0;i<nrows;i++)
		{
			for(int j=0;j<ncols;j++)
				printf("%f ",value[i][j]);
			printf("\n");
		}
		printf("\n");
	}		



	void Matrix::g( Matrix expected_pose, Matrix landmarkmean )
	{
      value[0][0]= landmarkmean.value[0][0]-expected_pose.value[0][0]; 
      value[1][0]= landmarkmean.value[1][0]-expected_pose.value[1][0]; 
   
   	}	
   
	void Matrix::inv_g( Matrix expected_pose, Matrix z )
   	{	
      value[0][0] = expected_pose.value[0][0] + z.value[0][0];
      value[1][0] = expected_pose.value[1][0] + z.value[1][0];
      
   	}				
		
	void Matrix::sample ( Matrix mean, Matrix variance)
	{
		float u=(rand()%1000+1)/1000.0;
		float v=(rand()%1000+1)/1000.0;
		float x= sqrt( -2*log(u)) * cos (2*PI*v);
		value[0][0]=mean.value[0][0]+ x*sqrt(variance.value[0][0]);

		u = (rand()%1000+1)/1000.0;
		v=(rand()%1000+1)/1000.0;
		x= sqrt( -2*log(u)) * cos (2*PI*v);
		value[1][0]=mean.value[1][0]+ x*sqrt(variance.value[1][1]);

		u=(rand()%1000+1)/1000.0;
		v=(rand()%1000+1)/1000.0;
		x= sqrt( -2*log(u)) * cos (2*PI*v);		
		value[2][0]=mean.value[2][0]+ x*sqrt(variance.value[2][2]);
	}	
	





	
