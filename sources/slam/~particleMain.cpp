#include<stdio.h>
#include<stdlib.h> 
#include "particle.cpp"
#include<iostream> 
#include<fstream>

#define PI 3.14159265
using namespace std;

// sprintf(call, "./particleMain %sout%d.asc %d %d %d %f %f", argv[1],i+1, width, height, numParticle, x, y, th); 

int main(int argc, char **argv)
{
	if(argc<6){
		printf("USAGE: %s <image name> <width> <height> <numParticle> <x> <y> <th>\n", argv[0]);
		exit(0);
	}

	getPicInfo(argc,argv); 
	int M,num_obs=0;
	int width = atoi(argv[2]);
	int height = atoi(argv[3]);
	FILE *f=fopen("picInfo.txt","r");
	Landmark *obs=new Landmark[1000];
	int i, tmp1, tmp2;

	while(!feof(f))
	{
		fscanf(f,"%f",&obs[num_obs].mean.value[0][0]);
		fscanf(f,"%f",&obs[num_obs].mean.value[1][0]);	
		fscanf(f,"%d",&obs[num_obs].rtlen);

		if(obs[num_obs].rtlen>0)
		{
			obs[num_obs].relationtree=new int[obs[num_obs].rtlen];
			for(i=0;i<obs[num_obs].rtlen;i++)
				fscanf(f,"%d",&obs[num_obs].relationtree[i]);
			
		}
		num_obs++;
	}		 

	num_obs--;
	fclose(f);
	
	
	Matrix u(3,1);
	M=atoi(argv[4]);
	sscanf(argv[5],"%f",&u.value[0][0]);
	sscanf(argv[6],"%f",&u.value[1][0]);
	sscanf(argv[7],"%f",&u.value[2][0]);
	int num_runs = atoi(argv[8]);
	char fileName[100];
	
	Particle *p=new Particle[M];

	for( i=0;i<M;i++)
	{
		sprintf(fileName,"%s%d.asc", "particle", i);
		f=fopen(fileName, "r");				//opens particle<i>.asc
	    fscanf(f, "%lf", &p[i].w);			// particle weight
	    fscanf(f, "%d", &p[i].n_poses);		// number of poses in the particle
	    p[i].pose_history = new Matrix[p[i].n_poses];
	    for (int j=0; j<p[i].n_poses; j++){
			p[i].pose_history[j].nrows=3;
			p[i].pose_history[j].ncols=1;
			//cout<<"-----------------------------------------------------------------------HISTORY\n";
			for (int k=0; k<3; k++){
				fscanf(f, "%f", &p[i].pose_history[j].value[k][0]);
				//cout<<"----------------------------------------------------------------------"<<p[i].pose_history[j].value[k][0]<<endl;
			}	
	    }	
	    if (p[i].n_poses !=0) p[i].pose = p[i].pose_history[p[i].n_poses-1];
	    else
		for(int j=0;j<3;j++) fscanf(f,"%f",&p[i].pose.value[j][0]);	// particle mean pose
	    fscanf(f,"%d",&p[i].N);				// num of landmark
      	
	    p[i].landmark=new Landmark[(p[i].N+num_obs)];
      	
	    for(int j=0;j<p[i].N;j++)			//for all landmarks
		{ 
	  
		    for(int k=0;k<2;k++) fscanf(f,"%f",&p[i].landmark[j].mean.value[k][0]);		//landmark mean
		    for(int l=0;l<2;l++){		        //land mark variance
			for(int k=0;k<2;k++){
			    fscanf(f,"%f",&p[i].landmark[j].variance.value[l][k]);
			}
		    }
		    	
		    fscanf(f, "%d", &p[i].landmark[j].rtlen);
		    p[i].landmark[j].relationtree   =   new int[(p[i].landmark[j].rtlen)];
		    for(int k=0;k < p[i].landmark[j].rtlen; k++)
				fscanf(f,"%d",&p[i].landmark[j].relationtree[k]);
    // 		nfile = number of file where the landmark is present
	//		ifinfo = int [][4], -->[fileno][x][y][theta]
	//	    fscanf(f,"%d",&p[i].landmark[j].nfile);
	  
	//	    for(int k=0;k<p[i].landmark[j].nfile; k++) // Read the landmarks
	//		for(int l=0;l<3;l++) 
	//		    fscanf(f,"%d",&p[i].landmark[j].ifinfo[k][l]);
	  
		}
			
	    fclose(f); 
	}
	
	Matrix R(2,2);
	for(i=0;i<2;i++)
	{
		for(int j=0;j<2;j++)
		if(i==j)
			R.value[i][j]=10.0;
		else
			R.value[i][j]=0.0;
	}	


//Here allocating space for N
//no need to do so in relationTreecheck
//first get maximum number of landmarks for any particle
	
	int max_num_land=0;
	for( i=0;i<M;i++)
	  if(max_num_land<p[i].N)
	    max_num_land=p[i].N;
	
	candiPr **N = new candiPr*[num_obs];

	f = fopen("thetaInfo.txt","a");
	for(i=0;i<num_obs;i++)
	  N[i]=new candiPr[max_num_land+1];
	
	float x=0.0,y=0.0,r,th,tmp;
	int is_first_run = 0;
	if (num_runs==1) is_first_run = 1;
	int maxP=0;
	
	if (num_runs>1) p=resampling(p,M);
	printf("\nResampling done\n");
	
	printf("_____INFO_____\n");
	for( i=0;i<M;i++)
	{
		th=p[i].pose.value[2][0]+u.value[2][0];
		for( int j=0;j<num_obs;j++)
		{
			tmp = obs[j].mean.value[0][0]; 
			obs[j].mean.value[0][0]= tmp*cos(th)-obs[j].mean.value[1][0]*sin(th);
			obs[j].mean.value[1][0]= tmp*sin(th)+obs[j].mean.value[1][0]*cos(th);	
		}
		p[i].pose.nrows=2;
		u.nrows=2;
		relationTreeCheck( &obs, num_obs, &p[i], &N);
		updateParticle( &p[i], u, R, &obs, num_obs, N);	
		p[i].pose.nrows=3;
		u.nrows=3;
		p[i].pose.value[2][0] = th;
		fprintf(f,"%f %f %f\n", p[i].pose.value[0][0], p[i].pose.value[1][0], p[i].pose.value[2][0]);
		printf("Pose: %f %f %f\n", p[i].pose.value[0][0], p[i].pose.value[1][0], p[i].pose.value[2][0]);
		if(p[maxP].w<p[i].w) maxP=i;
		
	}
	//fclose(f);
	
	printf("Maximum weight particle is %d\n", maxP);  
	//writing the max weight Particle
	ofstream fParticle("maxWPar.txt");
	fParticle<<maxP<<endl;
	//fParticle.close();
    


	//delete []obs;
	

	//Update the files ParticlesInfo.txt
	for( i=0;i<M;i++)
	{
		sprintf(fileName,"particle%d.asc", i);
		f=fopen(fileName, "w");																	//opens particle<i>.asc

		fprintf(f, "%g\n", p[i].w);			// particle weight
	    fprintf(f, "%d\n", p[i].n_poses+1);		// number of poses in the particle
	    for (int j=0; j<p[i].n_poses; j++){
		for (int k=0; k<3; k++){
		    fprintf(f, "%f ", p[i].pose_history[j].value[k][0]);
		}	
		fprintf(f, "\n");
	    }
	    for(int j=0;j<3;j++) 
		fprintf(f,"%f ",p[i].pose.value[j][0]);		                                // particle mean pose
      
      
	    fprintf(f,"\n%d\n",p[i].N);				                                        // num of landmark
	    for(int j=0;j<p[i].N;j++)			                                            //for all landmarks
		{ 
		    for(int k=0;k<2;k++) fprintf(f,"%f ",p[i].landmark[j].mean.value[k][0]);    //landmark mean
		    for(int l=0;l<2;l++){
				fprintf(f, "\n");				                                        //land mark variance
				for(int k=0;k<2;k++)
					fprintf(f,"%f ",p[i].landmark[j].variance.value[l][k]);
			}
		    fprintf(f,"\n%d\n",p[i].landmark[j].rtlen);
		    for(int k=0;k < p[i].landmark[j].rtlen; k++)
			fprintf(f,"%d ",p[i].landmark[j].relationtree[k]);
	  
//		    fprintf(f,"\n%d\n",p[i].landmark[j].nfile);
	  
//		    for(int k=0;k<p[i].landmark[j].nfile; k++){
//			for(int l=0;l<3;l++)fprintf(f,"%d ",p[i].landmark[j].ifinfo[k][l]);
//			fprintf(f, "\n");
//		    }
		}
	    fclose(f);
	}

	return 0;
}
