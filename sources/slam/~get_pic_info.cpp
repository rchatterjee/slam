#include<stdio.h>
#include<stdlib.h>
#include<iostream>
using namespace std;
int  **pic,**pictemp;
int IsComplete;
int FILE_NO=1;
class pixel
{
	public:int x,y;
};

class region
{
	public:
	float xmean,ymean;
	int pixelcount;
	pixel *Pixel;
	int pcount;
	pixel *Perimeter;
	int rtlen;
	int *relationtree;
	int nrows,ncols;
	int *status;
	public:
	region(int,int);
	~region();
	void findRegion(int x,int y,int label);
	void calcMean();
	int isNeighbour(pixel,pixel);
	int findRelationTree();

};

region::region(int m, int n)
{
	pixelcount=0;
	pcount=0;
	nrows=m;
	ncols=n;
	Pixel=(pixel*)malloc(m*n*sizeof(pixel));
	Perimeter=(pixel*)malloc(m*n*sizeof(pixel));
	status=new int[m*n];
}

region::~region()
{
	free(Pixel);
	free(Perimeter);
	free(status);
}

void region::findRegion( int x, int y, int label)
{
	if(y<=0 || x<=0 ||y>=nrows-1||x>=ncols-1)	
	{
		IsComplete=0;
		return;
	}	

	if(pictemp[y][x]!=label)
	{
	//	printf("pcount=%d",pcount);
		if(pic[y][x]!=label)
		{
			for( int i=0;i<pcount;i++)
				if(Perimeter[i].x==x && Perimeter[i].y==y)
					return;
			Perimeter[pcount].x=x;
			Perimeter[pcount++].y=y;
		}	
		return;
	}	
	
	pictemp[y][x]=-1;

	Pixel[pixelcount].x=x;
	Pixel[pixelcount++].y=y;

	findRegion(x+1,y,label);
	findRegion(x+1,y+1,label);
	findRegion(x,y+1,label);
	findRegion(x-1,y+1,label);
	findRegion(x-1,y,label);
	findRegion(x-1,y-1,label);
	findRegion(x,y-1,label);
	findRegion(x+1,y-1,label);
}	

void region::calcMean()
{
	float x=0.0,y=0.0;
	for(int i=0;i<pixelcount;i++)
	{
		x+=Pixel[i].x;
		y+=Pixel[i].y;
	}
	xmean=x/pixelcount;	
	ymean=y/pixelcount;		
}	

int region::isNeighbour(pixel p1,pixel p2)
{
	if( p1.x-p2.x>=-1 && p1.x-p2.x<=1 && p1.y-p2.y>=-1 && p1.y-p2.y<=1)
	{
		if(p1.x==p2.x && p1.y==p2.y)
			return 0;
		else
			return 1;
	}
	return 0;
}				
	
int region::findRelationTree()
{
	int i,j,tmp,min,max;
	rtlen=1;
	relationtree=new int[(pcount+1)];
	
	for(i=0;i<pcount;i++)
		status[i]=0;
	
	i=0;           
    	for(int k=0;k<pcount;k++)
    	{
    		status[i]=1;
//    	printf("(%d)%d ",i,pic[Perimeter[i].y][Perimeter[i].x]);
    		for( j=0;j<pcount;j++)        
    		{
    			if(isNeighbour(Perimeter[i],Perimeter[j]) && !status[j])
    			{
    				if(relationtree[rtlen-1]!=pic[Perimeter[j].y][Perimeter[j].x])
    					relationtree[rtlen++]=pic[Perimeter[j].y][Perimeter[j].x];
    				if((relationtree[rtlen-1]==relationtree[1] && rtlen>2) || relationtree[rtlen-1]==0)
    					rtlen--;	
    				i=j;
    				break;
    			}
    		}		
    	}

    	if(rtlen<2)
    		return 0;
   	 else
    		return 1;	
    //printf("\n");			    

}

void getPicInfo(int argc, char **argv)
{
	FILE *f;
	int m,n,i,j;
	FILE *fp=fopen("string_table.asc","r+a");
	char ch=getc(fp);
	while( ch != EOF ) {
       if(ch=='\n') FILE_NO++;
       ch = getc( fp );
     }
  	fprintf(fp, "%s\n",argv[1]);
	fclose(fp);
	//cout<<"####FILE NO: "<<FILE_NO<<endl;

	n=atoi(argv[2]);
	m=atoi(argv[3]);
	
	f = fopen( argv[1],"r"); //DSLabels442x649.asc
	
	pictemp = new int*[m];
	
	for(i=0;i<m;i++)
		pictemp[i] = new int[n];
	
	pic = new int*[m];
	for(i=0; i<m; i++)
	{
		pic[i] = new int[n];
				
		for(j=0; j<n; j++)
		{
			fscanf(f,"%d",&pic[i][j]);
			pictemp[i][j]=pic[i][j];
		}
	}
	fclose(f);
	
	
	
	int label,landmark=0,p=0,t;
       
	f=fopen("picInfo.txt","w");
	
	region R=region(m,n);
	
	for(i=1;i<m-1;i++)
	for(j=1;j<n-1;j++)
		if(pictemp[i][j]!=-1)
		{
			
			IsComplete=1;
			R.pixelcount=0;
			R.pcount=0;
			label=pictemp[i][j];
				
			R.findRegion(j,i,label);
			
			if(IsComplete==0 || R.pixelcount<50)
				break;
		//	printf("i=%d j=%d\n",i,j);
			t=R.findRelationTree();	
		//	printf("i=%d j=%d\n",i,j);	
			if(!t)
				break;
					
			R.relationtree[0]=label; 
				
			R.calcMean();	
		//	printf(">>>>>>>>>>%f %f\n",R.xmean-(n-1)/2.0,R.ymean-(m-1)/2.0);
			fprintf(f,"%f %f\n%d\n",R.xmean-(n-1)/2.0,R.ymean-(m-1)/2.0,R.rtlen);
			for(int k=0;k<R.rtlen;k++)
			{
				fprintf(f,"%d ",R.relationtree[k]);
			}	
		
			landmark++;

		}
		
		
	for(i=0;i<m;i++)
	{
		delete []pic[i];
		delete []pictemp[i];
	}
	
	delete []pic;
	delete []pictemp;
	
	
			
	fclose(f);

	//printf("No of landmarks:%d\n",landmark);	

}	

