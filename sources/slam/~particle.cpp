#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<assert.h>
#include "relation_tree.cpp"
#include "get_pic_info.cpp"
#include "matrix.cpp"

#include <iostream>
using namespace std;

////////////////////Update of a Particle///////////////////////////////////
/*
*expected_pose         st<hat>
*pose_variance         initial error in control information (sigma in paper)
*num_of_obs            no of closed segments observed
*expected_obs          zt<hat>
*G_map                 Jacobian wrt theta  
*G_pose                Jacobian wrt theta 
*N[][]                 N[i][0] contains the number of candidate landmarks
                       followed by the N[i][0] indices 
*w                     weight associated with particle, used in resampling  
*mean,cov              mean and covariance of proposal distribution                                                    
*proposal /R             covariance of initial proposal distribution    
*z                     observation            
*p                     probability (% match)  
*highestmatch_prob     %match for maximum match of landmark
*highestmatch_loc      location of the maximum matched landmark
*p0                    threshold value of probability       
*relation tree comparison to be done for every particle separately.
 So, for every particle, we will have comparisons with RAG of landmarks in that particular particle.
 Changed part not applied yet.	


*Additional non parameterized input required for updateParticle()
-----num_of_obs, **N (for candidate observations)----------		


*/
/////////////////////Structures used/////////////////////////////////////

#define PI  3.14159265
#define EXP 2.71828183
#define THRESHOLD_td 0.5
#define P_ZERO 0.00001
// class to store landmarks

class Landmark
{
public:
  Matrix mean;								// mean of landmark
  Matrix variance;							// variance associated with landmark pose
  int rtlen;								// relation tree length
  int *relationtree;						// the relation tree		
  int nfile, ifinfo[100][3];				//'nfile' number of file where the landmark is present

  Landmark(){
		mean.nrows = 2;
		mean.ncols = 1;
		variance.nrows = 2;
		variance.ncols = 2;
		nfile=0;return ;
	}

  void loadinfo(int fileno, Matrix p){
    ifinfo[nfile][0]=fileno;
    ifinfo[nfile][1]=p.value[0][0];
    ifinfo[nfile][2]=p.value[1][0];
    nfile++;
    return ;
  }
														//'ifinfo' is the land mark info corresponding to the image file
};


// class to store particles
class Particle
{
	public:
	int N;					// number of landmarks
	Matrix pose;				// present pose of robot
	Landmark *landmark;			// landmarks observed 
	double w;				// weight associated with the particle
	int n_poses;
	Matrix *pose_history; 

	Particle(){
		pose.nrows = 3;
		pose.ncols = 1;
	}

	~Particle()
	{
		delete []landmark;
	}	

};	

typedef struct _candi
{
	int match;
	float matchPr;
}candiPr;


		
////////////////Particle Update////////////////////////////////////


// This function takes observation set, checks all the landmarks seen upto the previous level
// Prepares a list of candidates for each of the observation
void print_rttree(int *t, int len){
	for(int i=0;i<len;i++)
		printf("%d ", t[i]);
	printf("\n");
}

void relationTreeCheck( Landmark **obs, int num_obs, Particle *p ,candiPr ***candidates)
{
	float *td = new float[p->N];
	float sum=0.0;
	int partition_cost=1;
	
	for( int i=0;i<num_obs;i++)
	{	
		(*candidates)[i][0].match=0;
		sum=0.0;
		
		for(int j=0;j<p->N;j++)
		{
		  td[j]=getTreeDistance( (*obs)[i].relationtree, (*obs)[i].rtlen, 
					 p->landmark[j].relationtree, p->landmark[j].rtlen, 
					 partition_cost );
//		  sum += td[j];
		}
//		sum/=p->N;
		int t=0;
		for(int j=0;j<p->N;j++)
		  {
//		    float prob = td[j]/sum;
		    if( td[j] > THRESHOLD_td)
		      {
				t = ++(*candidates)[i][0].match;
				(*candidates)[i][t].match=j;
				(*candidates)[i][t].matchPr=td[j];
		      }

		}	
		(*candidates)[i][t+1].match=p->N;
		(*candidates)[i][t+1].matchPr=THRESHOLD_td;
	}
	//delete[] td;
}


// Returns maximum number of candidates possible (useful for memory allocation) 

int maximum(candiPr **N,int n)
{

  int max=0;
  for(int i=0;i<n;i++)
    if(max<N[i][0].match) max=N[i][0].match;

  return max;
}



 
void updateParticle ( Particle *P , Matrix u, Matrix R, Landmark **obs, int num_obs, candiPr **N )
{
	Matrix expected_pose(2,1), expected_obs(2,1), temp3(1,1), tempPose(2,1), tempObs(2, 1);
	Matrix pose_variance(2,2);
	int maxcandi=maximum(N,num_obs);


	Matrix *z=new Matrix[num_obs];
	for(int i=0;i<num_obs;i++){
		z[i].nrows=2;
		z[i].ncols=1;
		z[i] = (*obs)[i].mean;
	}

	Matrix G_map(2, 2, 1), G_pose(2, 2, -1), I(2, 2, 1);
	Matrix Z(2, 2), invZ(2, 2), invL(2, 2), K(2, 2), L(2, 2), tempmat(2, 2);		
	int loc;
	
//	cout<<"max candi="<<maxcandi<<endl;
	Matrix *mean=new Matrix[maxcandi+1];	
	Matrix *cov=new Matrix[maxcandi+1];
	for(int i=0;i<maxcandi+1;i++){
		mean[i].nrows=2;
		mean[i].ncols=1;
	}
	double *p= new double[maxcandi+1];
	float *highestmatch_prob=new float[num_obs];
	int *highestmatch_loc=new int[num_obs];
	
	for(int i=0;i<num_obs;i++)
	{
		highestmatch_prob[i]=0.0;
		highestmatch_loc[i]=0;
	}	
	

	pose_variance = R;	

	double ct, st;
	
	

	Matrix R1(2,2);
	R1.value[0][0] = R.value[0][0];		R1.value[0][1] = R.value[0][1];
	R1.value[1][0] = R.value[1][0];		R1.value[1][1] = R.value[1][1];
	
	int n;
	expected_pose= P->pose + u;
//	cout<<"Expected pose\n";
//	expected_pose.transpose().print();
	P->w = 100.0;
	int oldLandmarks = 0, newLandmarks = 0;
	for(int i=0;i<num_obs;i++)
	{
		for( n=0; n < N[i][0].match ;n++)
		{
			int match_id= N[i][n+1].match;
			expected_obs.g(expected_pose, P->landmark[match_id].mean);

			Z = R1 + (P->landmark[match_id].variance);
			invZ = Z.inverse();

			cov[n] = (invZ + R.inverse()).inverse();												//pose covariance
			mean[n] = expected_pose + cov[n] * G_pose.transpose() * invZ * (z[i] - expected_obs);	//pose mean
			mean[n].sample(mean[n], cov[n]);
			
			tempPose.g(mean[n], P->landmark[match_id].mean);
			tempPose = z[i]-tempPose;
			temp3 = tempPose.transpose() * invZ * tempPose;
			
			p[n]= pow( EXP, -0.5*temp3.value[0][0])/(sqrt(fabs(8.0*PI*PI*PI*Z.det())));
//			cout<<"p[n] : "<<p[n]<<endl;
		}

		p[n]=P_ZERO;
		loc=0;
		
		for(n=0;n<=N[i][0].match;n++) 																//pick a data association
		{
		//	cout << n <<"-------"<< p[n] << endl;
			if(highestmatch_prob[i] < p[n])
			{
				highestmatch_prob[i]=p[n];
				highestmatch_loc[i]=N[i][n+1].match;
				loc=n;
			}
		}
		//	cout<<i<<" matched to "<<highestmatch_loc[i]<<" "<<p[loc]<<endl;
		if(highestmatch_loc[i] < P->N)
		{
			pose_variance = cov[loc];
			expected_pose = mean[loc];
		}
		

		if(highestmatch_loc[i]==P->N)		
		{
			P->landmark[P->N+newLandmarks].mean.inv_g(expected_pose, z[i]);	
			P->landmark[P->N+newLandmarks].variance = R1;
			
			P->landmark[P->N+newLandmarks].rtlen = (*obs)[i].rtlen;
			P->landmark[P->N+newLandmarks].relationtree = (*obs)[i].relationtree;
			P->w *= P_ZERO;
			newLandmarks++;
		}
		else												
		{			
			expected_obs.g(expected_pose, P->landmark[ highestmatch_loc[i]].mean);	
			expected_pose=mean[loc];

			Z = R1 + P->landmark[highestmatch_loc[i]].variance;
			K = (P->landmark[highestmatch_loc[i]].variance) * G_map.transpose() * Z.inverse();
			tempObs = z[i]-expected_obs;
			
			P->landmark[highestmatch_loc[i]].mean = P->landmark[highestmatch_loc[i]].mean + K * tempObs;
			L = R + R1 + P->landmark[highestmatch_loc[i]].variance;
			P->landmark[highestmatch_loc[i]].variance = (I - K * G_map) * P->landmark[highestmatch_loc[i]].variance;
			invL = L.inverse();
			temp3 = tempObs.transpose()*invL*tempObs;
			
			P->w*= 10.0 *pow( EXP, -0.5*temp3.value[0][0])/(fabs(sqrt(8.0*PI*PI*PI*L.det())));
//			P->w+= 100*pow( EXP, -0.5*temp3.value[0][0]);
			oldLandmarks++;
		}
	}

	P->N += newLandmarks;
	P->pose = expected_pose;
//	printf("weight=%g\n",P->w);
//	printf("Old landmarks observed:%d\n",oldLandmarks);		
//	printf("New landmarks observed:%d\n",newLandmarks);	
	//printf("Total landmarks detected:%d\n",P->N);	
	delete[] mean;
	delete[] cov;
	delete[] p;delete[] highestmatch_loc;delete[] highestmatch_prob;

}
///////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////Resampling///////////////////////////////////////////////////////////

Particle* resampling( Particle* P, int M )
{
	float *index=(float*)malloc((M+1)*sizeof(float));
	index[0]=0.0;
	int randno;
	for( int i=1; i<=M; i++)
	{
		index[i] = P[i-1].w*1000 + index[i-1]+1;
	//	printf("%d %f\n",i,index[i]);	
	}	
	Particle *updatedSet=(Particle*)malloc(M*sizeof(Particle));
	
	for( int i=0;i<M;i++)
	{
		randno= rand() % (int)index[M];
		
		for(int j=1;j<=M;j++)
			if( randno<index[j])
			{
				updatedSet[i]=P[j-1];
				break;
			}
	}
	
	return updatedSet;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////				

