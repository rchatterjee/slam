/************************************************************
Program # run.cpp
Work		#	runs the whole particleMain and Map building files
Authur 	#	RChat
Date 		# 13th August, 2010
____________________________________________________________
dependecies:
	particleMain <executable>
		....its dependencies
	map <executable>
		....its dependencies

Control File should contain:
	num of images
	width height
	0 0 0
	<control info-1>[i.e. xx yy theta]
	<control info-2>
		.
		.
		.
	<control info-(num_img-1)>
folders name: 
		e.g classified/, abc/ [ending slash is important]
**********************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
//#include<gnuplot.h>

int main(int argc, char *argv[])
{
	int n, height, width;
	float r, th, x, y;
	if(argc<2) {
		printf("Usage: %s <folder's name> <error> <num Particle> <input of \'control\' file> \n", argv[0]);
		exit(0);
	}
	//float error=0;
	int error=atof(argv[2]);float e=0.0;
	int numParticle=atoi(argv[3]);
	scanf("%d", &n);
	scanf("%d%d", &width, &height);	
	char call[100];
	sprintf(call, "./executables/reset.out %d", numParticle);
	system(call);
	system("rm thetaInfo.txt");
	printf("Running SLAM:...");
	FILE *realpath=fopen("realpath","w");
	FILE *errorpath=fopen("errorpath","w");
	float realx=0.0, realy=0.0, realtheta=0.0, errorx=0.0, errory=0.0, errortheta=0.0;
	for(int i=0;i<n;i++)
	{	
		scanf("%f%f%f", &x, &y, &th);
		printf(".");
		fflush(stdout);
		
		realx+=x;
		realy+=y;
		realtheta+=th;
		
		if((argc>2) && (error!=0))
		{
			e = rand()%error;// - error/2;
			x = x+e; 
			e = rand()%error;// - error/2;
			y = y+e; 
	//		e = rand()%error - error/2;
	//		th = th+e/100;
		}
		
		errorx+=x; 
		errory+=y;
		errortheta+=th;
		
		fprintf(realpath,"%f %f %f\n", realx, realy, realtheta);
		fprintf(errorpath,"%f %f %f\n", errorx, errory, errortheta);
		sprintf(call, "./executables/particleMain.out %sout%d.asc %d %d %d %f %f %f", argv[1],i+1, width, height, numParticle, x, y, th);
		system(call);
		sprintf(call, "./executables/map.out %d %d %d %d", width,height, numParticle, 0);
		system(call);
	
		sprintf(call, "mv out.ppm at%d.ppm", i+1);
		system(call);
	}
	fclose(realpath);
	fclose(errorpath);
	
//	system("echo \"set size ratio -1; set xtics 50; set ytics 50; plot \\\"<awk '{print \\$1,-\\$2}' tempfile\\\" using 1:2 title \\\"Path with error\\\" with lines, \\\"<awk '{print \\$1,-\\$2}' tempfile2\\\" using 1:2 title \\\"Real Path\\\" with linespoints,  \\\"<awk '{print \\$1,-\\$2}' slampath_global\\\" using 1:2 title \\\"SLAM Path\\\" with linespoints, \\\"<awk '{print \\$1,-\\$2}' particle_points\\\" using 1:2 title \\\"Particle Points\\\" with points\" | gnuplot -persist");
	
	system("echo \"set size ratio -1; set xtics 50; set ytics 50; plot \\\"<awk '{print \\$1,-\\$2}' errorpath\\\" using 1:2 title \\\"Path with error\\\" with lines, \\\"<awk '{print \\$1,-\\$2}' realpath\\\" using 1:2 title \\\"Real Path\\\" with linespoints,  \\\"<awk '{print \\$1,-\\$2}' thetaInfo.txt\\\" using 1:2 title \\\"Particle Points\\\" with linespoints\" | gnuplot -persist");
	
	printf("...Complete\n");
	return 0;
}
