#segmentation=graph convertor uniform
EXCS = executables/
SOURCES = sources/

$(EXCS)GUInew.out: $(EXCS)particleMain.out $(EXCS)reset.out $(EXCS)map.out $(EXCS)cutimage1.out $(EXCS)run.out $(EXCS)graph.out $(EXCS)ImgConvertor.out $(EXCS)uniform.out $(EXCS)raw2jpg.out
	g++ -o $(EXCS)GUInew.out $(SOURCES)GUInew.cpp `pkg-config opencv --cflags --libs`
$(EXCS)run.out:  $(SOURCES)run.cpp $(EXCS)particleMain.out $(EXCS)map.out
	g++ -o $(EXCS)run.out $(SOURCES)run.cpp

$(EXCS)particleMain.out: #$(SOURCES)slam/particleMain.cpp $(SOURCES)slam/particle.cpp $(SOURCES)slam/relation_tree.cpp $(SOURCES)slam/matrix.cpp $(SOURCES)slam/get_pic_info.cpp
	g++ -o $(EXCS)particleMain.out $(SOURCES)slam/particleMain.cpp
$(EXCS)reset.out: $(SOURCES)slam/reset.cpp
	g++ -o $(EXCS)reset.out $(SOURCES)slam/reset.cpp
$(EXCS)map.out: $(SOURCES)map/map.cpp
	g++ -o $(EXCS)map.out $(SOURCES)map/map.cpp
$(EXCS)cutimage1.out: $(SOURCES)cutImage/cutimage1.cpp
	g++ -o $(EXCS)cutimage1.out $(SOURCES)cutImage/cutimage1.cpp `pkg-config opencv --libs --cflags`
$(EXCS)graph.out: $(SOURCES)segmentation/Classify_Graph_DS/Graph5.cpp
	g++ -o $(EXCS)graph.out $(SOURCES)segmentation/Classify_Graph_DS/Graph5.cpp -g
$(EXCS)ImgConvertor.out: $(SOURCES)segmentation/Convertor/ImgConvertor.cpp
	g++ -o $(EXCS)convertor.out $(SOURCES)segmentation/Convertor/ImgConvertor.cpp #`pkg-config opencv --libs --cflags`
$(EXCS)uniform.out: $(SOURCES)segmentation/Uniform/Driver.cpp
	g++ -o $(EXCS)uniform.out $(SOURCES)segmentation/Uniform/Driver.cpp
$(EXCS)raw2jpg.out: $(SOURCES)cutImage/raw2jpg.cpp
	g++ -o $(EXCS)raw2jpg.out $(SOURCES)cutImage/raw2jpg.cpp `pkg-config opencv --libs --cflags`
clean:
	rm $(EXCS)*.out particle*.asc at*.ppm raw_images/*.raw
